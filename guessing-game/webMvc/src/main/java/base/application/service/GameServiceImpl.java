package base.application.service;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import str.spring.Game;
import str.spring.MessageGenerator;


@Service
public class GameServiceImpl implements GameService {

    Logger log = LoggerFactory.getLogger(getClass());

    private final Game game;
    private final MessageGenerator generator;

    @Autowired
    public GameServiceImpl(Game game, MessageGenerator generator) {
        this.game = game;
        this.generator = generator;
    }

    @Override
    public boolean isGameOver() {
        return game.isGameWon() || game.isGameLost();
    }

    @Override
    public String getMainMessage() {
        return generator.getMainMessage();
    }

    @Override
    public String getResultMessage() {
        return generator.getResultMessage();
    }

    @Override
    public void checkGuess(int guess) {
        game.setGuess(guess);
        game.check();
    }

    @Override
    public void reset() {
        game.reset();
    }
}
