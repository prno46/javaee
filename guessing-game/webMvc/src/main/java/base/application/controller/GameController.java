package base.application.controller;

import base.application.service.GameService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;

import static base.common.Mapping.*;

@Controller
public class GameController {
    Logger log = LoggerFactory.getLogger(getClass());

    private final GameService service;

    @Autowired
    public GameController(GameService service) {
        this.service = service;
    }

    // request methods
    @GetMapping(PLAY)
    public String play() {
        if (service.isGameOver()) return GAME_OVER_VIEW;
        return PLAY_VIEW;
    }

    @ModelAttribute(MAIN_MESSAGE)
    public String getMainMessage() {
        return service.getMainMessage();
    }

    @ModelAttribute(RESULT_MESSAGE)
    public String getResultMessage() {
        return service.getResultMessage();
    }

    @GetMapping(RESET)
    public String gameReset() {
        service.reset();
        return REDIRECT_TO_PLAY;
    }

    //post methods
    @PostMapping(PLAY)
    // this guess parameter is the same that is market in play.html in input tag with name='guess'
    public String submitProcessing (@RequestParam int guess) {
        log.info("requested parameter: {}", guess);
        service.checkGuess(guess);
        return REDIRECT_TO_PLAY;
    }

}
