package base.configuration;


import base.application.interceptor.RequestInterceptor;
import base.common.Mapping;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Import;
import org.springframework.web.servlet.LocaleResolver;
import org.springframework.web.servlet.ViewResolver;
import org.springframework.web.servlet.config.annotation.InterceptorRegistry;
import org.springframework.web.servlet.config.annotation.ViewControllerRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer;
import org.springframework.web.servlet.i18n.LocaleChangeInterceptor;
import org.springframework.web.servlet.i18n.SessionLocaleResolver;
import org.springframework.web.servlet.view.InternalResourceViewResolver;
import org.springframework.web.servlet.view.UrlBasedViewResolver;
import org.thymeleaf.spring5.templateresolver.SpringResourceTemplateResolver;
import str.spring.config.GameConfig;

//@EnableWebMvc
@Configuration
//@ComponentScan(basePackages = "base.application")
@Import(value = GameConfig.class)
public class WebConfig implements WebMvcConfigurer {

    Logger log = LoggerFactory.getLogger(getClass());

    private static final String RESOLVER_PREFIX = "/WEB-INF/View/";
    private static final String RESOLVER_SUFFIX = ".jsp";

    private final SpringResourceTemplateResolver resolver;

    @Autowired
    public WebConfig(SpringResourceTemplateResolver resolver) {
        this.resolver = resolver;
    }

    @Bean
    public ViewResolver viewResolver() {
        UrlBasedViewResolver resolver = new InternalResourceViewResolver();
        resolver.setPrefix(RESOLVER_PREFIX);
        resolver.setSuffix(RESOLVER_SUFFIX);
        return resolver;
    }

    // added this to be able to decouple logic from view to a separate file
    @Bean
    public SpringResourceTemplateResolver resourceTemplateResolver() {
        log.info("init of the Thymeleaf resolver {}", resolver.hashCode());
        resolver.setUseDecoupledLogic(true);
        return resolver;
    }

    // for resolving the app language
    @Bean
    public LocaleResolver localeResolver () {
        return new SessionLocaleResolver();
    }

    // for mapping to home page without the method in the controller - basic view
    @Override
    public void addViewControllers(ViewControllerRegistry registry) {
        registry.addViewController("/").setViewName(Mapping.HOME_VIEW);
    }

    // registration of interceptors in the app
    @Override
    public void addInterceptors(InterceptorRegistry registry) {
        registry.addInterceptor(new RequestInterceptor());
        LocaleChangeInterceptor localeChangeInterceptor = new LocaleChangeInterceptor();
        localeChangeInterceptor.setParamName("lang");
        registry.addInterceptor(localeChangeInterceptor);
    }

}
