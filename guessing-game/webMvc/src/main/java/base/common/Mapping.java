package base.common;

public final class Mapping {

    private Mapping() { }

    // views mappings
    public static final String HOME_PAGE_VIEW = "/";
    public static final String HOME_VIEW = "homeView/home";
    public static final String PLAY_VIEW = "playView/play";
    public static final String GAME_OVER_VIEW = "gameOverView/game-over";

    // controller methods mapping
    public static final String PLAY = "play";
    public static final String RESET = "reset";
    public static final String REDIRECT_TO_PLAY = "redirect:/"+PLAY;

    // model attributes names
    public static final String MAIN_MESSAGE = "mainMessage";
    public static final String RESULT_MESSAGE = "resultMessage";

}
