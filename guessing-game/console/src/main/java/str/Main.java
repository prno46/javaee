package str;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;


@SpringBootApplication
class Main {
    private static final Logger LOG = LoggerFactory.getLogger(Main.class);

    public static void main(String[] args) {
        LOG.info("the new project - guessing game");

        // this will run the application with springBoot
        // springBoot supplies the context and enables package scanning
        SpringApplication.run(Main.class, args);

    }
}
