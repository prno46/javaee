package str.spring.textGui;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.event.ContextRefreshedEvent;
import org.springframework.context.event.EventListener;
import org.springframework.stereotype.Component;
import str.spring.Game;
import str.spring.MessageGenerator;

import java.util.Scanner;

/***
 * this class is to be used as a starting point of the App
 * the EventListeners are going to be used to make this work
 * Event listener in Spring is the implementation of observe pattern
 */

@Component
public class ConsoleNumberGuess  {

    private static final Logger log = LoggerFactory.getLogger(ConsoleNumberGuess.class);

    private final Game game;
    private final MessageGenerator messageGenerator;

    @Autowired
    ConsoleNumberGuess(Game game, MessageGenerator messageGenerator) {
        this.game = game;
        this.messageGenerator = messageGenerator;
    }

    @EventListener(ContextRefreshedEvent.class)
    public void startConsoleApplication() {
        log.info("start() -> Container is ready to use");

        Scanner sc = new Scanner(System.in);
        while (true) {
            System.out.println(messageGenerator.getMainMessage());
            System.out.println(messageGenerator.getResultMessage());

            int guess = sc.nextInt();
            sc.nextLine();
            game.setGuess(guess);
            game.check();

            if(game.isGameWon() || game.isGameLost()) {
                System.out.println(messageGenerator.getResultMessage());
                System.out.println("play again ? y/n");
                String playAgain = sc.nextLine().trim();
                if(!playAgain.equalsIgnoreCase("y")) {
                    System.out.println("end of the game");
                    break;
                }
                game.reset();
            }
        }
    }
}
