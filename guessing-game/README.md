# Guessing Game

Simple game made using Spring Framework.

First project made using Spring.


## Prerequisites

To launch this one you need to have: 

```
Java 8+
IDE 
Maven
```

## Installing

Import this as a Maven project to your IDE.

Start the application and go to localhost:8080 in your browser.

Try to guess the number.


## Built With

* [Maven](https://maven.apache.org/) - Dependency Management

## Authors

* **Przemyslaw Nowacki** 

