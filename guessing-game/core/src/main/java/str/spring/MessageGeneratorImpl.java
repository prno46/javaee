package str.spring;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.MessageSource;
import org.springframework.context.i18n.LocaleContextHolder;
import org.springframework.stereotype.Component;

import javax.annotation.PostConstruct;

@Component
public class MessageGeneratorImpl implements MessageGenerator {

    private static final Logger LOG = LoggerFactory.getLogger(MessageGeneratorImpl.class);
    private static final String MAIN_MESSAGE="main.message";

    private static final String RESULT_GAME_WON="result.gameWon";
    private static final String RESULT_GAME_LOST="result.gameLost";
    private static final String RESULT_VALID_RANGE="result.validRange";
    private static final String RESULT_NEXT_GUESS="result.nextGuess";
    private static final String RESULT_DIRECTION="result.remainingGuess";

    private static final String DIRECTION_LOWER="direction.lower";
    private static final String DIRECTION_HIGHER="direction.higher";


    private final Game game;
    private final MessageSource messageSource;

    @Autowired
    public MessageGeneratorImpl(Game game, MessageSource messageSource) {
        this.game = game;
        this.messageSource = messageSource;
    }

    @Override
    public String getMainMessage() {
        return getMessage(MAIN_MESSAGE, game.getSmallest(), game.getBiggest());
    }

    @Override
    public String getResultMessage() {
        if(game.isGameWon()) {
            return getMessage(RESULT_GAME_WON, game.getNumber());
        }else if(game.isGameLost()) {
            return getMessage(RESULT_GAME_LOST, game.getNumber());
        } else if(!game.isValidNumberRange()) {
            return getMessage(RESULT_VALID_RANGE);
        } else if(game.getRemainingGuesses() == game.getGuessCount()) {
            return getMessage(RESULT_NEXT_GUESS);
        } else {
            String direction = getMessage(DIRECTION_LOWER);
            if(game.getGuess() < game.getNumber()) {
                direction = getMessage(DIRECTION_HIGHER);
            }
            return getMessage(RESULT_DIRECTION, direction, game.getRemainingGuesses());
        }
    }

    @PostConstruct
    void posExexuteMethod() {
        LOG.debug("Message form post execute the value of number is {}", game.getNumber());
    }

    /**
     *
     * @param code - represents the key in the message property file
     * @param args - those are the parameters that we need to pass to our message
     * @return - returns a compete message
     */
    private String getMessage (String code, Object ... args) {
        return messageSource.getMessage(code, args, LocaleContextHolder.getLocale());
    }
}
