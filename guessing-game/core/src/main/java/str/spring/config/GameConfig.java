package str.spring.config;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.PropertySource;
import str.spring.GuessCount;
import str.spring.MaxNumber;
import str.spring.MinNumber;

@Configuration
@PropertySource("classpath:config/game.properties")
@ComponentScan(basePackages = "str.spring")
public class GameConfig {

    // the :20 is the default value that will be assign to this field if spring will not find it in the
    // property file

    @Value("${game.maxNumber:20}")
    private int newMaxNumber;

    @Value("${game.guessCount:6}")
    private int newGuessCount;

    @Value("${game.minNumber:5}")
    private int minNumber;

    @Bean
    @MaxNumber
    public int getMaxNumber() {
        return newMaxNumber;
    }

    @Bean
    @GuessCount
    public int getGuessCount() {
        return newGuessCount;
    }

    @Bean
    @MinNumber
    public int getMinNumber() {
        return minNumber;
    }
}
