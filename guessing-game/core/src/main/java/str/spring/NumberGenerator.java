package str.spring;

public interface NumberGenerator {
    int next();
    int getMaxNumber();
    int getMinNumber();

}
