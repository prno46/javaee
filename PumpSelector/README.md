# Pump Selector

The main goal of this project is to make a selection of hydraulic pump required in manufacturing of Refuse and Garbage Truck

easier, so the person without technical background after a basic training could such a pump select and order by supplier 

without any worry that ordered part will not fulfill the requirements.

```
Still under development.

```


## How it should work

### As a input data we need: 

* Body Type - the type of Garbage Truck that was ordered by customer 

* Chassis Type - the chassis that was supplied by the customer on which the Garbage Truck is going to be build (MAN, DAF, SCANIA ...)

* PTO ratio - depends on given chassis, available in chassis' data sheet

* Direction of chassis PTO rotation (only two option possible LEFT or RIGHT), available in chassis' data sheet

### As a output:

List of a possible pumps that can be used for a given input and given engine speed (min. 850 rpm, max 1200 rpm). 


## Project Structure

Project contains four main modules:

* core - contains domain models for hydraulic pump creation

* repository - contains classes responsible for data base operations

* useCases - contains classes responsible for pump calculation

* web - contains classes responsible for presentation layer like views and controllers


## Prerequisites

You should have a given software installed to be able to run this early alpha:

```
Maven 3.8.0
Java 11 (JDK 11)
IDE IntelliJ Idea Community Edition 2018 or later
```

Libraries and Frameworks used in project:

```
Java 11
Spring Boot 2.+
Spring Data 5.+
Spring MVC 5.+
Hibernate 5.+
PostgreSQL 10
Thymeleaf 3.0

```

### Installing

Download this project, import it in your IDE as Maven Project.

Modify the hibernate.setup.properties file in resource folder in web module

to insert a data to your data base connection. 

After starting the application go to localhost:8080 in your browser you should see the Main Menu.

Next you need to insert some data to database to see the calculation result:

After inserting those data choose new calculation from main view and press calculate.

You should see the list available pumps for a given product with given options.

##### Insert this in Add New Pump Ring: 

```
symbol: B10; flow: 34.1; BOTH  
symbol: B12; flow: 37.1; BOTH  
symbol: B14; flow: 46.0; BOTH  
symbol: B17; flow: 58.3; BOTH  
symbol: B20; flow: 63.8; BOTH  
symbol: B22; flow: 70.1; INDIRECT  
symbol: B25; flow: 79.3; DIRECT  
symbol: B24; flow: 79.5; INDIRECT  
symbol: B28; flow: 89.7; BOTH  
```

##### Insert this data to Add New Pto Value:  

```
1.0
1.07
1.19
1.22
```

##### Insert this data to Add New Body Type:

```
Type: Medium XL; Ratio: 80.0
Type: Medium XL-S; Ratio: 70.0
Type: Mini XL; Ratio: 70.0
Type: Mini; Ratio: 60.0
```

##### Insert this data to Add New Chassis Brand:

```
MAN
DAF
SCANIA
MERCEDES AROCS
MERCEDES AXOR
```

## Deployment

No additional notes about how to deploy this on a live system

## Built With

* [Maven](https://maven.apache.org/) - Dependency Management


## Authors

* **Przemyslaw Nowacki** 




