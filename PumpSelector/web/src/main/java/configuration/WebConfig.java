package configuration;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.servlet.config.annotation.ResourceHandlerRegistry;
import org.springframework.web.servlet.config.annotation.ViewControllerRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer;
import org.springframework.web.servlet.mvc.method.annotation.RequestMappingHandlerAdapter;
import org.thymeleaf.spring5.templateresolver.SpringResourceTemplateResolver;

import javax.annotation.PostConstruct;

@Configuration
public class WebConfig implements WebMvcConfigurer {
    private Logger log = LoggerFactory.getLogger(getClass().getSimpleName());

    private final SpringResourceTemplateResolver resolver;

    @Autowired
    private RequestMappingHandlerAdapter requestMappingHandlerAdapter;

    @Autowired
    public WebConfig(SpringResourceTemplateResolver resolver) {
        this.resolver = resolver;
    }

    // added this to be able to decouple logic from view to a separate file
    @Bean
    public SpringResourceTemplateResolver resourceTemplateResolver() {
        resolver.setUseDecoupledLogic(true);
        return resolver;
    }

    // home page mapping without controller -> directs to thi site when app starts
    @Override
    public void addViewControllers(ViewControllerRegistry registry) {
        registry.addViewController("/").setViewName(Mappings.HOME_SCREEN);
    }

    /***
     * When spring.resources.add-mappings=false it allows to set path to css and js files
     * used in our views, used when we want to have more control over Errors handling.
     * @param registry - handler that is used to specify the files and its location
     *                   to be used in our application.
     */
    @Override
    public void addResourceHandlers(ResourceHandlerRegistry registry) {
        registry.addResourceHandler("/css/**")
                .addResourceLocations("classpath:/static/css/");
    }

    @PostConstruct
    public void init() {
        requestMappingHandlerAdapter.setIgnoreDefaultModelOnRedirect(true);
    }


    /*@Bean
    public SimpleMappingExceptionResolver createSimpleMappingExceptionResolver(){
        SimpleMappingExceptionResolver resolver = new SimpleMappingExceptionResolver();
        resolver.setDefaultErrorView("error");
        return resolver;
    }*/
}
