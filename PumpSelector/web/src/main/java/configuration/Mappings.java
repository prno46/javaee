package configuration;

public class Mappings {

    private Mappings() { }

    //*********Screens or Views
    public static final String REDIRECT = "redirect:/";
    public static final String HOME_SCREEN = "homeView/home";
    public static final String ADD_NEW_PTO_SCREEN = "addNewPtoView/newPto";
    public static final String ADD_NEW_BODY_TYPE_SCREEN = "addNewBodyTypeView/newBodyType";
    public static final String ADD_NEW_CHASSIS_SCREEN = "addNewChassisBrandView/newChassis";
    public static final String ADD_NEW_PUMP_RING_SCREEN = "addNewPumpRingView/newPumpRing";
    public static final String PUMP_CALCULATION_SCREEN = "pumpCalculationView/pumpCalculation";
    public static final String CALCULATION_RESULT_VIEW = "CalculationResultView/calculationResult";

    //*********Controller's method mappings
    // Get
    public static final String ADD_NEW_PTO = "add_new_pto";
    public static final String ADD_NEW_BODY = "add_new_body";
    public static final String ADD_NEW_CHASSIS = "add_new_chassis";
    public static final String ADD_NEW_PUMP_RING = "add_new_pumpRing";
    public static final String PUMP_CALCULATION = "calculation";

    //Post
    public static final String NEW_PTO_SUBMIT = "ptoSubmit";
    public static final String NEW_BODY_SUBMIT = "bodySubmit";
    public static final String NEW_CHASSIS_SUBMIT = "chassisSubmit";
    public static final String NEW_PUMP_RING_SUBMIT = "pumpRingSubmit";
    public static final String CALCULATION_RESULT = "result";

    //********Controller's attribute mappings
    public static final String PTO_TYPES = "ptoTypes";

    // CalculationResultController
    public static final String CALCULATION_MODEL = "calculationModel";
    public static final String CHASSIS_BRANDS = "chassisBrands";
    public static final String RESULT_MAP = "resultMap";

    //******entity models
    public static final String PTO_MODEL = "ratioEntity";
    public static final String BODY_MODEL = "bodyType";
    public static final String CHASSIS_MODEL = "chassisType";
    public static final String PUMP_RINGS_MODEL = "pumpRingType";
    public static final String PUMP_RING_CATEGORY_MODEL = "ringCategory";

    // used in *.th.xml files for thymeleaf logic
    // PtoRatio entity fields
    public static final String TYPE = "type";
    public static final String RATIO = "ratio";

    // BodyType entity fields
    public static final String SYMBOL = "symbol";
    public static final String FLOW_RATIO = "flowRatio";

    // ChassisBrand entity fields
    public static final String BRAND = "brand";

    // PumpRingE entity fields
    public static final String RING_SYMBOL = "symbol";
    public static final String RING_FLOW_RATIO = "flowRatio";


    //******* input validation
    //OrderViewModel
    public static final String SYMBOL_MESSAGE = "cannot be null and length have to be between 6 and 8 characters";
    public static final String SYMBOL_PATTERN_MESSAGE = "it must consists of at least 2 - 4 letters in uppercase and 4 digits";
    public static final String ORDER_DATE_MESSAGE = "date cannot be null, empty or cannot be in the future";
    public static final String PUMP_DATA_MESSAGE = "you need to choose one option";

    // th.logic constants of OrderViewModel
    public static final String SAVE_SUBMIT = "saveToDb";
    public static final String ORDER_MODEL = "orderModel";
    public static final String ORDER_SYMBOL = "orderSymbol";
    public static final String ORDER_DATE = "orderDate";
    public static final String ORDER_BODY_TYPE = "bodyType";
    public static final String ORDER_PTO_RATIO = "ptoRatio";
    public static final String ORDER_DIRECT_MOUNT = "directMountPump";
    public static final String ORDER_ECO = "ecoOption";
    public static final String ORDER_PUMP_DATA = "pumpData";
    public static final String ORDER_CHASSIS = "chassis";

    //th.logic composed constants -> OrderViewModel
    public static final String ORDER_AND_DATE = String.join(".", ORDER_MODEL, ORDER_DATE);
    public static final String ORDER_AND_PTO_RATIO = String.join(".", ORDER_MODEL, ORDER_PTO_RATIO);
    public static final String ORDER_AND_DIRECT_MOUNT = String.join(".", ORDER_MODEL, ORDER_DIRECT_MOUNT);
    public static final String ORDER_AND_ECO = String.join(".", ORDER_MODEL, ORDER_ECO);
    public static final String RESULT_MAP_AND_KEY_SET = String.join(".", RESULT_MAP, "keySet()");


}
