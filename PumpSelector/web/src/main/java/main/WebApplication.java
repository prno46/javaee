package main;

import application.converters.LocalDateConverter;
import configuration.WebConfig;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.jdbc.DataSourceAutoConfiguration;
import org.springframework.context.annotation.Import;
import org.springframework.format.FormatterRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer;
import repoConfig.HibernateConfiguration;

@SpringBootApplication(exclude = {DataSourceAutoConfiguration.class},
        scanBasePackages = {"domain",
                "repository.databaseSource.daos", "repository.dataRepository",
                "repository.domainRepository", "useCases", "application"}
)
@Import({WebConfig.class, HibernateConfiguration.class})
public class WebApplication implements WebMvcConfigurer {

    @Override
    public void addFormatters(FormatterRegistry registry) {
        registry.addConverter(new LocalDateConverter("yyyy-MM-dd"));
    }

    public static void main(String[] args) {

        SpringApplication.run(WebApplication.class);

        /*ApplicationContext ctx =  SpringApplication.run(WebApplication.class);
        DispatcherServlet ds = (DispatcherServlet) ctx.getBean("dispatcherServlet");
        ds.setThrowExceptionIfNoHandlerFound(true);  -> not needed for now*/
    }

}
