package application.controller;


import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.NoHandlerFoundException;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.sql.SQLException;

@RequestMapping("/error")
@ControllerAdvice
public class AppExceptionHandler {
    Logger logger = LoggerFactory.getLogger(getClass().getSimpleName());

    public AppExceptionHandler() {
        logger.info("AdvisedController creation");
    }

    @ExceptionHandler()
    String defaultExceptionHandler(HttpServletRequest rq, HttpServletResponse rs, Exception ex)  {
        logger.info("in defaultExceptionHandler method, status code: {}", ex.getMessage());
        int statusCode = rs.getStatus();
        String sCode;
        switch(statusCode) {
            case 404 : sCode = "error/error-404";
                break;
            case 405 : sCode = "error/error-405";
                break;
            case 500 : sCode = "error/error-500";
                break;
            default: return "error";
        }
        return sCode;
    }

    @ExceptionHandler(NoHandlerFoundException.class)
    ModelAndView noHandlerFound(Exception e) {
        var modelAndView = new ModelAndView();
        logger.info("in noHandlerFound method, message: {}", e.getMessage());
        modelAndView.setViewName("error/error-noHandlerFound");
        modelAndView.addObject("er",e);
        return modelAndView;
    }

    @ExceptionHandler(SQLException.class)
    ModelAndView sqlExceptions(Exception e) {
        var modelAndView = new ModelAndView();
        modelAndView.setViewName("error/error-noHandlerFound");
        modelAndView.addObject("er",e);
        return modelAndView;
    }
}
