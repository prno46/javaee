package application.controller;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.boot.web.servlet.error.ErrorController;
import org.springframework.web.bind.annotation.RequestMapping;

import javax.servlet.http.HttpServletResponse;

//@Controller
class CustomErrorController implements ErrorController {

    private Logger logger = LoggerFactory.getLogger(getClass().getSimpleName());

    public CustomErrorController() {
        logger.info("in constructor, class loaded");
    }

    @RequestMapping("/error")
    String getErrorView(HttpServletResponse response) {
        logger.info("@RequestMapping method ");
        int statusCode = response.getStatus();
        String sCode;
        switch(statusCode) {
            case 404 : sCode = "error/error-404";
            break;
            case 405 : sCode = "error/error-405";
            break;
            case 500 : sCode = "error/error-500";
            break;
            default: return "error/error-common";
        }
        return sCode;
    }

    @Override
    public String getErrorPath() {
        return "/";
    }

    /*
    * When not using a controller to have a custom error page you need to put error.htm to template
    * folder, when using Thymeleaf.
    * when using controller you need to (for now) mark the method @RequestMapping("/error")
    * and direct to your custom error page.
    */
}
