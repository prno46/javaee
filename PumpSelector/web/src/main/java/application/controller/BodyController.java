package application.controller;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import repository.databaseSource.entities.BodyType;
import useCases.LoadBasicDataUseCase;

import javax.validation.Valid;

import static configuration.Mappings.*;

@Controller
class BodyController {
    private Logger log = LoggerFactory.getLogger(getClass().getSimpleName());

    private final LoadBasicDataUseCase bodyUseCase;

    @Autowired
    public BodyController(LoadBasicDataUseCase bodyUseCase) {
        this.bodyUseCase = bodyUseCase;
    }

    @GetMapping(ADD_NEW_BODY)
    String showAddNewBodyTypeForm() {
        return ADD_NEW_BODY_TYPE_SCREEN;
    }

    @ModelAttribute(BODY_MODEL)
    BodyType modelProvider(){
        return new BodyType();
    }

    @PostMapping(NEW_BODY_SUBMIT)
    String formSubmit(@ModelAttribute(BODY_MODEL) @Valid BodyType body, BindingResult result) {
        if (result.hasErrors()) {
            return ADD_NEW_BODY_TYPE_SCREEN;
        }
        bodyUseCase.saveNewBodyEntity(body);
        return HOME_SCREEN;
    }
}
