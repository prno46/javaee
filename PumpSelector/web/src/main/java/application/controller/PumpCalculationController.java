package application.controller;

import domain.body.RCVType;
import domain.pumps.Rotation;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;
import repository.databaseSource.entities.PtoRatio;
import useCases.LoadBasicDataUseCase;
import useCases.PumpCalculationUseCase;
import useCases.calculationOptions.PumpCalculationResult;
import useCases.models.CalculationInputModel;

import java.util.Arrays;
import java.util.List;
import java.util.Map;

import static configuration.Mappings.PUMP_CALCULATION;
import static configuration.Mappings.PUMP_CALCULATION_SCREEN;

/**
 * hint -> for thymeleaf to work properly with models the getters need to start with get!!!
 */
@Controller
class PumpCalculationController {
    private Logger log = LoggerFactory.getLogger(getClass().getSimpleName());

    private final PumpCalculationUseCase calculationUseCase;
    private final LoadBasicDataUseCase loadData;


    @Autowired
    PumpCalculationController(PumpCalculationUseCase calculationUseCase, LoadBasicDataUseCase loadData) {
        this.calculationUseCase = calculationUseCase;
        this.loadData = loadData;
    }

    // get
    @GetMapping(PUMP_CALCULATION)
    String showCalculationView() {
        return PUMP_CALCULATION_SCREEN;
    }

    // models
    @ModelAttribute("ratios")
    List<PtoRatio> ptos() {
        return loadData.loadAllPtoRatios();
    }

    @ModelAttribute("rcvTypes")
    List<RCVType> provideRcvTypes() {
        return loadData.loadAllRcvTypes();
    }
    @ModelAttribute("rotation")
    List<Rotation> provideRotationDirection() {
        return Arrays.asList(Rotation.values());
    }

    @ModelAttribute("calculationModel")
    CalculationInputModel provideCalculationModel() {
        return new CalculationInputModel();
    }

    //post
    @PostMapping("calculate")
    String calculate(@ModelAttribute("calculationModel") CalculationInputModel inputModel, BindingResult result,
                                                                                    RedirectAttributes redAttr) {
        if (result.hasErrors()) {
            return "pumpCalculationView/pumpCalculation";
        }
        calculationUseCase.setUserInput(inputModel);
        Map<Integer, List<PumpCalculationResult>> resultMap = calculationUseCase.getCalculatedPumpsMap();
        inputModel.setCalculatedPumps(resultMap);
        redAttr.addFlashAttribute("calculationModel", inputModel);

        return "redirect:/result";
    }

    /* Hint
     * forward:/ - if used in POST method it can be received only by POST method in
     *              different controller (so GET with GET, POST with POST)
     * redirect:/ - you can redirect either to GET or POST
     *
     * to pass object from one controller to another need to use RedirectAttributes in POST Method and
     * set -> addFlashAttributes on RedirectAttributes object.
     */

}
