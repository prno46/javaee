package application.controller;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.servlet.ModelAndView;
import repository.databaseSource.PtoType;
import repository.databaseSource.entities.PtoRatio;
import useCases.LoadBasicDataUseCase;

import javax.annotation.PostConstruct;
import javax.servlet.http.HttpServletRequest;
import javax.validation.Valid;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import static configuration.Mappings.*;


@Controller
class NewPtoController {
    private Logger log = LoggerFactory.getLogger(getClass().getSimpleName());

    private final LoadBasicDataUseCase ptoUseCase;
    private List<PtoRatio> ratiosCache;

    @Autowired
    public NewPtoController (LoadBasicDataUseCase useCase) {
        this.ptoUseCase = useCase;
    }

    @PostConstruct
    private void setPtoRatios() {
        ratiosCache = new ArrayList<>(ptoUseCase.loadAllPtoRatios());
    }

    @GetMapping(ADD_NEW_PTO)
    public String showView() {
        return ADD_NEW_PTO_SCREEN;
    }

    @ModelAttribute(PTO_TYPES)
    public List<PtoType> ptoTypes() {
        return Arrays.asList(PtoType.values());
    }

    // backed by model
    @ModelAttribute(PTO_MODEL)
    PtoRatio backingModelForGetMapping() {
        return new PtoRatio();
    }

    @PostMapping(NEW_PTO_SUBMIT)
    public String getParamsFromForm (
            @ModelAttribute(PTO_MODEL) @Valid PtoRatio entity, BindingResult result) {
        if (result.hasErrors()){
            return ADD_NEW_PTO_SCREEN;
        }
        ptoUseCase.saveNewPtoEntity(entity); // todo -> what errors can be here ??
        return HOME_SCREEN;
    }

    //todo -> add a method to delete selected item from database or edit selected item

    @ModelAttribute("allRatios")
    List<PtoRatio> availablePtoRatios() {
        return ratiosCache;
    }

    @PostMapping("refresh")
    ModelAndView refreshPtoAfterChange() {
        ModelAndView modelAndView = new ModelAndView();
        //log.warn("the value of the [0] ratio id: {}", ratiosCache.get(0).getPtoRId());
        //ratiosCache.removeIf(p -> p.getPtoRId() == 16);
        modelAndView.setViewName("redirect:/"+ ADD_NEW_PTO);
        modelAndView.addObject("allRatios", ratiosCache);
        return modelAndView;
        // todo -> possible that this method will not be needed
    }

    @PostMapping("edit")
    ModelAndView editEndDelete(@ModelAttribute("id") Long id, HttpServletRequest rq) {
        log.warn("Post was called, id value is: {}", id);
        ModelAndView view = new ModelAndView();
        String action = rq.getParameter("action");
        switch(action) {
            case "Edit" :
                log.warn("Edit button was pressed");
                break;
            case "Delete":
                log.warn("Delete button was pressed");
                break;
        }
        view.setViewName("redirect:/"+ADD_NEW_PTO);
        return view;
    }
   // todo -> this works for now, the value of Id is passed from view to method above
    // need to commit changes -next


}
