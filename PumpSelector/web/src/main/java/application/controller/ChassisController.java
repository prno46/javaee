package application.controller;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import repository.databaseSource.entities.ChassisBrand;
import useCases.LoadBasicDataUseCase;

import javax.validation.Valid;

import static configuration.Mappings.*;

@Controller
class ChassisController {
    private Logger log = LoggerFactory.getLogger(getClass().getSimpleName());

    private final LoadBasicDataUseCase chassisUseCase;

    @Autowired
    public ChassisController(LoadBasicDataUseCase chassisUseCase) {
        this.chassisUseCase = chassisUseCase;
    }

    @GetMapping(ADD_NEW_CHASSIS)
    String showChassisBrandAddScreen(){
        return ADD_NEW_CHASSIS_SCREEN;
    }

    @ModelAttribute(CHASSIS_MODEL)
    ChassisBrand chassisBrandModel() {
        return new ChassisBrand();
    }

    @PostMapping(NEW_CHASSIS_SUBMIT)
    String submitChassisBrand(@ModelAttribute(CHASSIS_MODEL) @Valid ChassisBrand brand, BindingResult result) {
        if (result.hasErrors()) {
            return ADD_NEW_CHASSIS_SCREEN;
        }
        chassisUseCase.saveNewChassisBrandEntity(brand);
        return HOME_SCREEN;
    }
}
