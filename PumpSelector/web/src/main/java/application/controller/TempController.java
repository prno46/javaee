package application.controller;

import application.temp.Temp;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Controller;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;

import javax.validation.Valid;

@Controller
class TempController {
    private Logger log = LoggerFactory.getLogger(getClass().getSimpleName());
    // backed by model

    @GetMapping("temp")
    String tempHomePage() {
        return "temp/hm";
    }

    @ModelAttribute(name = "temp")
    Temp attribute() {
        return new Temp();
    }

    @PostMapping("test")
    String submit(@Valid Temp temp, BindingResult bindingResult) {
        if (bindingResult.hasErrors()){
            log.info("field error: {}",bindingResult.getFieldError());
            return "temp/hm";
        }
        return "redirect:/temp";
    }

}
