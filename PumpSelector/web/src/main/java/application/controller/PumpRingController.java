package application.controller;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import repository.databaseSource.PumpRingCategory;
import repository.databaseSource.entities.PumpRingE;
import useCases.LoadBasicDataUseCase;

import javax.validation.Valid;

import static configuration.Mappings.*;

@Controller
class PumpRingController {
    private Logger log = LoggerFactory.getLogger(getClass().getSimpleName());

    private final LoadBasicDataUseCase useCase;

    @Autowired
    public PumpRingController(LoadBasicDataUseCase useCase) {
        this.useCase = useCase;
    }

    @GetMapping(ADD_NEW_PUMP_RING)
    String showAddPumpRingScreen() {
        return ADD_NEW_PUMP_RING_SCREEN;
    }

    @ModelAttribute(PUMP_RINGS_MODEL)
    PumpRingE modelProvider() {
        return new PumpRingE();
    }

    @ModelAttribute(PUMP_RING_CATEGORY_MODEL)
    PumpRingCategory[] pumpringCategoryModel() {
        return PumpRingCategory.values();
    }

    @PostMapping(NEW_PUMP_RING_SUBMIT)
    String formSubbmit(@ModelAttribute(PUMP_RINGS_MODEL) @Valid PumpRingE ring, BindingResult res) {
        if (res.hasErrors()) {
            return ADD_NEW_PUMP_RING_SCREEN;
        }
        useCase.saveNewPumpRingEntity(ring);
        return HOME_SCREEN;
    }
}
