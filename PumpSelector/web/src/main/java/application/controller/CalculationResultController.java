package application.controller;

import application.models.OrderViewModel;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.servlet.ModelAndView;
import repository.databaseSource.entities.ChassisBrand;
import useCases.LoadBasicDataUseCase;
import useCases.calculationOptions.PumpCalculationResult;
import useCases.models.CalculationInputModel;

import javax.validation.Valid;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import static configuration.Mappings.*;

@Controller
class CalculationResultController {

    private Logger log = LoggerFactory.getLogger(getClass().getSimpleName());
    private final LoadBasicDataUseCase basicDataUseCase;
    private Map<Integer, List<PumpCalculationResult>> resultMap;
    private ModelAndView modelAndView;

    @Autowired
    CalculationResultController(LoadBasicDataUseCase basicDataUseCase) {
        this.basicDataUseCase = basicDataUseCase;
        resultMap = new HashMap<>();
        modelAndView = new ModelAndView(CALCULATION_RESULT_VIEW);
    }

    @GetMapping(CALCULATION_RESULT)
    ModelAndView getResultView(@ModelAttribute(CALCULATION_MODEL) CalculationInputModel calculationModel) {
        if (calculationModel.getRcvType() != null) {
            modelAndView.addObject(CALCULATION_MODEL, calculationModel);
            resultMap.putAll(calculationModel.getCalculatedPumps());
        }
        return modelAndView;
    }

    @ModelAttribute(RESULT_MAP)
    Map<Integer, List<PumpCalculationResult>> providePumpList() {
        return resultMap;
    }

    @ModelAttribute(CHASSIS_BRANDS)
    List<ChassisBrand> provideChassis() {
        return basicDataUseCase.loadAllChassisBrands();
    }

    @ModelAttribute(ORDER_MODEL)
    OrderViewModel provideOrderModel() {
        return new OrderViewModel();
    }

    @PostMapping(SAVE_SUBMIT)
    String saveOrderInDbDisplayHomeView(@Valid @ModelAttribute(ORDER_MODEL) OrderViewModel orderModel, BindingResult br) {
        if (br.hasErrors()) {
            return CALCULATION_RESULT_VIEW;
        }
        if (orderModel.getPumpData() != null) {
            String[] pumpData = orderModel.getPumpData().split("-");
            log.info("order model pumpData: speed={}, symbol={}, p1={}, p2={}, date={}",
                    pumpData[0], pumpData[1], pumpData[2], pumpData[3], orderModel.getOrderDate());
        }
        return "redirect:/"; // goes to home page for now
    }


}
