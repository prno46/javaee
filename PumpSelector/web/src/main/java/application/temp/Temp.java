package application.temp;

import javax.validation.constraints.Min;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

public class Temp {

    @NotNull
    @Size(min = 3, max = 30, message = "the length should be between 3 and 20")
    private String name;

    @Min(value = 16, message = "it should be above 16")
    private Integer age;

    public String getName() {
        return this.name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Integer getAge() {
        return age;
    }

    public void setAge(Integer age) {
        this.age = age;
    }

    public static void main(String[] args) {

        String pattern = "^[A-Z]{2,4}[0-9]{4}$";
        //System.out.println("FS925".matches(pattern));

        String pattern1 = "^[BP][0-9]{2}$";
        System.out.println("P55".matches(pattern1));
        System.out.println("P75".matches(pattern1));

    }


    // TODO -> below is what has left to do
    //  the persistence of the final oder to database
    //  to add delete and update option to : PtoView, ChassisView, PumpRingView
    //  work on front-end side of the project
    //  add search view for saved orders + search filter options + database
    //  add unit tests to this project

}
