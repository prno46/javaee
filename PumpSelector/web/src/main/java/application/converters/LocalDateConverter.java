package application.converters;

import org.springframework.core.convert.converter.Converter;

import java.time.LocalDate;
import java.time.format.DateTimeFormatter;

public class LocalDateConverter implements Converter<String, LocalDate> {

    private final DateTimeFormatter formatter;

    public LocalDateConverter(String format) {
        this.formatter = DateTimeFormatter.ofPattern(format);
    }

    @Override
    public LocalDate convert(String s) {
        return (s == null || s.isEmpty()) ? null /*LocalDate.now()*/ : LocalDate.parse(s, formatter);
    }
}
