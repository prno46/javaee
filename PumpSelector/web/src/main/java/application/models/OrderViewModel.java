package application.models;


import javax.validation.constraints.NotNull;
import javax.validation.constraints.PastOrPresent;
import javax.validation.constraints.Pattern;
import javax.validation.constraints.Size;
import java.time.LocalDate;

import static configuration.Mappings.*;

public class OrderViewModel {

    @NotNull
    @Size(min = 6, max = 8, message = SYMBOL_MESSAGE)
    @Pattern(regexp = "^[A-Z]{2,4}[0-9]{4}$", message = SYMBOL_PATTERN_MESSAGE)
    private String orderSymbol;

    @PastOrPresent(message = ORDER_DATE_MESSAGE)
    @NotNull
    private LocalDate orderDate;
    private String bodyType;
    private String chassis;
    private double ptoRatio;
    private String directMountPump;
    private String ecoOption;

    @NotNull(message = PUMP_DATA_MESSAGE)
    private String pumpData;

    public OrderViewModel() { }

    public String getOrderSymbol() {
        return orderSymbol;
    }

    public void setOrderSymbol(String orderSymbol) {
        this.orderSymbol = orderSymbol.toUpperCase();
    }

    public LocalDate getOrderDate() {
        return orderDate;
    }

    public void setOrderDate(LocalDate orderDate) {
        this.orderDate = orderDate;
    }

    public String getBodyType() {
        return bodyType;
    }

    public void setBodyType(String bodyType) {
        this.bodyType = bodyType;
    }

    public double getPtoRatio() {
        return ptoRatio;
    }

    public void setPtoRatio(double ptoRatio) {
        this.ptoRatio = ptoRatio;
    }

    public String getDirectMountPump() {
        return directMountPump;
    }

    public void setDirectMountPump(String directMountPump) {
        this.directMountPump = directMountPump;
    }

    public String getEcoOption() {
        return ecoOption;
    }

    public void setEcoOption(String ecoOption) {
        this.ecoOption = ecoOption;
    }

    public String getChassis() {
        return chassis;
    }

    public void setChassis(String chassis) {
        this.chassis = chassis;
    }

    public String getPumpData() {
        return pumpData;
    }

    public void setPumpData(String pumpData) {
        this.pumpData = pumpData;
    }
}
