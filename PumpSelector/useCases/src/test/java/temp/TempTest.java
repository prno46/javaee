package temp;

import domain.body.RCVType;
import domain.body.RCVTypeFactory;
import domain.pumpRings.PumpRings;
import domain.pumpRings.PumpRingsFactory;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import repository.databaseSource.PtoType;
import repository.databaseSource.entities.PtoRatio;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

public class TempTest {

    private RCVTypeFactory bodyFactory;
    private PumpRingsFactory ringsFactory;
    private RCVType miniXLH;
    private List<Integer> engineRevs;
    private PtoRatio ratio;
    private List<PumpRings> rings;
    private double delta = 0.101;

    @BeforeEach
    void setUp() {
        ringsFactory = new PumpRingsFactory();
        bodyFactory = new RCVTypeFactory();
        miniXLH = bodyFactory.provideRcvType("MINI XL_H", 70.0);
        engineRevs = Arrays.asList(600, 800, 850, 900, 950, 1000, 1050, 1100, 1150, 1200);
        ratio = new PtoRatio();
        ratio.setRatio(0.95);
        ratio.setType(PtoType.ENGINE);
        rings = Arrays.asList( ringsFactory.providePumpRing("B10", 34.1), ringsFactory.providePumpRing("B12", 37.1),
                ringsFactory.providePumpRing("B14", 46), ringsFactory.providePumpRing("B17", 58.3)
                , ringsFactory.providePumpRing("B20", 63.8), ringsFactory.providePumpRing("B22", 70.3)
                , ringsFactory.providePumpRing("B24", 79.5), ringsFactory.providePumpRing("B25", 79.3)
                , ringsFactory.providePumpRing("P70", 70), ringsFactory.providePumpRing("P35", 35));
    }

    private List<PumpRings> calculateProperPumpRingsForGivenSpeed(int rev, boolean isP2) {
        List<PumpRings> ringList = new ArrayList<>();
        double calcRingFlow;
        for (PumpRings p: rings) {
            calcRingFlow = ringFlowCalc(p.getFlowRate(), rev);
            if ( isFlowInRange(miniXLH, calcRingFlow, isP2)){
                ringList.add(p);
            }
        }
        return ringList;
    }

    private double ringFlowCalc(double ringFlow, int rSpeed) {
        return rSpeed * ringFlow * ratio.getRatio() / 1000;
    }

    private boolean isFlowInRange(RCVType body ,double calcRingFlow, boolean isP2) {
        double lowerBound = (!isP2 ? body.getFlowRate() : 40.0) + 1;
        double upperBound = lowerBound + maxBodyRatioValue(body.getFlowRate());
        return calcRingFlow >= lowerBound && calcRingFlow <= upperBound;
    }

    private double maxBodyRatioValue(double currentBodyRatio) {
        return currentBodyRatio * delta;
    }

    @Test
    void givenRevs_whenAppliedGivenCondition_thenNewListOfRevsReturned() {
        List<Integer> revs = engineRevs.stream()
                .filter(r -> calculateProperPumpRingsForGivenSpeed(r, false).size() != 0)
                .collect(Collectors.toList());
        System.out.println(revs);
    }

    @Test
    void givenListOfPumpRings_when_thenConvertItToMap() {
        Map<Integer, List<PumpRings>> revs = engineRevs.stream()
                //.filter(r -> calculateProperPumpRingsForGivenSpeed(r, false).size() != 0)
                .collect(Collectors.toMap (
                        (r) -> r,
                        (r) -> calculateProperPumpRingsForGivenSpeed(r,false))
                );
        System.out.println(revs);
    }

    @Test
    void refactorOf_calculateProperPumpRingsForGivenSpeed() {
        List<PumpRings> ringList = rings.stream()
                .collect(ArrayList::new,
                            (l, p) -> {
                                double calc = ringFlowCalc(p.getFlowRate(), 950);
                                if (isFlowInRange(miniXLH, calc, false)) l.add(p);
                            }, ArrayList::addAll
                );

        System.out.println(ringList);
    }
}
