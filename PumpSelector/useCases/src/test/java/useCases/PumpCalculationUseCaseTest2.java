package useCases;

import domain.body.RCVType;
import domain.body.RCVTypeFactory;
import domain.pumpRings.PumpRings;
import domain.pumpRings.PumpRingsFactory;
import domain.pumps.HydraulicPump;
import domain.pumps.PumpFactory;
import domain.pumps.PumpSymbol;
import domain.pumps.Rotation;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.TestInstance;
import org.junit.jupiter.api.extension.ExtendWith;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.MethodSource;
import org.mockito.junit.jupiter.MockitoExtension;
import repository.databaseSource.PtoType;
import repository.databaseSource.entities.PtoRatio;

import java.util.*;
import java.util.stream.Stream;

import static org.junit.jupiter.api.Assertions.assertTrue;
import static org.junit.jupiter.api.TestInstance.Lifecycle.PER_CLASS;


@TestInstance(PER_CLASS)
@ExtendWith(MockitoExtension.class)
class PumpCalculationUseCaseTest2 {

    private RCVTypeFactory bodyFactory;
    private PumpRingsFactory ringsFactory;
    private RCVType rcvBodyType;
    private List<Integer> engineRevs;
    private PtoRatio ratio;
    private List<PumpRings> directMountRings;
    private List<PumpRings> indirectMountRings;
    private double delta = 0.178;
    private PumpFactory pumpFactory;
    private boolean isEcoDelta = true;
    private boolean isP1SuperCharged;
    private boolean isPumpDirectMount = true;

    @BeforeAll
    void setUp() {
        bodyFactory = new RCVTypeFactory();
        ringsFactory = new PumpRingsFactory();
        pumpFactory = new PumpFactory();
        //rcvBodyType = bodyFactory.provideRcvType("MINI XL_H", 70.0);
        rcvBodyType = bodyFactory.provideRcvType("MEDIUM XXL", 100.0);
        engineRevs = Arrays.asList(600, 800, 850, 900, 950, 1000, 1050, 1100, 1150, 1200);
        ratio = new PtoRatio();
        ratio.setRatio(1.19);
        ratio.setType(PtoType.ENGINE);
        if (rcvBodyType.getType()
                .equalsIgnoreCase("medium xxl") || rcvBodyType.getType()
                .equalsIgnoreCase("medium xxxl")) isP1SuperCharged = true;

        directMountRings = Arrays.asList( ringsFactory.providePumpRing("B10", 34.1),
                ringsFactory.providePumpRing("B12", 37.1),
                ringsFactory.providePumpRing("B14", 46), ringsFactory.providePumpRing("B17", 58.3)
                , ringsFactory.providePumpRing("B20", 63.8), ringsFactory.providePumpRing("B25", 79.3)
                , ringsFactory.providePumpRing("P70", 70), ringsFactory.providePumpRing("P35", 35));

        indirectMountRings = Arrays.asList( ringsFactory.providePumpRing("B10", 34.1),
                ringsFactory.providePumpRing("B12", 37.1),
                ringsFactory.providePumpRing("B14", 46), ringsFactory.providePumpRing("B17", 58.3)
                , ringsFactory.providePumpRing("B20", 63.8), ringsFactory.providePumpRing("B22", 70.3)
                , ringsFactory.providePumpRing("B24", 79.3));

    }

    Stream<PumpRings> directRingsProvider() {
        return directMountRings.stream();
    }

    @ParameterizedTest
    @MethodSource({"directRingsProvider"})
    void givenRingFlowAndEngineSpeed_whenEcoAndChargeIsFalse_thenReturnTrueIfP1InRange(PumpRings ring) {
        double calculatedFlow = ringFlowCalc(ring.getFlowRate(), 600);
        System.out.println(ring.getSymbol()+": "+calculatedFlow+": "+ isRingFlowInRange(calculatedFlow, true));
    }

    @Test
    void givenPumpRing_when_isFlowInRangeCalled_trueReturned(){
        double calculatedFlowP1 = ringFlowCalc(79.3, 950);
        double calculatedFlowP2 = ringFlowCalc(79.3, 600);
        assertTrue(isRingFlowInRange(calculatedFlowP1, false));
        assertTrue(isRingFlowInRange(calculatedFlowP2, true));
    }

    @Test
    void generatePumps_prototype() {
        var createdPumpsForGivenRevs = new HashMap<Integer, List<HydraulicPump>>();
        List<HydraulicPump> listOfPumps;
        for (var rev : engineRevs) {
            if (pumpShaftSpeed(rev) < 900) continue;
            listOfPumps =  createPumpForGivenRevSpeed(rev);
            if (!listOfPumps.isEmpty()) createdPumpsForGivenRevs.put(rev, listOfPumps);
        }
        System.out.println("the map of pumps for speed: "+ createdPumpsForGivenRevs);
    }

    List<HydraulicPump> createPumpForGivenRevSpeed(int rev){
        var listOfPumps = new ArrayList<HydraulicPump>();
        HydraulicPump pump;
        for (PumpRings r1 : directMountRings) {
            double p1calculatedFlow = ringFlowCalc(r1.getFlowRate(), rev);
            if (!isRingFlowInRange(p1calculatedFlow, false)) continue;
            for (PumpRings r2 : directMountRings) {
                if (r1.getSymbol().charAt(0) != r2.getSymbol().charAt(0) ) continue;
                if (isEcoDelta) {
                    double p2calculatedFlow = p2ecoDeltaFlowCalc(r2.getFlowRate());
                    if (!isP2InEcoRange(p2calculatedFlow)) continue;
                    pump = createPump(r1, r2);
                } else {
                    double p2calculatedFlow = ringFlowCalc(r2.getFlowRate(), rev);
                    if (!isRingFlowInRange(p2calculatedFlow, true)) continue;
                    pump = createPump(r1, r2);
                }
                listOfPumps.add(pump);
            }
        }
        return listOfPumps;
    }

    private HydraulicPump createPump(PumpRings r1, PumpRings r2) {
        PumpSymbol symbol = isPumpDirectMount ? PumpSymbol.T6GCC : PumpSymbol.T6DCMY;
        return ((r1.getSymbol().charAt(0) == 'P') && r2.getSymbol().charAt(0) == 'P')
                ? pumpFactory.createPump(PumpSymbol.PARKER, r1, r2, Rotation.R)
                : pumpFactory.createPump(symbol, r1, r2, Rotation.R);
    }

    @Test
    void test_chargeP1FromP2() {
        var createdPumpsForGivenRevs = new HashMap<Integer, List<HydraulicPump>>();
        List<HydraulicPump> listOfPumps;
        for (var rev : engineRevs) {
            if (pumpShaftSpeed(rev) < 900) continue;
            listOfPumps = createPumpsWithP1SuperChargedEcoDelta(rev);
            if (!listOfPumps.isEmpty()) createdPumpsForGivenRevs.put(rev, listOfPumps);
        }
        System.out.println("the map of pumps for speed: "+ createdPumpsForGivenRevs);
    }

    List<HydraulicPump> createPumpsWithP1SuperChargedEcoDelta(int revs) {
        var listOfPumps = new ArrayList<HydraulicPump>();
        for (var r1 : directMountRings) {
            double p1calculatedFlow = ringFlowCalc(r1.getFlowRate(), revs);
            if (p1calculatedFlow < 74) continue;
            for (var r2 : directMountRings) {
                if (r1.getSymbol().charAt(0) != r2.getSymbol().charAt(0) ) continue;
                if (isRingFlowInRange(p1calculatedFlow, false)) {
                    double p2CalculatedFlow = p2ecoDeltaFlowCalc(r2.getFlowRate());
                    if (!isP2InEcoRange(p2CalculatedFlow)) continue;
                    listOfPumps.add(createPump(r1, r2));
                } else {
                    if (p2ecoDeltaFlowCalc(r2.getFlowRate()) < 32) continue;
                    double p2CalculatedFlow = ringFlowCalc(r2.getFlowRate(), revs);
                    if (!isP2InSuperchargedRange(p1calculatedFlow, p2CalculatedFlow)) continue;
                    listOfPumps.add(createPump(r1, r2));
                }
            }
        }
        return listOfPumps;
    }

    // testing only supercharge of P1
    @Test
    void testOnlyP1SuperCharge() {
        var createdPumpsForGivenRevs = new HashMap<Integer, List<HydraulicPump>>();
        List<HydraulicPump> listOfPumps;
        for (var rev : engineRevs) {
            if (pumpShaftSpeed(rev) < 900) continue;
            listOfPumps = createPumpsWithP1SuperCharged(rev);
            if (!listOfPumps.isEmpty()) createdPumpsForGivenRevs.put(rev, listOfPumps);
        }
        System.out.println("the map of pumps for speed: "+ createdPumpsForGivenRevs);
    }

    List<HydraulicPump> createPumpsWithP1SuperCharged(int revs) {
        var listOfPumps = new ArrayList<HydraulicPump>();
        for (var r1 : directMountRings) {
            double p1calculatedFlow = ringFlowCalc(r1.getFlowRate(), revs);
            if (p1calculatedFlow < 74) continue;
            for (var r2 : directMountRings) {
                if (r1.getSymbol().charAt(0) != r2.getSymbol().charAt(0)) continue;
                double p2CalculatedFlow = ringFlowCalc(r2.getFlowRate(), revs);
                if (isRingFlowInRange(p1calculatedFlow, false)) {
                    if (!isRingFlowInRange(p2CalculatedFlow, true)) continue;
                    listOfPumps.add(createPump(r1, r2));
                } else {
                    if (p2ecoDeltaFlowCalc(r2.getFlowRate()) < 32) continue;
                    if (!isP2InSuperchargedRange(p1calculatedFlow, p2CalculatedFlow)) continue;
                    listOfPumps.add(createPump(r1, r2));
                }
            }
        }
        return listOfPumps;
    }


    // helper methods for superChargeP1 mode
    private boolean isP2InSuperchargedRange (double flowP1, double flowP2) {
        boolean result;
        if (rcvBodyType.getFlowRate() >= 100 && rcvBodyType.getFlowRate() <= 110){
            result = (flowP1 + (flowP2 - 48)) >= 101 && (flowP1 + (flowP2 - 48)) <= 116;
        } else  {
            result = (flowP1 + (flowP2 - 48)) >= 131 && (flowP1 + (flowP2 - 48)) <= 146;
        }
        return result;
    }

    // helper methods for eco delta calculation
    private double p2ecoDeltaFlowCalc(double ringFlow) {
        return 600 * ringFlow * ratio.getRatio() / 1000;
    }

    private boolean isP2InEcoRange(double calcRingFlow) {
        return calcRingFlow >= 32 && calcRingFlow <= 50;
    }

    // helper methods for standard calculation
    private boolean isRingFlowInRange(double calcRingFlow, boolean isP2) {
        double lowerBound = (!isP2 ? rcvBodyType.getFlowRate() : 40)+1;
        double upperBound = lowerBound + maxBodyRatioValue();
        return calcRingFlow >= lowerBound && calcRingFlow <= upperBound;
    }

    private double maxBodyRatioValue() {
        return rcvBodyType.getFlowRate() * delta;
    }

    private int pumpShaftSpeed (int rev) {
        return (int) (rev * ratio.getRatio());
    }

    private double ringFlowCalc(double ringFlow, int rSpeed) {
        return rSpeed * ringFlow * ratio.getRatio() / 1000;
    }

}