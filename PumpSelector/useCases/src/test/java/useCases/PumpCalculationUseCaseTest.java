package useCases;

import domain.body.RCVType;
import domain.body.RCVTypeFactory;
import domain.pumpRings.PumpRings;
import domain.pumpRings.PumpRingsFactory;
import domain.pumps.HydraulicPump;
import domain.pumps.PumpFactory;
import domain.pumps.PumpSymbol;
import domain.pumps.Rotation;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.junit.jupiter.MockitoExtension;
import repository.databaseSource.PtoType;
import repository.databaseSource.entities.PtoRatio;

import java.util.*;

import static org.junit.jupiter.api.Assertions.*;

@ExtendWith(MockitoExtension.class)
class PumpCalculationUseCaseTest {

    private RCVTypeFactory bodyFactory;
    private PumpRingsFactory ringsFactory;
    private RCVType rcvBodyType;
    private List<Integer> engineRevs;
    private PtoRatio ratio;
    private List<PumpRings> rings;
    private double delta = 0.101;
    private PumpFactory pumpFactory;
    private boolean isP1SuperCharged;

    @BeforeEach
    void setUp() {
        pumpFactory = new PumpFactory();
        ringsFactory = new PumpRingsFactory();
        bodyFactory = new RCVTypeFactory();
        rcvBodyType = bodyFactory.provideRcvType("MINI XL_H", 70.0);
        engineRevs = Arrays.asList(600, 800, 850, 900, 950, 1000, 1050, 1100, 1150, 1200);
        ratio = new PtoRatio();
        ratio.setRatio(0.95);
        ratio.setType(PtoType.ENGINE);
        rings = Arrays.asList( ringsFactory.providePumpRing("B10", 34.1), ringsFactory.providePumpRing("B12", 37.1),
                ringsFactory.providePumpRing("B14", 46), ringsFactory.providePumpRing("B17", 58.3)
                , ringsFactory.providePumpRing("B20", 63.8), ringsFactory.providePumpRing("B22", 70.3)
                , ringsFactory.providePumpRing("B24", 79.3), ringsFactory.providePumpRing("B25", 79.3)
                , ringsFactory.providePumpRing("P70", 70), ringsFactory.providePumpRing("P35", 35));
        if (rcvBodyType.getType().equalsIgnoreCase("medium xxl")
                || rcvBodyType.getType().equalsIgnoreCase("medium xxxl")) isP1SuperCharged = true;
    }

    @Test
    void basicTest() {
        double bodyMaxflowValue = maxBodyRatioValue(rcvBodyType.getFlowRate());
        assertEquals(5.6, bodyMaxflowValue, delta);
    }

    @Test
    void givenBodyTypeAndProperEngineRevs_WhenPumpRingFlowIsCalculated_thenItShouldBeWithinGivenRange() {
        double ringFlowVal = ringFlowCalc(70.3, 850);
        assertTrue(isFlowInRange(rcvBodyType, ringFlowVal, false));
        assertFalse(isFlowInRange(rcvBodyType, ringFlowVal, true));
    }

    // main methods
    private boolean isFlowInRange(RCVType body ,double calcRingFlow, boolean isP2) {
        double lowerBound = (!isP2 ? body.getFlowRate() : 40.0) + 1;
        double upperBound = lowerBound + maxBodyRatioValue(body.getFlowRate());
        return calcRingFlow >= lowerBound && calcRingFlow <= upperBound;
    }

    @Test
    void givenInputData_whenCalculatePumpRingForGivenSpeed_returnListOfRingsP1() {
       Map<Integer, Map<PumpRings, List<PumpRings>>> result = MergeRingMapsByRevs();
       System.out.println(result);
    }

    private boolean isP2FlowInEcoRange(PumpRings ring) {
        double ecoDeltaFlow = 600*ring.getFlowRate()*ratio.getRatio() / 1000;
        return ecoDeltaFlow >= 35.0;
    }
    /* result expected :
        P1: rev 950 -> B24, B25         P2: 600 -> B24, B25
        P1: rev 1000 -> B24, B25        P2: 800 -> B17
        P1: rev 1100 -> B22, P70        P2: 850 -> B17
        P1: rev 1150 -> B22, P70        P2: 950 -> B14
        P1: rev 1200 -> B20             P2: 1000 -> B14
                                        P2: 1050 -> B14
                                        P2: 1100 -> B14
                                        P2: 1200 -> B12
     */

    @Test
    // this method creates a pump from given rings
    void pompCreation() {
        Map<Integer, Map<PumpRings, List<PumpRings>>> merged = MergeRingMapsByRevs();
        Map<Integer, List<HydraulicPump>> pumps = new HashMap<>();
        List<HydraulicPump> hydraulicPumps = new ArrayList<>();

        for (Map.Entry<Integer, Map<PumpRings, List<PumpRings>>> entry: merged.entrySet()) { // speed
            for (Map.Entry<PumpRings, List<PumpRings>> rings : entry.getValue().entrySet() ) { // p1
                for (PumpRings p : rings.getValue()) { // p2
                    if (rings.getKey().getSymbol().charAt(0) != (p.getSymbol().charAt(0)) ) continue;
                    System.out.println("Speed: "+entry.getKey()+" " +"ring p1: "+ rings.getKey() + " ring p2: "+p);
                    // adding the F2 pump if is present
                    if ((rings.getKey().getSymbol().charAt(0) == 'P') && ((p.getSymbol().charAt(0)) == 'P')) {
                        hydraulicPumps.add( pumpFactory.createPump(PumpSymbol.PARKER, rings.getKey(), p, Rotation.R ));
                    } else {
                        hydraulicPumps.add(pumpFactory.createPump(PumpSymbol.T6GCC, rings.getKey(), p, Rotation.R));
                    }
                }
            }
            pumps.put(entry.getKey(), new ArrayList<>(hydraulicPumps));
            hydraulicPumps.clear();
        }
        System.out.println("************** pumps ************" + "\n");
        System.out.println(pumps);
    }

    private Map<Integer, Map<PumpRings, List<PumpRings>>> MergeRingMapsByRevs(){
        Map<Integer, List<PumpRings>> mapP1 = revToPumpRings(false, false);
        Map<Integer, List<PumpRings>> mapP2 = revToPumpRings(true, false);
        if (mapP1.isEmpty() ) {
        }
        Map<PumpRings, List<PumpRings>> p1AndP2 = new HashMap<>();
        Map<Integer, Map<PumpRings, List<PumpRings>>> pumpMap = new HashMap<>();
        for (Map.Entry<Integer, List<PumpRings>> p1E: mapP1.entrySet()) {
           if (mapP2.containsKey(p1E.getKey())) {
               for (PumpRings p: p1E.getValue()) {
                   p1AndP2.put(p, mapP2.get(p1E.getKey()));
               }
               pumpMap.put(p1E.getKey(), new HashMap<>(p1AndP2));
           }
           p1AndP2.clear();
        }
        return pumpMap;
    }

    private Map<Integer, List<PumpRings>> revToPumpRings (boolean isP2, boolean isEcoDelta) {
        Map<Integer, List<PumpRings>> revsAndRings = new HashMap<>();
        List<PumpRings> rList;
        for (int rev: engineRevs) {
            if (pumpShaftSpeed(rev, ratio.getRatio()) < 900) continue;
            rList = calculateProperPumpRingsForGivenSpeed(rev, isP2, isEcoDelta);
            if (rList.size() != 0) {
                revsAndRings.put(rev, rList);
            }
        }
        return revsAndRings;
    }

    private List<PumpRings> calculateProperPumpRingsForGivenSpeed(int rev, boolean isP2, boolean isEcoDelta) {
        List<PumpRings> ringList = new ArrayList<>();
        double calcRingFlow;
        for (PumpRings p: rings) {
            calcRingFlow = ringFlowCalc(p.getFlowRate(), rev);
            if (isEcoDelta ) {
               if (isP2FlowInEcoRange(p) && isFlowInRange(rcvBodyType, calcRingFlow, false)) ringList.add(p);

            } else if (isP1SuperCharged ) {
                if (isP2 && (calcRingFlow - 40) >= 41) {
                    ringList.add(p);
                }else  {
                    if ((!isFlowInRange(rcvBodyType, calcRingFlow, false) && calcRingFlow >= 85.00)) {
                        ringList.add(p);
                    }
                }
            } else {
                if ( isFlowInRange(rcvBodyType, calcRingFlow, isP2) ) {
                   ringList.add(p);
               }
            }
        }
        return ringList;
    }

    // helper methods
    private double maxBodyRatioValue(double currentBodyRatio) {
        return currentBodyRatio * delta;
    }

    private int pumpShaftSpeed (int rev, double ptoRatio) {
        return (int) (rev * ptoRatio);
    }

    private double ringFlowCalc(double ringFlow, int rSpeed) {
        return rSpeed * ringFlow * ratio.getRatio() / 1000;
    }
}
