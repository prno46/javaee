package useCases;

import domain.body.RCVType;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import repository.databaseSource.PtoType;
import repository.databaseSource.entities.BodyType;
import repository.databaseSource.entities.ChassisBrand;
import repository.databaseSource.entities.PtoRatio;
import repository.databaseSource.entities.PumpRingE;
import repository.domainRepository.DomainRepository;

import javax.annotation.PostConstruct;
import java.util.Collections;
import java.util.List;

@Service
public class LoadBasicDataUseCase {

    private final DomainRepository repository;
    private List<PtoRatio> ratios;
    private List<RCVType> rcvTypes;
    private List<ChassisBrand> chassisBrands;

    @Autowired
    public LoadBasicDataUseCase(DomainRepository repository) {
        this.repository = repository;
    }

    @PostConstruct
    private void loadDataFromRepository() {
        ratios = repository.loadAllPtoRatios();
        rcvTypes = repository.loadAllRcvTypes();
        chassisBrands = repository.loadAllChassisBrands();
    }

    // immutable list
    public List<RCVType> loadAllRcvTypes() {
        return  (rcvTypes != null) ? List.copyOf(rcvTypes) : Collections.emptyList();
    }

    // immutable list
    public List<PtoRatio> loadAllPtoRatios() {
        return (ratios != null) ? List.copyOf(ratios) : Collections.emptyList();
    }

    // immutable list
    public List<ChassisBrand> loadAllChassisBrands() {
        return (chassisBrands != null) ? List.copyOf(chassisBrands) : Collections.emptyList();
    }

    public void saveNewPto(double value, PtoType type) {
        repository.saveNewPto(value, type);
    }

    public void saveNewPtoEntity(PtoRatio entity) {
        repository.saveNewPtoEntity(entity);
    }

    public void saveNewBodyType (String symbol, double flowRatio) {
        repository.saveNewBodyType(symbol, flowRatio);
    }

    public void saveNewBodyEntity(BodyType entity) {
        repository.saveNewBodyTypeEntity(entity);
    }

    public void saveNewChassisBrand(String brand) {
        repository.saveNewChassisBrand(brand);
    }

    public void saveNewChassisBrandEntity(ChassisBrand entity) {
        repository.saveNewChassisBrandEntity(entity);
    }
    public void saveNewPumpRing(String symbol, double ratio) {
        repository.saveNewPumpRing(symbol, ratio);
    }

    public void saveNewPumpRingEntity(PumpRingE entity) {
        repository.saveNewPumpRingEntity(entity);
    }
}
