package useCases;


import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import useCases.models.CalculationInputModel;
import useCases.calculationOptions.OptionFactory;
import useCases.calculationOptions.PumpCalculationResult;

import java.util.Collections;
import java.util.List;
import java.util.Map;

@Service
public class PumpCalculationUseCase {
    private Logger log = LoggerFactory.getLogger(getClass().getSimpleName());

    private OptionFactory calculation;
    private Map<Integer, List<PumpCalculationResult>> resultMap;
    private CalculationInputModel model;

    @Autowired
    public PumpCalculationUseCase(OptionFactory calculation) {
        this.calculation = calculation;
    }

    public void setUserInput (CalculationInputModel model) {
        this.model = model;
        calculationInit();
    }

    private void calculationInit() {
        if (model == null) System.err.println("Check if the mode has been set");
        calculation.setCalculationModel(model);
        resultMap = Map.copyOf(calculation.getResultMap());
    }

    public Map<Integer, List<PumpCalculationResult>> getCalculatedPumpsMap() {
        return  (model != null) ? resultMap : Collections.emptyMap();
    }
}
