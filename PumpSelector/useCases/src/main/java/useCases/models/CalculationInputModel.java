package useCases.models;

import domain.pumps.Rotation;
import useCases.calculationOptions.PumpCalculationResult;

import java.io.Serializable;
import java.util.Collections;
import java.util.List;
import java.util.Map;

public class CalculationInputModel implements Serializable {
    private static long serialVersionUID = 46L;

    private String rcvType;
    private double ptoRatio;
    private Rotation rotationDirection;
    private boolean isDirectMount;
    private boolean hasEcoDelta;

    private Map<Integer, List<PumpCalculationResult>> calculatedPumps;

    public CalculationInputModel() { }

    public String getRcvType() {
        return rcvType;
    }

    public void setRcvType(String rcvType) {
        this.rcvType = rcvType;
    }

    public double getPtoRatio() {
        return ptoRatio;
    }

    public void setPtoRatio(double ptoRatio) {
        this.ptoRatio = ptoRatio;
    }

    // for spring the getter has to start with get... so Spring could find it
    public boolean getIsDirectMount() {
        return isDirectMount;
    }

    public void setIsDirectMount(boolean directMount) {
        isDirectMount = directMount;
    }

    // for spring the getter has to start with get... so Spring could find it
    public boolean getHasEcoDelta() {
        return hasEcoDelta;
    }

    public void setHasEcoDelta(boolean hasEcoDelta) {
        this.hasEcoDelta = hasEcoDelta;
    }

    public Rotation getRotationDirection() {
        return rotationDirection;
    }

    public void setRotationDirection(Rotation rotationDirection) {
        this.rotationDirection = rotationDirection;
    }

    public Map<Integer, List<PumpCalculationResult>> getCalculatedPumps() {
        return (calculatedPumps != null) ? Map.copyOf(calculatedPumps) : Collections.emptyMap();
    }

    public void setCalculatedPumps(Map<Integer, List<PumpCalculationResult>> calculatedPumps) {
        this.calculatedPumps = calculatedPumps;
    }
}
