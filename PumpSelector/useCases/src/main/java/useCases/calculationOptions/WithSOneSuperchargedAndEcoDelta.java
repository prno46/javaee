package useCases.calculationOptions;

import domain.body.RCVType;
import domain.pumpRings.PumpRings;
import domain.pumps.PumpFactory;
import useCases.models.CalculationInputModel;

import java.util.*;

class WithSOneSuperchargedAndEcoDelta extends BaseOption {

    WithSOneSuperchargedAndEcoDelta(RCVType rcvType, List<PumpRings> pumpRings,
                                           PumpFactory factory, CalculationInputModel model) {
        super(rcvType, pumpRings, factory, model);
    }

    @Override
    public Map<Integer, List<PumpCalculationResult>> calculate() {
        Map<Integer, List<PumpCalculationResult>> createdPumpsForGivenRevs = new TreeMap<>();
        List<PumpCalculationResult> listOfPumps;
        for (int rev : engineRevs) {
            if (pumpShaftSpeed(rev) < PUMP_SHAFT_MIN_REV_VALUE) continue;
            listOfPumps = createPumpsWithP1SuperChargedAndEcoDelta(rev);
            if (!listOfPumps.isEmpty()) createdPumpsForGivenRevs.put(rev, listOfPumps);
        }
        return createdPumpsForGivenRevs;
    }

    private List<PumpCalculationResult> createPumpsWithP1SuperChargedAndEcoDelta(Integer revs) {
        List<PumpCalculationResult> listOfPumps = new ArrayList<>();
        for (PumpRings r1 : pumpRings) {
            double p1calculatedFlow = ringFlowCalc(r1.getFlowRate(), revs);
            if (p1calculatedFlow < 74) continue;
            for (PumpRings r2 : pumpRings) {
                if (r1.getSymbol().charAt(0) != r2.getSymbol().charAt(0) ) continue;
                if (isRingFlowInRange(p1calculatedFlow, false)) {
                    double p2CalculatedFlow = p2ecoDeltaFlowCalc(r2.getFlowRate());
                    if (!isP2InEcoRange(p2CalculatedFlow)) continue;
                    listOfPumps
                            .add( new PumpCalculationResultImpl(createPump(r1, r2), p1calculatedFlow, p2CalculatedFlow) );
                } else {
                    if (p2ecoDeltaFlowCalc(r2.getFlowRate()) < 32) continue;
                    double p2CalculatedFlow = ringFlowCalc(r2.getFlowRate(), revs);
                    if (!isP2InSuperchargedRange(p1calculatedFlow, p2CalculatedFlow)) continue;
                    listOfPumps
                            .add(new PumpCalculationResultImpl(createPump(r1, r2), p1calculatedFlow, p2CalculatedFlow));
                }
            }
        }
        return listOfPumps;
    }

    private boolean isP2InSuperchargedRange(double flowP1, double flowP2) {
        boolean result;
        if (rcvType.getFlowRate() >= MXXL_P1_FLOW_MIN_MODEL_VALUE
                && rcvType.getFlowRate() <= MXXL_P1_FLOW_MAX_MODEL_VALUE) {
            result = (flowP1 + (flowP2 - CHARGE_FACTOR)) >= MXXL_P1_FLOW_MIN
                    && (flowP1 + (flowP2 - CHARGE_FACTOR)) <= MXXL_P1_FLOW_MAX;
        } else {
            result = (flowP1 + (flowP2 - CHARGE_FACTOR)) >= MXXXL_P1_FLOW_MIN
                    && (flowP1 + (flowP2 - CHARGE_FACTOR)) <= MXXXL_P1_FLOW_MAX;
        }
        return result;
    }

    private double p2ecoDeltaFlowCalc(double ringFlow) {
        return ECODELTA_ENGINE_SPEED * ringFlow * model.getPtoRatio() / 1000;
    }

    private boolean isP2InEcoRange(double calcRingFlow) {
        return calcRingFlow >= ECO_LOWER_LIMIT && calcRingFlow <= ECO_UPPER_LIMIT;
    }
}
