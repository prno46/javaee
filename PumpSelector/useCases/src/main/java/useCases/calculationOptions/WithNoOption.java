package useCases.calculationOptions;

import domain.body.RCVType;
import domain.pumpRings.PumpRings;
import domain.pumps.PumpFactory;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import useCases.models.CalculationInputModel;

import java.util.*;

class WithNoOption extends BaseOption {

    private Logger log = LoggerFactory.getLogger(getClass().getSimpleName());

    WithNoOption(RCVType rcvType, List<PumpRings> pumpRings, PumpFactory factory, CalculationInputModel model) {
        super(rcvType, pumpRings,  factory, model);
    }

    @Override
    public Map<Integer, List<PumpCalculationResult>> calculate() {
        Map<Integer, List<PumpCalculationResult>> createdPumpsForGivenRevs = new TreeMap<>();
        List<PumpCalculationResult> listOfPumps;
        for (int rev : engineRevs) {
            if (pumpShaftSpeed(rev) < PUMP_SHAFT_MIN_REV_VALUE) continue;
            listOfPumps =  createPumpForGivenRevSpeed(rev);
            if (!listOfPumps.isEmpty()) createdPumpsForGivenRevs.put(rev, listOfPumps);
        }
        return createdPumpsForGivenRevs;
    }

    private List<PumpCalculationResult> createPumpForGivenRevSpeed(int rev){
        List<PumpCalculationResult> listOfPumps = new ArrayList<>();
        for (PumpRings r1 : pumpRings) {
            double p1calculatedFlow = ringFlowCalc(r1.getFlowRate(), rev);
            if (!isRingFlowInRange(p1calculatedFlow, false)) continue;
            for (PumpRings r2 : pumpRings) {
                if (r1.getSymbol().charAt(0) != r2.getSymbol().charAt(0) ) continue;
                double p2calculatedFlow = ringFlowCalc(r2.getFlowRate(), rev);
                if (!isRingFlowInRange(p2calculatedFlow, true)) continue;
                listOfPumps
                        .add(new PumpCalculationResultImpl(createPump(r1, r2), p1calculatedFlow, p2calculatedFlow));
            }
        }
        return listOfPumps;
    }
}
