package useCases.calculationOptions;

import domain.body.RCVType;
import domain.pumpRings.PumpRings;
import domain.pumps.PumpFactory;
import useCases.models.CalculationInputModel;

import java.util.*;

class OnlyWithEcoDelta extends BaseOption {



    OnlyWithEcoDelta(RCVType rcvType, List<PumpRings> pumpRings, PumpFactory factory, CalculationInputModel model) {
        super(rcvType, pumpRings, factory, model);
    }

    @Override
    public Map<Integer, List<PumpCalculationResult>> calculate() {
        Map<Integer, List<PumpCalculationResult>> createdPumpsForGivenRevs = new TreeMap<>();
        List<PumpCalculationResult> listOfPumps;
        for (int rev : engineRevs) {
            if (pumpShaftSpeed(rev) < 900) continue;
            listOfPumps =  createPumpForGivenRevSpeed(rev);
            if (!listOfPumps.isEmpty()) createdPumpsForGivenRevs.put(rev, listOfPumps);
        }
        return createdPumpsForGivenRevs;
    }

    private List<PumpCalculationResult> createPumpForGivenRevSpeed(int rev){
        List<PumpCalculationResult> listOfPumps = new ArrayList<>();
        for (PumpRings r1 : pumpRings) {
            double p1calculatedFlow = ringFlowCalc(r1.getFlowRate(), rev);
            if (!isRingFlowInRange(p1calculatedFlow, false)) continue;
            for (PumpRings r2 : pumpRings) {
                if (r1.getSymbol().charAt(0) != r2.getSymbol().charAt(0) ) continue;
                double p2calculatedFlow = p2ecoDeltaFlowCalc(r2.getFlowRate());
                if (!isP2InEcoRange(p2calculatedFlow)) continue;
                listOfPumps
                        .add(new PumpCalculationResultImpl(createPump(r1, r2), p1calculatedFlow, p2calculatedFlow));
            }
        }
        return listOfPumps;
    }

    private double p2ecoDeltaFlowCalc(double ringFlow) {
        return 600 * ringFlow * model.getPtoRatio() / 1000;
    }

    private boolean isP2InEcoRange(double calcRingFlow) {
        return calcRingFlow >= ECO_LOWER_LIMIT && calcRingFlow <= ECO_UPPER_LIMIT;
    }

}
