package useCases.calculationOptions;

import domain.body.RCVType;
import domain.pumpRings.PumpRings;
import domain.pumps.PumpFactory;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import repository.domainRepository.DomainRepository;
import useCases.LoadBasicDataUseCase;
import useCases.models.CalculationInputModel;

import java.util.List;
import java.util.Map;

@Service
public class OptionFactory {

    private Logger log = LoggerFactory.getLogger(getClass().getSimpleName());

    private final DomainRepository repository;
    private final PumpFactory factory;
    private final LoadBasicDataUseCase basicData;
    private CalculationInputModel model;
    private RCVType rcvType;
    private List<PumpRings> pumpRings;

    @Autowired
    public OptionFactory(DomainRepository repository, PumpFactory factory, LoadBasicDataUseCase basicData) {
        this.repository = repository;
        this.factory = factory;
        this.basicData = basicData;
    }

    public void setCalculationModel(CalculationInputModel model) {
        this.model = model;
        rcvType = getRcvTypeByName(model.getRcvType());
        pumpRings = repository.fetchAllRingsWithCriteria(model.getIsDirectMount());
        log.info("the loaded list of rings: {}", pumpRings);
    }

    public Map<Integer, List<PumpCalculationResult>> getResultMap() {
        Map<Integer, List<PumpCalculationResult>> resultMap;
        if ( rcvType.getFlowRate() >= 100 && model.getHasEcoDelta()) {
            resultMap = new WithSOneSuperchargedAndEcoDelta(rcvType, pumpRings, factory, model).calculate();
        } else if (rcvType.getFlowRate() >= 100) {
            resultMap = new WithSectionOneSupercharged(rcvType, pumpRings, factory, model).calculate();
        } else if (model.getHasEcoDelta()) {
            resultMap = new OnlyWithEcoDelta(rcvType, pumpRings, factory, model).calculate();
        } else {
            resultMap = new WithNoOption(rcvType, pumpRings, factory, model).calculate();
        }
        return resultMap;
    }

    private RCVType getRcvTypeByName(String rcvName) {
        List<RCVType> types = basicData.loadAllRcvTypes();
        return types.stream()
                .filter(rcv -> rcv.getType().equalsIgnoreCase(rcvName))
                .findFirst()
                .orElseThrow();
    }
}
