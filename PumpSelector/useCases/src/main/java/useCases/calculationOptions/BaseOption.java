package useCases.calculationOptions;

import domain.body.RCVType;
import domain.pumpRings.PumpRings;
import domain.pumps.HydraulicPump;
import domain.pumps.PumpFactory;
import domain.pumps.PumpSymbol;
import useCases.models.CalculationInputModel;

import java.util.Arrays;
import java.util.List;
import java.util.Map;

public abstract class BaseOption {

    protected final double DELTA = 0.178;
    private final int P2_MIN_VALUE = 40;
    protected final int PUMP_SHAFT_MIN_REV_VALUE = 900;
    protected final int ECODELTA_ENGINE_SPEED = 600;
    protected final int ECO_DELTA_MIN_LIMIT = 32;
    protected final int P1_SUPERCHARGE_FROM_VALUE = 74;
    protected final int MXXL_P1_FLOW_MIN_MODEL_VALUE = 100;
    protected final int MXXL_P1_FLOW_MAX_MODEL_VALUE = 110;
    protected final int CHARGE_FACTOR = 48;
    protected final int MXXL_P1_FLOW_MIN = 101;
    protected final int MXXL_P1_FLOW_MAX = 116;
    protected final int MXXXL_P1_FLOW_MIN = 131;
    protected final int MXXXL_P1_FLOW_MAX = 146;
    protected final int ECO_LOWER_LIMIT = 32;
    protected final int ECO_UPPER_LIMIT = 50;

    private final PumpFactory factory;
    protected RCVType rcvType;
    protected List<Integer> engineRevs;
    protected List<PumpRings> pumpRings;
    protected CalculationInputModel model;

    BaseOption(RCVType rcvType, List<PumpRings> pumpRings,  PumpFactory factory, CalculationInputModel model) {
        this.factory = factory;
        this.rcvType = rcvType;
        this.pumpRings = pumpRings;
        this.model = model;
        engineRevs = Arrays.asList(600, 850, 900, 950, 1000, 1050, 1100, 1150, 1200);
    }

    public abstract Map<Integer, List<PumpCalculationResult>> calculate();

    protected HydraulicPump createPump(PumpRings r1, PumpRings r2) {
        PumpSymbol symbol = model.getIsDirectMount() ? PumpSymbol.T6GCC : PumpSymbol.T6DCMY;
        return ((r1.getSymbol().charAt(0) == 'P') && r2.getSymbol().charAt(0) == 'P')
                ? factory.createPump(PumpSymbol.PARKER, r1, r2, model.getRotationDirection())
                : factory.createPump(symbol, r1, r2, model.getRotationDirection());
    }

    protected boolean isRingFlowInRange(double calcRingFlow, boolean isP2) {
        double lowerBound = (!isP2 ? rcvType.getFlowRate() : P2_MIN_VALUE)+1;
        double upperBound = lowerBound + maxBodyRatioValue();
        return calcRingFlow >= lowerBound && calcRingFlow <= upperBound;
    }

    protected double maxBodyRatioValue() {
        return rcvType.getFlowRate() * DELTA;
    }

    protected int pumpShaftSpeed (int rev) {
        return (int) (rev * model.getPtoRatio());
    }

    protected double ringFlowCalc(double ringFlow, int rSpeed) {
        return rSpeed * ringFlow * model.getPtoRatio() / 1000;
    }
}
