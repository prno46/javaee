package useCases.calculationOptions;

import domain.pumps.HydraulicPump;

public class PumpCalculationResultImpl implements PumpCalculationResult {

    private final HydraulicPump hydraulicPump;
    private final double p1CalculatedFlow;
    private final double p2CalculatedFlow;

    PumpCalculationResultImpl(HydraulicPump hydraulicPump, double p1CalculatedFlow, double p2CalculatedFlow) {
        this.hydraulicPump = hydraulicPump;
        this.p1CalculatedFlow = p1CalculatedFlow;
        this.p2CalculatedFlow = p2CalculatedFlow;
    }

    @Override
    public double getP1CalculatedFlow() {
        return p1CalculatedFlow;
    }

    @Override
    public double getP2CalculatedFlow() {
        return p2CalculatedFlow;
    }

    @Override
    public String getPumpSymbol() {
        StringBuilder sb = new StringBuilder();
        return sb.append(hydraulicPump.getPumpSymbol()).append(" ")
                .append(hydraulicPump.getP1().getSymbol()).append(" ")
                .append(hydraulicPump.getP2().getSymbol()).append(" ")
                .append(hydraulicPump.getRotation()).toString();
    }
}
