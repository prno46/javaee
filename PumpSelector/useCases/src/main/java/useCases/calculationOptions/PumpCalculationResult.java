package useCases.calculationOptions;

public interface PumpCalculationResult {
    double getP1CalculatedFlow();
    double getP2CalculatedFlow();
    String getPumpSymbol();
}
