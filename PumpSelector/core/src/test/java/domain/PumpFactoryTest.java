
package domain;

import domain.pumpRings.PumpRings;
import domain.pumpRings.PumpRingsFactory;
import domain.pumps.HydraulicPump;
import domain.pumps.PumpFactory;
import domain.pumps.PumpSymbol;
import domain.pumps.Rotation;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.junit.jupiter.api.function.Executable;
import org.mockito.junit.jupiter.MockitoExtension;

import static org.junit.jupiter.api.Assertions.*;


@ExtendWith(MockitoExtension.class)
class PumpFactoryTest {

    private PumpRings r1;
    private PumpRings r2;
    private Rotation rotation;
    private PumpSymbol gcc;
    private PumpSymbol dcmy;
    private PumpSymbol parker;
    private  PumpRingsFactory factory;

    private PumpFactory SUT;

    @BeforeEach
    void setUp() {
        gcc = PumpSymbol.T6GCC;
        dcmy = PumpSymbol.T6DCMY;
        parker = PumpSymbol.PARKER;
        factory = new PumpRingsFactory();
        r1 = factory.providePumpRing("B20", 63.8);
        r2 = factory.providePumpRing("B12", 37.1);
        rotation = Rotation.R;
        SUT = new PumpFactory();
    }

    //test cases for this class
    // test if T6DCMY is returned correctly

    @Test
    void createPump_createsDCMYPump_shouldReturnTrue() {
        HydraulicPump pump = SUT.createPump(dcmy, r1, r2, rotation);
        assertEquals(dcmy.name(), pump.getPumpSymbol());
    }

    @Test
    void createPump_argumentsPassedToFactoryAreNotNull_returnNotNull() {

        HydraulicPump pump = SUT.createPump(dcmy, r1, r2, rotation);
        assertNotNull(pump.getP1());
        assertNotNull(pump.getP2());
        assertNotNull(pump.getPumpSymbol());
    }

    // test if T6GCC is returned correctly
    @Test
    void createPump_createsGCCPump_shouldReturnTrue() {
        HydraulicPump pump = SUT.createPump(gcc, r1, r2, rotation);
        assertEquals(gcc.name(), pump.getPumpSymbol());
    }

    // test ring two(B2) cannot be bigger flow than ring one (B1)
    @Test
    void createPump_r2FlowMustBeLowerThanR1Flow_shouldReturnTrue() {
        HydraulicPump pump = SUT.createPump(gcc, r1, r2, rotation);
        assertTrue(pump.getP1().getFlowRate() > pump.getP2().getFlowRate());
    }

    @Test
    void createPump_r2FlowBiggerThanR1Flow_throwsException() {
        Executable executable = () -> SUT.createPump(gcc, r2, r1, rotation);
        assertThrows(RuntimeException.class, executable);
    }
    

    // test if PARKER is returned correctly
    @Test
    void createPump_createsParkerPump_shouldReturnTrue() {
        HydraulicPump pump = SUT.createPump(parker, r1, r2, rotation);
        assertEquals(parker.name(), pump.getPumpSymbol());
    }

}
