package main;

import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.PropertySource;

@PropertySource("classpath:ringsConfiguration.properties")
@ComponentScan(basePackages = {"main", "domain"})
@Configuration
class AppCoreConfiguration {


}
