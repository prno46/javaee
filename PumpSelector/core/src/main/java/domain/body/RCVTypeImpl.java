package domain.body;

class RCVTypeImpl implements RCVType {

    private final String rcvType;
    private final double flowRate;

    RCVTypeImpl(String rcvType, double flowRate) {
        this.rcvType = rcvType;
        this.flowRate = flowRate;
    }

    @Override
    public String getType() {
        return rcvType;
    }

    @Override
    public double getFlowRate() {
        return flowRate;
    }

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder(rcvType);
        sb.append("-").append(flowRate);
        return sb.toString();
    }
}
