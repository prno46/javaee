package domain.body;

public interface RCVType {
    String getType();
    double getFlowRate();
}
