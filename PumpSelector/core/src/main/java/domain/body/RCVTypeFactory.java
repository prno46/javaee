package domain.body;

import org.springframework.stereotype.Service;

@Service
public class RCVTypeFactory {

    public RCVTypeFactory() {}

    public RCVType provideRcvType(String type, double flowRate){
        return new RCVTypeImpl(type, flowRate);
    }
}
