package domain.pumpRings;

import org.springframework.stereotype.Service;

@Service
public class PumpRingsFactory {

    public PumpRingsFactory() {}

    public PumpRings providePumpRing(String symbol, double flowRate) {
        return new PumpRingImpl(symbol, flowRate);
    }
}
