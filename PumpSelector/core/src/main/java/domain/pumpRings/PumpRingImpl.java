package domain.pumpRings;

class PumpRingImpl implements PumpRings{
    private final String symbol;
    private final double flowRate;

    public PumpRingImpl(String symbol, double flowRate) {
        this.symbol = symbol;
        this.flowRate = flowRate;
    }

    @Override
    public String getSymbol() {
        return symbol;
    }

    @Override
    public double getFlowRate() {
        return flowRate;
    }

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder(symbol);
        sb.append("-").append(flowRate);
        return sb.toString();
    }
}
