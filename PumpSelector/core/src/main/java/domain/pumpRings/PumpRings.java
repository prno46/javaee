package domain.pumpRings;

public interface PumpRings {
    String getSymbol();
    double getFlowRate();
}
