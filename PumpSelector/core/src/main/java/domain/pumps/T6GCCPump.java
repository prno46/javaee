package domain.pumps;

import domain.pumpRings.PumpRings;

class T6GCCPump extends HydraulicPump {

    T6GCCPump(PumpRings p1, PumpRings p2, Rotation rotation) {
        super(PumpSymbol.T6GCC, p1, p2, rotation);
    }

    @Override
    public String getPumpSymbol() {
        return PumpSymbol.T6GCC.name();
    }

    @Override
    public PumpRings getP1() {
        return p1;
    }

    @Override
    public PumpRings getP2() {
        return p2;
    }

    @Override
    public String getRotation() {
        return rotation.name();
    }

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder(PumpSymbol.T6GCC.name());
        sb.append(" ").append(p1.getSymbol()).append(" ").append(p2.getSymbol()).append(" ").append(rotation.name());
        return sb.toString();
    }
}
