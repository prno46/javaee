package domain.pumps;

import domain.pumpRings.PumpRings;
import org.springframework.lang.NonNull;
import org.springframework.stereotype.Service;

@Service
public class PumpFactory {

    public PumpFactory() { }

    public HydraulicPump createPump(@NonNull PumpSymbol symbol , @NonNull PumpRings r1, @NonNull PumpRings r2, @NonNull Rotation rotation) {
        if( r1.getFlowRate() < r2.getFlowRate()) {
            System.err.println("WARNING! The flowRate of P2 is higher than P1: "+r1.getFlowRate() + " P2: "+r2.getFlowRate());
        }
        HydraulicPump pump;
        switch (symbol) {
            case T6DCMY: pump = new T6DCMYPump(r1, r2, rotation);
                break;
            case T6GCC: pump = new T6GCCPump(r1, r2, rotation);
                break;
            case PARKER: pump = new PARKERPump(r1, r2, rotation);
                break;
            default:
                throw new RuntimeException("Unknown symbol of the pump: " + symbol.name());
        }
        return  pump;
    }
}
