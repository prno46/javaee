package domain.pumps;

public enum PumpSymbol {
    T6DCMY, T6GCC, PARKER
}
