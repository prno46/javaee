package domain.pumps;

import domain.pumpRings.PumpRings;

public abstract class HydraulicPump {

    protected final PumpSymbol symbol;
    protected final PumpRings p1;
    protected final PumpRings p2;
    protected final Rotation rotation;

    HydraulicPump(PumpSymbol symbol, PumpRings p1, PumpRings p2, Rotation rotation) {
        this.symbol = symbol;
        this.p1 = p1;
        this.p2 = p2;
        this.rotation = rotation;
    }

    public abstract String getPumpSymbol();

    public abstract PumpRings getP1();

    public abstract PumpRings getP2();

    public abstract String getRotation();
}
