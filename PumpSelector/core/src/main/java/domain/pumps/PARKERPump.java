package domain.pumps;


import domain.pumpRings.PumpRings;

class PARKERPump extends HydraulicPump {

    PARKERPump(PumpRings p1, PumpRings p2, Rotation rotation) {
        super(PumpSymbol.PARKER, p1, p2, rotation);
    }

    @Override
    public String getPumpSymbol() {
        return PumpSymbol.PARKER.name();
    }

    @Override
    public PumpRings getP1() {
        return p1;
    }

    @Override
    public PumpRings getP2() {
        return p2;
    }

    @Override
    public String getRotation() {
        return rotation.name();
    }

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder(PumpSymbol.PARKER.name());
        sb.append(" ").append(p1.getSymbol()).append(" ").append(p2.getSymbol()).append(" ").append(rotation.name());
        return sb.toString();
    }
}
