package temp;


import java.util.Arrays;
import java.util.List;

class TempMain {

    public static void main(String[] args) {

        List<Some> somes = Arrays.asList(Some.values());

        for(Some s: somes) {
            System.out.println(s);
        }

    }
}

enum Some {
    SOME_ONE, SOME_TWO, SOME_THREE
}