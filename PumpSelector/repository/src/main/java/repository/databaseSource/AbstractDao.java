package repository.databaseSource;


import org.hibernate.Criteria;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.criterion.Criterion;
import org.springframework.beans.factory.annotation.Autowired;

import java.io.Serializable;
import java.lang.reflect.ParameterizedType;
import java.util.List;

public abstract class AbstractDao<T,ID extends Serializable> {

    private Class<T> persistentClass;

    @Autowired
    private  SessionFactory factory;

    @SuppressWarnings("unchecked")
    public AbstractDao(){
        this.persistentClass = (Class<T>) ((ParameterizedType) getClass().getGenericSuperclass()).getActualTypeArguments()[0];
    }

    protected Session getSession(){
        if (factory == null) {
            throw new RuntimeException("SessionFactory was not injected to: "+ getPersistentClass());
        }
        return factory.getCurrentSession();
    }

    protected Class<T> getPersistentClass(){
        return persistentClass;
    }

    protected T findById(ID id) {
        return getSession().load(getPersistentClass(), id);
    }

    protected void save(T entity) {
        getSession().saveOrUpdate(entity);
    }

    protected void delete(T entity) {
        getSession().delete(entity);
    }

    protected void flush() {
        getSession().flush();
    }

    protected void clear() {
        getSession().clear();
    }

    @SuppressWarnings("unchecked")
    protected List<T> findByCriteria(Criterion... criterion) {
        Criteria crit = getSession().createCriteria(getPersistentClass());
        for (Criterion c : criterion) {
            crit.add(c);
        }
        return crit.list();
    }
}