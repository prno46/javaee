package repository.databaseSource.daos;

import org.hibernate.criterion.Criterion;
import org.hibernate.query.Query;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;
import repository.databaseSource.AbstractDao;
import repository.databaseSource.entities.PtoRatio;

import java.util.List;

import static repository.databaseSource.entities.PtoRatio.FIND_PTO_BY_VALUE;
import static repository.databaseSource.entities.PtoRatio.LOAD_ALL_PTOS;
import static repository.databaseSource.entities.PtoRatio.VALUE_PARAMETER;

@Repository
@Transactional(rollbackFor = Exception.class)
class PtoDaoImp extends AbstractDao<PtoRatio, Long>  implements PtoDao {

    @Override
    public PtoRatio findById (Long id) {
        return super.findById (id);
    }

    @Override
    @SuppressWarnings("unchecked")
    public PtoRatio findByValue (double value) {
        Query<PtoRatio> query = getSession().getNamedQuery(FIND_PTO_BY_VALUE);
        query.setParameter(VALUE_PARAMETER, value);
        return query.getSingleResult();
    }

    @Override
    public void save (PtoRatio entity) {
        super.save(entity);
    }

    @Override
    public void delete (PtoRatio entity) {
        super.delete(entity);
    }

    @Override
    public List<PtoRatio> findByCriteria (Criterion... criteria) {
        return super.findByCriteria(criteria);
    }

    @Override
    @SuppressWarnings("unchecked")
    public List<PtoRatio> findAll() {
        return getSession().getNamedQuery(LOAD_ALL_PTOS).list();
    }
}
