package repository.databaseSource.daos;

import org.hibernate.criterion.Criterion;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;
import repository.databaseSource.AbstractDao;
import repository.databaseSource.entities.BodyType;

import java.util.List;

import static repository.databaseSource.entities.BodyType.BODIES_FIND_ALL_QUERY;
import static repository.databaseSource.entities.BodyType.BODY_BY_SYMBOL;
import static repository.databaseSource.entities.BodyType.BODY_PARAMETER_NAME;

@Repository
@Transactional(rollbackFor = Exception.class)
class BodyDaoImp extends AbstractDao<BodyType, Long> implements BodyDao {



    @Override
    public BodyType findById(Long id) {
        return super.findById(id);
    }

    @Override
    public BodyType findBySymbol(String value) {
        return (BodyType) getSession().getNamedQuery(BODY_BY_SYMBOL).setParameter(BODY_PARAMETER_NAME, value).getSingleResult();
    }

    @Override
    public void save(BodyType entity) {
        super.save(entity);
    }

    @Override
    public void delete(BodyType entity) {
        super.delete(entity);
    }

    @Override
    public List<BodyType> findByCriteria(Criterion... criteria) {
        return super.findByCriteria(criteria);
    }

    @Override
    @SuppressWarnings("unchecked")
    public List<BodyType> findAll() {
        return getSession().getNamedQuery(BODIES_FIND_ALL_QUERY).list();
    }
}
