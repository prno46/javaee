package repository.databaseSource.daos;

import org.hibernate.criterion.Criterion;
import repository.databaseSource.entities.BodyType;

import java.util.List;

public interface BodyDao {

    BodyType findById(Long id);
    BodyType findBySymbol(String value);
    void save (BodyType entity);
    void delete (BodyType entity);
    List<BodyType> findByCriteria (Criterion... criteria);
    List<BodyType> findAll();
}
