package repository.databaseSource.daos;

import org.hibernate.criterion.Criterion;
import repository.databaseSource.entities.PtoRatio;

import java.util.List;

public interface PtoDao {

    PtoRatio findById(Long id);
    PtoRatio findByValue(double value);
    void save (PtoRatio entity);
    void delete (PtoRatio entity);
    List<PtoRatio> findByCriteria (Criterion ... criteria);
    List<PtoRatio> findAll();
}
