package repository.databaseSource.daos;

import org.hibernate.criterion.Criterion;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;
import repository.databaseSource.AbstractDao;
import repository.databaseSource.entities.ChassisBrand;

import java.util.List;

import static repository.databaseSource.entities.ChassisBrand.LOAD_ALL_CHASSIS;

@Repository
@Transactional(rollbackFor = Exception.class)
class ChassisDaoImp extends AbstractDao<ChassisBrand, Long> implements ChassisDao {

    @Override
    public ChassisBrand findById(Long id) {
        return super.findById(id);
    }

    @Override
    public ChassisBrand findByValue(String value) {
        // todo implementation
        return null;
    }

    @Override
    public void save(ChassisBrand entity) {
        super.save(entity);
    }

    @Override
    public void delete(ChassisBrand entity) {
        super.delete(entity);
    }

    @Override
    public List<ChassisBrand> findByCriteria(Criterion... criteria) {
        return super.findByCriteria(criteria);
    }

    @Override
    @SuppressWarnings("unchecked")
    public List<ChassisBrand> findAll() {
        return getSession().getNamedQuery(LOAD_ALL_CHASSIS).list();
    }
}
