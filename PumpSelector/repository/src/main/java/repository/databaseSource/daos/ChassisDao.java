package repository.databaseSource.daos;

import org.hibernate.criterion.Criterion;
import repository.databaseSource.entities.ChassisBrand;

import java.util.List;

public interface ChassisDao {

    ChassisBrand findById(Long id);
    ChassisBrand findByValue(String value);
    void save (ChassisBrand entity);
    void delete (ChassisBrand entity);
    List<ChassisBrand> findByCriteria (Criterion... criteria);
    List<ChassisBrand> findAll();
}
