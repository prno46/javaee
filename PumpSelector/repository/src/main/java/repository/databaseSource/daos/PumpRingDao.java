package repository.databaseSource.daos;

import repository.databaseSource.entities.PumpRingE;

import java.util.List;

public interface PumpRingDao {

    PumpRingE findById(Long id);
    PumpRingE findByValue(String value);
    void save (PumpRingE entity);
    void delete (PumpRingE entity);
    List<PumpRingE> loadAllRingsByCriteria (boolean isPumpDirectMount);
    List<PumpRingE> fetchAllRingsWithCriteria (boolean isPumpDirectMount);
    List<PumpRingE> findAll();
}
