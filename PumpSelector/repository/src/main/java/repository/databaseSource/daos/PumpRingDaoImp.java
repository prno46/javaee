package repository.databaseSource.daos;

import org.hibernate.query.Query;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;
import repository.databaseSource.AbstractDao;
import repository.databaseSource.entities.PumpRingE;

import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Expression;
import javax.persistence.criteria.Root;
import java.util.List;

import static repository.databaseSource.entities.PumpRingE.*;

@Repository
@Transactional(rollbackFor = Exception.class)
class PumpRingDaoImp extends AbstractDao<PumpRingE, Long> implements PumpRingDao{

    @Override
    public PumpRingE findById(Long id) {
        return super.findById(id);
    }

    @Override
    public PumpRingE findByValue(String value) {
        // todo to implement
        return null;
    }

    @Override
    public void save(PumpRingE entity) {
        super.save(entity);
    }

    @Override
    public void delete(PumpRingE entity) {
        super.delete(entity);
    }

    @Override
    public List<PumpRingE> loadAllRingsByCriteria(boolean isPumpDirectMount) {
        CriteriaBuilder cb = getSession().getCriteriaBuilder();
        CriteriaQuery<PumpRingE> criteriaQuery = cb.createQuery(PumpRingE.class);
        Root<PumpRingE> root = criteriaQuery.from(PumpRingE.class);

        /*Expression<Boolean> directMount =
                cb.and(cb.notLike(root.get("symbol"), B24),
                        cb.notLike(root.get("symbol"), B22),
                        cb.notLike(root.get("symbol"), B31));*/

        Expression<Boolean> directMount = null; //=
        // todo -> to make this work with enums, for now is cast exception String : Enum
               /* cb.or(cb.like(root.get("category"), BOTH.name()),
                        cb.like(root.get("category"), DIRECT.name()));*/

       Expression<Boolean> inDirectMount = null; /*
                cb.and(cb.notLike(root.get("symbol"), B25),
                        cb.notLike(root.get("symbol"), P70),
                        cb.notLike(root.get("symbol"), P35));*/

        Expression<Boolean> restriction = isPumpDirectMount ? directMount : inDirectMount;
        criteriaQuery.select(root).where(restriction);

        Query<PumpRingE> query = getSession().createQuery(criteriaQuery);

        return query.list();
    }

    @SuppressWarnings("unchecked")
    public List<PumpRingE> fetchAllRingsWithCriteria(boolean isPumpDirectMount) {
        return isPumpDirectMount ?
                getSession().getNamedQuery(ALL_RINGS_FOR_DIRECT_MOUNT_PUMP).getResultList()
                : getSession().getNamedQuery(ALL_RINGS_FOR_INDIRECT_MOUNT_PUMP).getResultList();
    }

    @Override
    @SuppressWarnings("unchecked")
    public List<PumpRingE> findAll() {
        return getSession().getNamedQuery(LOAD_ALL_RINGS).list();
    }
}
