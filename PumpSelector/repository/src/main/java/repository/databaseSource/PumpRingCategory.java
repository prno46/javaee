package repository.databaseSource;

public enum PumpRingCategory {
    DIRECT, INDIRECT, BOTH
}
