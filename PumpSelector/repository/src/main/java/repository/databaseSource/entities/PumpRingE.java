package repository.databaseSource.entities;

import repository.databaseSource.PumpRingCategory;

import javax.persistence.*;
import javax.validation.constraints.DecimalMax;
import javax.validation.constraints.DecimalMin;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Pattern;
import java.io.Serializable;

import static repository.databaseSource.entities.PumpRingE.*;


@Entity
@Table(name = "pump_ring")
@Access(value = AccessType.FIELD)
@NamedQueries({
        @NamedQuery(
                name = LOAD_ALL_RINGS,
                query = "from PumpRingE"
        ),
        @NamedQuery(
                name = ALL_RINGS_FOR_DIRECT_MOUNT_PUMP,
                query = "from PumpRingE e where e.category in ('"+ BOTH +"', '"+ DIRECT +"')"
        ),
        @NamedQuery(
                name = ALL_RINGS_FOR_INDIRECT_MOUNT_PUMP,
                query = "from PumpRingE e where e.category in ('"+ BOTH +"', '"+ INDIRECT +"')"
        )
})
public class PumpRingE implements Serializable {

    @Transient public static final String LOAD_ALL_RINGS = "loadAllRings";
    @Transient public static final String ALL_RINGS_FOR_DIRECT_MOUNT_PUMP = "directMountPto";
    @Transient public static final String ALL_RINGS_FOR_INDIRECT_MOUNT_PUMP = "inDirectMountPto";
    @Transient static final String BOTH = "BOTH";
    @Transient static final String DIRECT = "DIRECT";
    @Transient static final String INDIRECT = "INDIRECT";

    @Transient private static final long serialVersionUID = 46L;

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "bodyGenerator")
    @SequenceGenerator(name = "bodyGenerator", sequenceName = "pump_ring_sq", allocationSize = 1)
    @Column(name = "pump_ring_id")
    private Long pumpRingId;

    @NotNull
    @Pattern(regexp = "^[BP][0-9]{2}$")
    @Column(name = "pump_ring_symbol", unique = true)
    private String symbol;

    @NotNull
    @DecimalMin(value = "10.0")
    @DecimalMax(value = "135.0")
    @Column(name = "pump_ring_ratio", unique = true)
    private double flowRatio;

    @Enumerated(value = EnumType.STRING)
    @Column(name = "category")
    private PumpRingCategory category;

    public PumpRingE() { }

    public String getSymbol() {
        return symbol;
    }

    public void setSymbol(String symbol) {
        this.symbol = symbol.toUpperCase();
    }

    public double getFlowRatio() {
        return flowRatio;
    }

    public void setFlowRatio(double flowRatio) {
        this.flowRatio = flowRatio;
    }

    public Long getPumpRingId() {
        return pumpRingId;
    }

    public PumpRingCategory getCategory() {
        return category;
    }

    public void setCategory(PumpRingCategory direct) {
        category = direct;
    }

    @Override
    public int hashCode() {
        int result = symbol.hashCode();
        return  31 * result + symbol.hashCode() + 31 * result + Double.hashCode(flowRatio);
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {return true;}
        if (!(obj instanceof PumpRingE)) {return false;}
        PumpRingE that = (PumpRingE) obj;
        return (symbol.equals(that.symbol)
                && flowRatio == that.flowRatio
                && category.name().equals(that.category.name()));
    }

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder("\n symbol: ");
        return sb.append(symbol).append(" flow ratio: ").append(flowRatio).toString();
    }
}
