package repository.databaseSource.entities;

import repository.databaseSource.PtoType;

import javax.persistence.*;
import javax.validation.constraints.DecimalMax;
import javax.validation.constraints.DecimalMin;
import javax.validation.constraints.NotNull;
import java.io.Serializable;

import static repository.databaseSource.entities.PtoRatio.*;

@Entity
@Table(name = "pto_ratio")
@Access(value = AccessType.FIELD)
@NamedQueries({
        @NamedQuery(
                name = LOAD_ALL_PTOS,
                query = "from PtoRatio"
        ),
        @NamedQuery(
                name = FIND_PTO_BY_VALUE,
                query = "from PtoRatio p where p.ratio = :"+VALUE_PARAMETER
        )
})
public class PtoRatio implements Serializable {

    @Transient public static final String LOAD_ALL_PTOS = "loadAllPtos";
    @Transient public static final String FIND_PTO_BY_VALUE = "findByValue";
    @Transient public static final String VALUE_PARAMETER = "ratio";

    @Transient private static final long serialVersionUID = 46L;

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "generator")
    @SequenceGenerator(name = "generator", sequenceName = "pto_r_seq", allocationSize = 1)
    @Column(name = "pto_id")
    private Long ptoRId;

    @NotNull
    @DecimalMax(value = "2.0", message = "The proper value of PtoRatio should be between 0.5 and 1.5")
    @DecimalMin(value = "0.5")
    @Column(name = "pto_ratio", unique = true)
    private double ratio;

    @Enumerated(value = EnumType.STRING)
    @Column(name = "pto_type", unique = true)
    private PtoType type;

    public PtoRatio() { }

    public double getRatio() {
        return ratio;
    }

    public void setRatio(double ratio) {
        this.ratio = ratio;
    }

    public PtoType getType() {
        return type;
    }

    public void setType(PtoType type) {
        this.type = type;
    }

    public long getPtoRId() {
        return ptoRId;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {return true;}
        if (!(obj instanceof PtoRatio)) {return false;}
        PtoRatio that = (PtoRatio) obj;
        return (ratio == that.ratio && type.equals(that.type));
    }

    @Override
    public int hashCode() {
        int result = Double.hashCode(ratio);
        return 31 * result + Double.hashCode(ratio) + 31 * result + type.hashCode();
    }

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder("\n ratio value is: ");
        return sb.append(ratio).append(" PTO Type is: ").append(type).toString();
    }
}
