package repository.databaseSource.entities;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import java.io.Serializable;

import static repository.databaseSource.entities.ChassisBrand.LOAD_ALL_CHASSIS;

@Entity
@Table(name = "chassis_brand")
@Access(value = AccessType.FIELD)
@NamedQueries({
        @NamedQuery(
                name = LOAD_ALL_CHASSIS,
                query = "from ChassisBrand"
        )
})
public class ChassisBrand implements Serializable {

    @Transient public final static String LOAD_ALL_CHASSIS = "loadAllChassis";
    @Transient private static final long serialVersionUID = 46L;

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "generator")
    @SequenceGenerator(name = "generator", sequenceName = "chassis_brand_sq", allocationSize = 1)
    @Column(name = "chassis_id")
    private Long chassisId;

    @NotNull
    @Size(min = 3, max = 25)
    @Column(name = "brand", unique = true)
    private String brand;

    public ChassisBrand() { }

    public String getBrand() {
        return brand;
    }

    public void setBrand(String brand) {
        this.brand = brand.toUpperCase();
    }

    public Long getChassisId() {
        return chassisId;
    }

    @Override
    public int hashCode() {
        int result = brand.hashCode();
        return 31 * result + brand.hashCode();
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {return true;}
        if (!(obj instanceof ChassisBrand)) {return false;}
        ChassisBrand that = (ChassisBrand) obj;
        return (brand.equals(that.brand));
    }

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder("\n chassis brand is: ");
        return sb.append(brand).toString();
    }
}
