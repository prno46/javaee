package repository.databaseSource.entities;

import javax.persistence.*;
import javax.validation.constraints.*;
import java.io.Serializable;

import static repository.databaseSource.entities.BodyType.*;

@Entity
@Table(name = "body_type")
@Access(value = AccessType.FIELD)
@NamedQueries({
        @NamedQuery(
                name = BODIES_FIND_ALL_QUERY,
                query = "from BodyType"
        ),
        @NamedQuery(
                name = BODY_BY_SYMBOL,
                query = "from BodyType b where b.symbol like :"+BODY_PARAMETER_NAME
        )
})
public class BodyType implements Serializable {
    @Transient public static final String BODIES_FIND_ALL_QUERY = "loadAllBodies";
    @Transient public static final String BODY_BY_SYMBOL = "loadBodyBySymbol";
    @Transient public static final String BODY_PARAMETER_NAME = "bodySymbol";

    @Transient private static final long serialVersionUID = 46L;

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "bodyGenerator")
    @SequenceGenerator(name = "bodyGenerator", sequenceName = "body_seq", allocationSize = 1)
    @Column(name = "body_id")
    private Long bodyId;

    @NotNull
    @NotEmpty
    @Size(min = 4, max = 18)
    @Column(name = "body_symbol", unique = true)
    private String symbol;

    @NotNull
    @DecimalMin(value = "45.0")
    @DecimalMax(value = "150.0")
    @Column(name = "flow_ratio", unique = true)
    private double flowRatio;

    public BodyType() { }

    public String getSymbol() {
        return symbol;
    }

    public void setSymbol(String symbol) {
        this.symbol = symbol.toUpperCase();
    }

    public double getFlowRatio() {
        return flowRatio;
    }

    public void setFlowRatio(double flowRatio) {
        this.flowRatio = flowRatio;
    }

    public Long getBodyId() {
        return bodyId;
    }

    @Override
    public int hashCode() {
        int result = symbol.hashCode();
        return  31 * result + symbol.hashCode() + 31 * result + Double.hashCode(flowRatio);
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {return true;}
        if (!(obj instanceof BodyType)) {return false;}
        BodyType that = (BodyType) obj;
        return (symbol.equals(that.symbol) && flowRatio == that.flowRatio);
    }

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder("\n symbol: ");
        return sb.append(symbol).append(" flow ratio: ").append(flowRatio).toString();
    }
}
