package repository.dataRepository;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import repository.databaseSource.daos.BodyDao;
import repository.databaseSource.daos.ChassisDao;
import repository.databaseSource.daos.PtoDao;
import repository.databaseSource.daos.PumpRingDao;
import repository.databaseSource.entities.BodyType;
import repository.databaseSource.entities.ChassisBrand;
import repository.databaseSource.entities.PtoRatio;
import repository.databaseSource.entities.PumpRingE;

import java.util.List;

@Component
class DaoDataRepositoryImpl implements DaoDataRepository {

    private final PtoDao ptoDao;
    private final BodyDao bodyDao;
    private final ChassisDao chassisDao;
    private final PumpRingDao pumpRingDao;

    @Autowired
    DaoDataRepositoryImpl(PtoDao ptoDao, BodyDao bodyDao, ChassisDao chassisDao, PumpRingDao pumpRingDao) {
        this.ptoDao = ptoDao;
        this.bodyDao = bodyDao;
        this.chassisDao = chassisDao;
        this.pumpRingDao = pumpRingDao;
    }

    // pto ratio
    @Override
    public void saveNewPtoRatioValue(PtoRatio entity) {
        ptoDao.save(entity);
    }

    @Override
    public List<PtoRatio> getAllPtoRatios() {
        return ptoDao.findAll();
    }

    // bodies
    @Override
    public void saveNewBodyType(BodyType entity) {
        bodyDao.save(entity);
    }

    @Override
    public List<BodyType> getAllBodyTypes() {
        return bodyDao.findAll();
    }

    @Override
    public BodyType getBodyBySymbol(String value) {
        return bodyDao.findBySymbol(value);
    }

    // pump rings

    @Override
    public void saveNewPumpRing(PumpRingE entity) {
        pumpRingDao.save(entity);
    }

    @Override
    public List<PumpRingE> getAllPumpRings() {
        return pumpRingDao.findAll();
    }

    @Override
    public List<PumpRingE> loadAllRingsByCriteria(boolean isPumpDirectMount) {
        return pumpRingDao.loadAllRingsByCriteria(isPumpDirectMount);
    }

    @Override
    public List<PumpRingE> fetchAllRingsWithCriteria(boolean isPumpDirectMount) {
        return pumpRingDao.fetchAllRingsWithCriteria(isPumpDirectMount);
    }

    // chassis

    @Override
    public void saveNewChassisBrand(ChassisBrand entity) {
        chassisDao.save(entity);
    }

    @Override
    public List<ChassisBrand> getAllChassisBrands() {
        return chassisDao.findAll();
    }
}
