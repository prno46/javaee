package repository.dataRepository;

import repository.databaseSource.entities.BodyType;
import repository.databaseSource.entities.ChassisBrand;
import repository.databaseSource.entities.PtoRatio;
import repository.databaseSource.entities.PumpRingE;

import java.util.List;

public interface DaoDataRepository {

    // for Pto
    void saveNewPtoRatioValue(PtoRatio entity);
    List<PtoRatio> getAllPtoRatios();

    // for body type
    void saveNewBodyType(BodyType entity);
    List<BodyType> getAllBodyTypes();
    BodyType getBodyBySymbol(String value);

    // for pump rings
    void saveNewPumpRing(PumpRingE entity);
    List<PumpRingE> getAllPumpRings();
    List<PumpRingE> loadAllRingsByCriteria(boolean isPumpDirectMount);
    List<PumpRingE> fetchAllRingsWithCriteria(boolean isPumpDirectMount);

    // for chassis
    void saveNewChassisBrand(ChassisBrand entity);
    List<ChassisBrand> getAllChassisBrands();

}
