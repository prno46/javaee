package repository.domainRepository;

import domain.body.RCVType;
import domain.pumpRings.PumpRings;
import repository.databaseSource.PtoType;
import repository.databaseSource.entities.BodyType;
import repository.databaseSource.entities.ChassisBrand;
import repository.databaseSource.entities.PtoRatio;
import repository.databaseSource.entities.PumpRingE;

import java.util.List;

public interface DomainRepository {

    List<PtoRatio> loadAllPtoRatios();
    List<ChassisBrand> loadAllChassisBrands();
    List<RCVType> loadAllRcvTypes();
    List<PumpRings> loadAllPumpRings();
    List<PumpRings> loadAllRingsByCriteria(boolean isPumpDirectMount);
    List<PumpRings> fetchAllRingsWithCriteria(boolean isPumpDirectMount);
    RCVType loadBodyBySymbol (String symbol);
    void saveNewPto(double value, PtoType type);
    void saveNewPtoEntity(PtoRatio entity);
    void saveNewBodyType (String symbol, double flowRatio);
    void saveNewBodyTypeEntity (BodyType entity);
    void saveNewChassisBrand(String brand);
    void saveNewChassisBrandEntity (ChassisBrand entity);
    void saveNewPumpRing(String symbol, double ratio);
    void saveNewPumpRingEntity (PumpRingE entity);
}
