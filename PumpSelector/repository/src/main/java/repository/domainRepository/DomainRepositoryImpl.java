package repository.domainRepository;

import domain.body.RCVType;
import domain.body.RCVTypeFactory;
import domain.pumpRings.PumpRings;
import domain.pumpRings.PumpRingsFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import repository.dataRepository.DaoDataRepository;
import repository.databaseSource.PtoType;
import repository.databaseSource.entities.BodyType;
import repository.databaseSource.entities.ChassisBrand;
import repository.databaseSource.entities.PtoRatio;
import repository.databaseSource.entities.PumpRingE;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

@Service
class DomainRepositoryImpl implements DomainRepository{

    private final DaoDataRepository daoRepository;
    private final RCVTypeFactory factory;
    private final PumpRingsFactory ringsFactory;

    @Autowired
    public DomainRepositoryImpl(DaoDataRepository daoRepository, RCVTypeFactory factory, PumpRingsFactory ringsFactory) {
        this.daoRepository = daoRepository;
        this.factory = factory;
        this.ringsFactory = ringsFactory;
    }

    @Override
    public List<PtoRatio> loadAllPtoRatios() {
        return daoRepository.getAllPtoRatios();
    }

    @Override
    public List<ChassisBrand> loadAllChassisBrands() {
        return daoRepository.getAllChassisBrands();
    }

    @Override
    public List<RCVType> loadAllRcvTypes() {
        List<RCVType> types = new ArrayList<>();
        for (BodyType t: daoRepository.getAllBodyTypes()) {
            types.add(factory.provideRcvType(t.getSymbol(), t.getFlowRatio()));
        }
        return types;
    }

    @Override
    public RCVType loadBodyBySymbol(String symbol) {
        BodyType body = daoRepository.getBodyBySymbol(symbol);
        return new RCVTypeFactory().provideRcvType(body.getSymbol(), body.getFlowRatio());
    }

    @Override
    public List<PumpRings> loadAllPumpRings() {
        List<PumpRings> rings = new ArrayList<>();
        for (PumpRingE p: daoRepository.getAllPumpRings()) {
            rings.add(ringsFactory.providePumpRing(p.getSymbol(), p.getFlowRatio()));
        }
        return rings;
    }

    @Override
    public List<PumpRings> loadAllRingsByCriteria(boolean isPumpDirectMount) {
        return daoRepository.loadAllRingsByCriteria(isPumpDirectMount).stream()
                .map( r -> ringsFactory.providePumpRing(r.getSymbol(), r.getFlowRatio()))
                .collect(Collectors.toList());
    }

    @Override
    public List<PumpRings> fetchAllRingsWithCriteria(boolean isPumpDirectMount) {
        return daoRepository.fetchAllRingsWithCriteria(isPumpDirectMount).stream()
                .map( r -> ringsFactory.providePumpRing(r.getSymbol(), r.getFlowRatio()))
                .collect(Collectors.toList());
    }

    @Override
    public void saveNewPto(double value, PtoType type) {
        PtoRatio ratio = new PtoRatio();
        ratio.setRatio(value);
        ratio.setType(type);
        daoRepository.saveNewPtoRatioValue(ratio);
    }

    @Override
    public void saveNewPtoEntity(PtoRatio entity) {
        daoRepository.saveNewPtoRatioValue(entity);
    }

    @Override
    public void saveNewBodyType (String symbol, double flowRatio) {
        BodyType body = new BodyType();
        body.setSymbol(symbol);
        body.setFlowRatio(flowRatio);
        daoRepository.saveNewBodyType(body);
    }

    @Override
    public void saveNewBodyTypeEntity(BodyType entity) {
        daoRepository.saveNewBodyType(entity);
    }

    @Override
    public void saveNewChassisBrand(String brand) {
        ChassisBrand Cbrand = new ChassisBrand();
        Cbrand.setBrand(brand);
        daoRepository.saveNewChassisBrand(Cbrand);
    }

    @Override
    public void saveNewChassisBrandEntity(ChassisBrand entity) {
        daoRepository.saveNewChassisBrand(entity);
    }

    @Override
    public void saveNewPumpRing(String symbol, double ratio) {
        PumpRingE pumpRingE = new PumpRingE();
        pumpRingE.setSymbol(symbol);
        pumpRingE.setFlowRatio(ratio);
        daoRepository.saveNewPumpRing(pumpRingE);
    }

    @Override
    public void saveNewPumpRingEntity(PumpRingE entity) {
        daoRepository.saveNewPumpRing(entity);
    }
    
}
