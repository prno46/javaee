package repository.fileSource;

import org.springframework.core.io.Resource;
import org.springframework.stereotype.Service;
import repoConfig.RingProviderImpl;
import repoConfig.RingResource;

import javax.annotation.PostConstruct;
import java.io.IOException;
import java.io.InputStream;
import java.util.HashMap;
import java.util.Map;
import java.util.Scanner;

@Service
@RingProviderImpl
class RingDataProviderImpl implements DataProvider {

    private final Resource ringFile;
    private Map<String, Double> ringSource;

    RingDataProviderImpl(@RingResource Resource ringFile) {
        this.ringFile = ringFile;
        ringSource = new HashMap<>(12);
    }

    @PostConstruct
    private void getRings() throws IOException {
        InputStream in = ringFile.getInputStream();
        Scanner sc = new Scanner(in);
        String[] key = new String[2];
        while(sc.hasNext()) {
            key = sc.nextLine().split("=");
            ringSource.put(key[0], Double.valueOf(key[1]));
        }
        sc.close();
    }

    @Override
    public Map<String, Double> provideData() {
        return ringSource;
    }
}
