package repository.fileSource;

import org.springframework.core.io.Resource;
import org.springframework.stereotype.Service;
import repoConfig.BodyResource;
import repoConfig.RcvProviderImpl;

import javax.annotation.PostConstruct;
import java.io.IOException;
import java.io.InputStream;
import java.util.HashMap;
import java.util.Map;
import java.util.Scanner;


@Service
@RcvProviderImpl
class RcvTypeDataProviderImpl implements DataProvider {

    private final Resource rcvTypeResource;
    private Map<String, Double> rcvType;

    RcvTypeDataProviderImpl(@BodyResource Resource rcvTypeResource) {
        this.rcvTypeResource = rcvTypeResource;
        this.rcvType = new HashMap<>(10);
    }

    @PostConstruct
    private void getBodyType() throws IOException {
        InputStream in = rcvTypeResource.getInputStream();
        Scanner sc = new Scanner(in);
        String[] key = new String[2];
        while(sc.hasNext()) {
            key = sc.nextLine().split("=");
            rcvType.put(key[0], Double.valueOf(key[1]));
        }
        sc.close();
    }

    @Override
    public Map<String, Double> provideData() {
        return rcvType;
    }
}
