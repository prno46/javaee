package repository.fileSource;

import java.util.Map;

public interface DataProvider {
    Map<String, Double> provideData();
}
