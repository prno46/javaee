package repository.fileSource;

import domain.body.RCVType;
import domain.pumpRings.PumpRings;

import java.util.List;

public interface LocalDataSource {
    List<Double> getPtoRatios();
    List<PumpRings> getPumpRings();
    List<RCVType> getRcvTypes();
}
