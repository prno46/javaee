package repository.fileSource;

import domain.body.RCVType;
import domain.body.RCVTypeFactory;
import domain.pumpRings.PumpRings;
import domain.pumpRings.PumpRingsFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import repoConfig.RcvProviderImpl;
import repoConfig.RingProviderImpl;

import javax.annotation.PostConstruct;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

@Service
class FileDataSource implements LocalDataSource /*, DaoDataRepository*/ {

    private final DataProvider ringProvider;
    private final DataProvider rcvProvider;
    private final List<Double> listOfPtoRatios;
    private final RCVTypeFactory rcvFactory;
    private final PumpRingsFactory pumpRingsFactory;
    private List<PumpRings> listOfPumpRings;
    private List<RCVType> listOfRcvTypes;

    @Autowired
    FileDataSource(@RingProviderImpl DataProvider ringProvider, @RcvProviderImpl DataProvider rcvProvider,
                   List<Double> listOfPtoRatios, RCVTypeFactory rcvFactory, PumpRingsFactory pumpRingsFactory) {
        this.ringProvider = ringProvider;
        this.rcvProvider = rcvProvider;
        this.listOfPtoRatios = listOfPtoRatios;
        this.rcvFactory = rcvFactory;
        this.pumpRingsFactory = pumpRingsFactory;
        listOfPumpRings = new ArrayList<>(12);
        listOfRcvTypes = new ArrayList<>(8);
    }

    @PostConstruct
    private void populateBothLists() {
        convertMapOfPumpRingsToRings();
        convertMapOfRcvTypeToRcv();
    }

    @Override
    public List<Double> getPtoRatios() {
        return listOfPtoRatios;
    }

    @Override
    public List<PumpRings> getPumpRings() {
        return listOfPumpRings;
    }

    @Override
    public List<RCVType> getRcvTypes() {
        return listOfRcvTypes;
    }

    private void convertMapOfPumpRingsToRings() {
        for (Map.Entry<String, Double> key: ringProvider.provideData().entrySet()) {
            listOfPumpRings.add(pumpRingsFactory.providePumpRing(key.getKey(), key.getValue()));
        }
    }

    private void convertMapOfRcvTypeToRcv() {
        for(Map.Entry<String, Double> key: rcvProvider.provideData().entrySet()) {
            listOfRcvTypes.add(rcvFactory.provideRcvType(key.getKey(), key.getValue()));
        }
    }

    // below methods form DaoDataRepository


}
