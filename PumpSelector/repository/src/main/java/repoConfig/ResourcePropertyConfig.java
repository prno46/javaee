package repoConfig;


import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.PropertySource;
import org.springframework.context.support.PropertySourcesPlaceholderConfigurer;
import org.springframework.core.io.Resource;

import java.util.List;

@PropertySource({"ringsConfiguration.properties", "rcvType.properties", "ptoRatio.properties"})
@ComponentScan(basePackages = "repository.fileSource")
@Configuration
public class ResourcePropertyConfig {

    @Value("ringsConfiguration.properties")
    private Resource ringResource;

    @Value("rcvType.properties")
    private Resource bodyResource;

    // example of using Spring SpEL expression language to obtain a list from property file
    @Value("#{'${ratios}'.split(',')}")
    private List<Double> ptoRatio;

    @Bean
    @RingResource
    Resource provideRingResource() {
        return ringResource;
    }

    @Bean
    @BodyResource
    Resource provideBodyResource() {
        return bodyResource;
    }

    @Bean
    @PtoResource
    List<Double> providePtoRatio() {
        return ptoRatio;
    }

    //required for SpEl expression to work used to return List of pto values from property file
    @Bean
    public static PropertySourcesPlaceholderConfigurer propertyConfigInDev() {
        return new PropertySourcesPlaceholderConfigurer();
    }
}
