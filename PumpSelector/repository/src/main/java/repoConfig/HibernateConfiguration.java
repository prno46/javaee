package repoConfig;

import com.mchange.v2.c3p0.ComboPooledDataSource;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.PropertySource;
import org.springframework.core.env.Environment;
import org.springframework.dao.annotation.PersistenceExceptionTranslationPostProcessor;
import org.springframework.orm.hibernate5.HibernateTransactionManager;
import org.springframework.orm.hibernate5.LocalSessionFactoryBean;
import org.springframework.transaction.PlatformTransactionManager;
import org.springframework.transaction.annotation.EnableTransactionManagement;

import javax.sql.DataSource;
import java.beans.PropertyVetoException;
import java.util.Properties;

@PropertySource("classpath:hibernate.setup.properties")
@Configuration
@EnableTransactionManagement
public class HibernateConfiguration  {

    private Logger logger = LoggerFactory.getLogger(getClass().getSimpleName());

    @Autowired
    private Environment environment;

    public HibernateConfiguration() {
        logger.warn("class loaded");
    }

    @Bean
    LocalSessionFactoryBean sessionFactory() {
        LocalSessionFactoryBean sessionFactory = new LocalSessionFactoryBean();
        sessionFactory.setDataSource(postgresDataSource());
        sessionFactory.setPackagesToScan(
                "repository.databaseSource.entities"
        );
        sessionFactory.setHibernateProperties(hibernateProperties());
        return sessionFactory;
    }

    private DataSource postgresDataSource() {

        ComboPooledDataSource dataSource = new ComboPooledDataSource();
        try {
            //dataSource.setDriverClass("org.postgresql.Driver");
            dataSource.setDriverClass(environment.getRequiredProperty("DriverClass"));
            //dataSource.setJdbcUrl("jdbc:postgresql://localhost:5432/pump_selector");
            dataSource.setJdbcUrl(environment.getRequiredProperty("JdbcUrl"));
            //dataSource.setUser("przemek");
            dataSource.setUser(environment.getRequiredProperty("DBUser"));
            //dataSource.setPassword("zoeller");
            dataSource.setPassword(environment.getRequiredProperty("Password"));
            dataSource.setAcquireIncrement(5);
            dataSource.setIdleConnectionTestPeriod(60);
            dataSource.setMaxPoolSize(100);
            dataSource.setMaxStatements(50);
            dataSource.setMinPoolSize(5);

        } catch (PropertyVetoException e) {
            throw new RuntimeException(e);
        }
        return dataSource;
    }

    private Properties hibernateProperties() {
        Properties hibernateProperties = new Properties();
        hibernateProperties.setProperty("hibernate.hbm2ddl.auto", "update");
        //hibernateProperties.setProperty("hibernate.dialect","org.hibernate.dialect.PostgreSQLDialect");
        hibernateProperties.setProperty("hibernate.dialect",environment.getRequiredProperty("hibernate.dialect"));
        hibernateProperties.setProperty("hibernate.jdbc.lob.non_contextual_creation","true");
        return hibernateProperties;
    }

    @Bean
    public PlatformTransactionManager hibernateTransactionManager() {
        HibernateTransactionManager transactionManager = new HibernateTransactionManager();
        transactionManager.setSessionFactory(sessionFactory().getObject());
        return transactionManager;
    }

    @Bean
    public PersistenceExceptionTranslationPostProcessor exceptionTranslation() {
        return new PersistenceExceptionTranslationPostProcessor();
    }

}
