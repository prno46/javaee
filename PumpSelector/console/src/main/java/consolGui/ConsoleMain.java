package consolGui;


import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.jdbc.DataSourceAutoConfiguration;
import org.springframework.context.annotation.Import;
import repoConfig.HibernateConfiguration;

@SpringBootApplication(exclude = {DataSourceAutoConfiguration.class},
scanBasePackages = {"consolGui", "domain",
        "repository.databaseSource.daos", "repository.dataRepository",
        "repository.domainRepository", "useCases"}
)
@Import({HibernateConfiguration.class})
class ConsoleMain {

    public static void main(String[] args) {
        SpringApplication.run(ConsoleMain.class);
    }

}
