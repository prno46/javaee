package consolGui;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import useCases.LoadBasicDataUseCase;

import javax.annotation.PostConstruct;

@Component
class UserInput {


    private final LoadBasicDataUseCase loadUseCase;

    @Autowired
    public UserInput(LoadBasicDataUseCase loadUseCase) {
        this.loadUseCase = loadUseCase;
    }

    @PostConstruct
    void testOfLoadingPtos() {
        System.out.println("Loading data from data base and use case: ");
        System.out.println(loadUseCase.loadAllPtoRatios());
    }
}
