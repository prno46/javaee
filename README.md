# JavaEE

This is repository for Java projects in which Spring framework is used.


## Projects Description


### Hibernate

The purpose of this project is to learn the basic of Hibernate framework.

It may be also used as some kind of cheat sheet, it contains:

* Basic Hibernate configuration examples
* Entity mappings example
* Collections mappings example
* Enum mappings example


### Pump Selector

Application which for the given input data has to display the list of possible

hydraulic pumps that can be used for production of Waste Collector Truck. 

#### Frameworks and libraries used:

* SpringBoot 2.xx
* Spring MVC 5.xx
* Hibernate 5.xx
* Thymeleaf 3.xx
* PostgreSQL
* Basic HTML
* Basic CSS


### Guessing Game

Simple console game made using Spring Framework. The main objective of this project was 

to get to know Spring Framework - learn the basics. 


### Simple War Game

Another simple console game made using Spring Framework. 


### TicTacToe

One of the first Java projects TicTacToe game. 

## Authors

* **Przemyslaw Nowacki** 