# Tic_Tac_Toe Game

Implementation of a Tic_Tac_Toe Game game .

You need two players to play this game.

There is a console gui and JavaFx gui.


## Installing

Import it as a Maven project.

If you want to start a console gui start the Cli.class from packet gui/console/Cli.

If you want to start javaFx gui start the FxGui.class from packet gui/fxgui/FxGui.

You need to start the javaFx version with Maven configuration : compile exec:java -f pom.xml


## Prerequisites

This game uses java 11 and openfx for java 11.

It will not run on lower versions.

IDE Intellij Idea Community 2018.



## How it works

#### Console version

When you start a game you are asked to enter the names of the players.

when board is drawn, first you put a latter A B or C (can be in lower case) - it describes column's name

then you enter the number - it describes rows.

When you play there are four options (words) that you write in a console:

```
new  :  starts a new game
exit :  quits from the game
save :  saves the current state of the game
load :  loads previously saved game state

```


#### javaFx

You just click on a filed where you want to place X or O.


## Built With

* [Maven](https://maven.apache.org/) - Dependency Management

## Authors

* **Przemyslaw Nowacki** 

