package application.gui.fxgui.events;

import application.game.gameengine.Action;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.scene.control.Button;
import javafx.stage.Stage;

import static application.game.gameengine.GameState.State.RUN;

public class MenuEvent implements EventHandler<ActionEvent> {

    private Action gameAction;
    private Stage stage;

    public MenuEvent(Action gameAction, Stage stage) {
        this.gameAction = gameAction;
        this.stage = stage;
    }

    @Override
    public void handle(ActionEvent actionEvent) {
        Button button = (Button)actionEvent.getSource();
        gameAction.setMessage(button.getText());
        gameAction.setStatus(RUN);
        stage.hide();
    }
}
