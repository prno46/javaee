package application.gui.fxgui.events;

import javafx.event.EventHandler;
import javafx.stage.WindowEvent;

public class GameExitEvent implements EventHandler<WindowEvent> {

    @Override
    public void handle(WindowEvent windowEvent) {
        System.exit(0);
    }
}
