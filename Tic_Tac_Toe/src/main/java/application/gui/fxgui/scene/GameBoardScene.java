package application.gui.fxgui.scene;

import application.game.exceptions.GameOXException;
import application.game.gameboard.GameBoard.Coordinate;
import application.game.gameengine.GameOX;
import application.gui.fxgui.events.GameBoardFieldEvent;
import application.gui.fxgui.events.MenuEvent;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.scene.control.Button;
import javafx.scene.control.ComboBox;
import javafx.scene.control.Label;
import javafx.scene.control.TextArea;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.HBox;

import static application.game.gameengine.GameState.State.RUN;

public class GameBoardScene extends GameScene {

    public static TextArea logs;

    static {
        if(logs == null) {
            logs = new TextArea();
        }
    }

    @Override
    protected void build() {
        EventHandler<ActionEvent> event = new MenuEvent(gameAction, stage);
        Button newGame = new Button("New");
        newGame.setOnAction(event);
        Button save = new Button("Save");
        save.setOnAction(event);
        Button load = new Button("Load");
        load.setOnAction(event);
        Button exit = new Button("Exit");
        exit.setOnAction(event);

        HBox menu = new HBox(newGame, save, load, exit);
        root.add(menu, 0, 0);

        try {
            root.add(getBoard(), 0, 1);
        } catch(GameOXException e) {
            e.printStackTrace();
        }
        root.add(logs, 0, 2);
        GameBoardScene.logs.appendText(gameAction.getMessage() + "\n");
    }

    private GridPane getBoard() throws GameOXException {
        GridPane board = new GridPane();
        Button endTrue = new Button("Next player");
        endTrue.setDisable(true);
        endTrue.setOnMouseClicked(event -> {
            gameAction.setCoords((Coordinate) endTrue.getUserData());
            stage.hide();
        });

       for(int y = 0; y < GameOX.number.size(); y++) {
            for(int x = 0; x < GameOX.literal.size(); x++) {
                Coordinate key = gameAction.getGameBoard().getKey(x, y);
                char sign = gameAction.getGameBoard().get(key);
                if(sign == ' ' && gameAction.getStatus() == RUN) {
                    ObservableList<Character> fieldList = FXCollections.observableArrayList();
                    fieldList.add(' ');
                    fieldList.add(gameAction.getCurrentPlayer().getSign());
                    ComboBox<Character> field = new ComboBox<>();
                    field.setItems(fieldList);
                    field.setUserData(key);
                    field.getSelectionModel().selectFirst();
                    field.setOnAction(new GameBoardFieldEvent(endTrue));
                    board.add(field, x, y);
                } else {
                    board.add(new Label(String.valueOf(sign)), x, y);
                }
            }
        }
        board.add(endTrue, GameOX.literal.size() - 1, GameOX.number.size());
        return board;
    }

}
