package application.gui.fxgui.scene;

import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;

public final class PlayersScene extends GameScene {

    @Override
    protected void build() {
        stage.setTitle("Tic Tac Toe Game - Players");
        Label question = new Label(gameAction.getMessage());
        root.add(question, 0, 0);
        TextField name = new TextField();
        root.add(name, 0, 1);
        Button next = new Button("next");
        next.setOnMouseClicked(event -> {
            gameAction.setMessage(name.getText());
            stage.hide();
        });
        root.add(next, 0, 2);
    }
}
