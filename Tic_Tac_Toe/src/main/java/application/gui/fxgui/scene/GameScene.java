package application.gui.fxgui.scene;

import application.game.gameengine.Action;
import application.gui.fxgui.events.GameExitEvent;
import javafx.scene.Scene;
import javafx.scene.layout.GridPane;
import javafx.stage.Stage;

public abstract class GameScene extends Scene {

    protected GridPane root;
    protected Stage stage;
    protected Action gameAction;

    GameScene() {
        super(new GridPane());
        root = (GridPane) this.getRoot();
        this.stage = new Stage();
        stage.setOnCloseRequest(new GameExitEvent());
    }

    protected abstract void build();

    public void setGameAction(Action gameAction) {
        this.gameAction = gameAction;
    }

    public void show() {
        build();
        stage.setScene(this);
        stage.showAndWait();
    }
}
