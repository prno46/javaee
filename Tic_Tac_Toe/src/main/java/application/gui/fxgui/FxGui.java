package application.gui.fxgui;

import application.game.gameengine.Action;
import application.game.gameengine.GameOX;
import application.game.gameengine.Singleton;
import application.gui.common.UserInterface;
import application.gui.fxgui.scene.GameBoardScene;
import application.gui.fxgui.scene.PlayersScene;
import javafx.application.Application;
import javafx.stage.Stage;

public class FxGui extends Application implements UserInterface {

    private static final GameOX GAME = Singleton.INSTANCE.getGameOx();

    @Override
    public String getType() {
        return "GUI";
    }

    @Override
    public void start(Stage primaryStage)  {
        GAME.setUI(this);
        Action gameAction = GAME.send(null);
        while(true) {
            if(gameAction.getGameBoard() != null) {
                GameBoardScene boardScene = new GameBoardScene();
                boardScene.setGameAction(gameAction);
                boardScene.show();
            } else {
                PlayersScene playersScene = new PlayersScene();
                playersScene.setGameAction(gameAction);
                playersScene.show();
            }
            gameAction = GAME.send(gameAction);
        }
    }

    public static void main(String[] args) {
        launch(args);
    }
}
