package application.gui.console;

import application.game.exceptions.GameOXException;
import application.game.gameengine.Action;
import application.game.gameengine.GameOX;
import application.game.gameengine.GameStateInterface;
import application.game.gameengine.Singleton;
import application.gui.common.UserInterface;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

public class Cli implements UserInterface {

    private final GameOX GAME;

    public static void main(String[] args) {
        new Cli(Singleton.INSTANCE.getGameOx());
    }

    private Cli(GameOX instance) {
        this.GAME = instance;
        GAME.setUI(this);
        start(GAME.send(null));
    }

    @Override
    public String getType() {
        return "CLI";
    }

    private void drowGameBoard(GameStateInterface gameState) throws GameOXException {
        for(int x = 0; x < GameOX.number.size() + 1; x++) {
            for(int y = 0; y < GameOX.literal.size() + 1; y++) {
                if(x == 0 && y < 3) {
                    System.out.print("   " + GameOX.literal.get(y));
                }
                if(y == 0 && x > 0) {
                    System.out.print(GameOX.number.get(x - 1) + " ");
                }
                if(y > 0 && x > 0) {
                    String coord = GameOX.literal.get(y - 1) + GameOX.number.get(x - 1); // TODO to check why need to switch x with y
                    if(y == GameOX.literal.size()) {
                        System.out.print(" " + gameState.get(coord) + " \n");
                        if(x < GameOX.number.size()) {
                            System.out.print("  ---+---+---");
                        }
                    } else {
                        System.out.print(" " + gameState.get(coord) + " |");
                    }
                }
            }
            System.out.println();
        }
    }

    private String question(Action action) throws GameOXException {
        if(action.getGameBoard() != null) {
            drowGameBoard(action.getGameBoard());
        }
        System.out.print(action.getMessage());
        String msg = "";
        try {
            BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));
            msg = reader.readLine();
        } catch(IOException e) {
            e.printStackTrace();
        }
        return msg;
    }

    private void start(Action action) {
        while(true) {
            try {
                String answer = question(action);
                action.setMessage(answer);
                action = GAME.send(action);
            } catch(GameOXException e) {
                e.printStackTrace();
                break;
            }
        }
    }
}
