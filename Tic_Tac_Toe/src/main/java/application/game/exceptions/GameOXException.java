package application.game.exceptions;

public class GameOXException extends Exception {

    private Error error;

    public enum Error {
        ERROR_EXCEPTION,
        APP_WRONG_COORDINATE_ERROR("Incorrect value of board's coordinate!"),
        APP_NOT_EMPTY_FIELD_ERROR("The field you have specified is already taken!");

        private String message;

        Error() { }

        Error(String message) {
            this.message = message;
        }
        public String getMessage() {
            return message;
        }
    }

    public GameOXException(Error error) {
        super(error.getMessage());
        this.error = error;
    }

    public GameOXException(Error error, Throwable cause) {
        super(cause.getMessage(), cause);
        this.error = error;
    }
}
