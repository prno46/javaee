package application.game.gameengine;

import application.game.gameboard.GameBoard;
import application.game.gameboard.GameBoard.Coordinate;
import application.game.gameengine.GameState.State;
import application.game.player.Player;
import application.gui.common.UserInterface;

import javax.json.*;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileWriter;
import java.util.Arrays;
import java.util.Iterator;
import java.util.List;


public class GameOX {

    public static List<String> literal = Arrays.asList("A", "B", "C");
    public static List<String> number = Arrays.asList("1", "2", "3");
    private static char[] signs = { 'O', 'X' };
    private UserInterface ui;
    private Player[] players;
    private int currentPlayerIndex;
    private int playersCount;
    private GameBoard board;

    GameOX() {
        players = new Player[2];
        playersCount = 0;
        board = new GameBoard();
    }

    public void setUI(UserInterface ui) {
        this.ui = ui;
    }

    private void setNextPlayer() {
        currentPlayerIndex++;
        if(currentPlayerIndex == playersCount) {
            currentPlayerIndex = 0;
        }
    }

    public Action send(Action action) {
        if(playersCount == players.length) {
            if(action != null && action.getMessage().equalsIgnoreCase("save")) {
                action = GameAction.SAVE;
            } else if(action != null && action.getMessage().equalsIgnoreCase("load")) {
                action = GameAction.LOAD;
            } else if(action != null && action.getMessage().equalsIgnoreCase("new")) {
                action = GameAction.NEW;
            } else if(action != null && action.getMessage().equalsIgnoreCase("exit")) {
                action = GameAction.EXIT;
            }
        }
        Action returnAction = action;
        try {
            if(action == null) {
                returnAction = GameAction.PLAYERS;
            }
            GameAction gameAction = (GameAction)returnAction;
            String msg = "";
            switch(gameAction) {
                case EXIT:
                    System.exit(0);
                    break;
                case LOAD:
                    File loadFile = new File("save/save.json");
                    try(JsonReader reader = Json.createReader(new FileInputStream(loadFile))) {
                        JsonObject json = reader.readObject();
                        json.getString("currentPlayer");
                    }
                    break;
                case NEW:
                    board.clearGameBoard();
                    currentPlayerIndex = 0;
                    returnAction = GameAction.PLAY;
                    returnAction.setMessage("New Game!\nNext Move " + players[currentPlayerIndex].getName() + "( " + players[currentPlayerIndex].getSign() + " ): ");
                    break;
                case PLAY:
                    GameState.State state;
                    if(ui.getType().equals("GUI")) {
                        Coordinate coord = gameAction.getCoords();
                        state = board.setField(coord, players[currentPlayerIndex].getSign());
                        returnAction.setStatus(state);
                    } else {
                        String coord = gameAction.getMessage();
                        state = board.setField(coord, players[currentPlayerIndex].getSign());
                    }
                    switch(state) {
                        case PAT:
                            msg = "No more moves available, it is a TIE!";
                            break;
                        case RUN:
                            setNextPlayer();
                            msg = "Next Move " + players[currentPlayerIndex].getName() + "( " + players[currentPlayerIndex].getSign() + " ): ";
                            break;
                        case WIN:
                            msg = "WINS " + players[currentPlayerIndex].getName();
                            break;
                    }
                    returnAction.setMessage(msg);
                    break;
                case PLAYERS:
                    msg = "Enter player name: ";
                    if(gameAction.getMessage() != null) {
                        players[playersCount] = new Player(gameAction.getMessage(), GameOX.signs[playersCount]);
                        playersCount++;
                        msg += (playersCount + 1) + ": ";
                        returnAction.setMessage(msg);
                        if(playersCount == players.length) {
                            currentPlayerIndex = 0;
                            gameAction = GameAction.PLAY;
                            gameAction.setGameBoard(board.getGameState());
                            gameAction.setStatus(State.RUN);
                            returnAction = gameAction;
                            msg = "Next Move " + players[currentPlayerIndex].getName() + "( " + players[currentPlayerIndex].getSign() + " ): ";
                        }
                    } else {
                        msg += (playersCount + 1) + ": ";
                    }
                    returnAction.setMessage(msg);
                    break;
                case SAVE:
                    File saveFile = new File("save/save.json");
                    if(!saveFile.exists()) {
                        saveFile.createNewFile();
                    }
                    JsonObjectBuilder jsonBuilder = Json.createObjectBuilder();
                    jsonBuilder.add("currentPlayer", String.valueOf(players[currentPlayerIndex].getSign()));
                    JsonArrayBuilder gameState = Json.createArrayBuilder();
                    Iterator<Coordinate> keys = board.getBoardCoordinate();
                    while(keys.hasNext()) {
                        Coordinate key = keys.next();
                        JsonObjectBuilder boardField = Json.createObjectBuilder();
                        boardField.add("x", key.getX());
                        boardField.add("y", key.getY());
                        boardField.add("s", String.valueOf(board.getGameState().get(key)));
                        gameState.add(boardField);
                    }
                    jsonBuilder.add("gameState", gameState);
                    try(FileWriter writer = new FileWriter(saveFile)) {
                        writer.write(jsonBuilder.build().toString());
                    }
                    returnAction = GameAction.PLAY;
                    returnAction.setMessage("Game was saved!\nNext Move " + players[currentPlayerIndex].getName() + "( " + players[currentPlayerIndex].getSign() + " ): ");
                    break;
                default:
                    break;
            }
        } catch(Exception e) {
            returnAction.setMessage(e.getMessage());
        }

        if(ui.getType().equals("GUI")) {
            ((GameAction)returnAction).setCurrentPlayer(players[currentPlayerIndex]);
        }
        return returnAction;
    }
}
