package application.game.gameengine;

import application.game.gameboard.GameBoard.Coordinate;
import application.game.player.Player;
import application.game.gameengine.GameState.State;

public enum GameAction implements Action {

    PLAYERS, PLAY, EXIT, SAVE, LOAD, NEW;

    private String msg;
    private GameStateInterface gameState;
    private Player currentPlayer;
    private Coordinate coords;
    private State gameStatus;

    public void setGameBoard(GameStateInterface gameState) {
        this.gameState = gameState;
    }

    @Override
    public GameStateInterface getGameBoard() {
        return gameState;
    }

    @Override
    public String getMessage() {
        return msg;
    }

    @Override
    public void setMessage(String msg) {
        this.msg = msg;
    }

    public void setCurrentPlayer(Player currentPlayer) {
        this.currentPlayer = currentPlayer;
    }

    @Override
    public Player getCurrentPlayer() {
        return currentPlayer;
    }

    @Override
    public void setCoords(Coordinate coords) {
        this.coords = coords;
    }

    @Override
    public Coordinate getCoords() {
        return coords;
    }

    @Override
    public void setStatus(State status) {
        this.gameStatus = status;
    }

    @Override
    public State getStatus() {
        return gameStatus;
    }

}
