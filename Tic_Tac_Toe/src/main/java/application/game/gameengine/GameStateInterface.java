package application.game.gameengine;
import application.game.gameboard.GameBoard.Coordinate;
import application.game.exceptions.GameOXException;

public interface GameStateInterface {

     char get(String coord) throws GameOXException;
     char get(Coordinate coord);
     Coordinate getKey(int x, int y) throws GameOXException;
}
