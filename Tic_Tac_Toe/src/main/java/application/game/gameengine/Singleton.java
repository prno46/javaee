package application.game.gameengine;

public enum Singleton {
    INSTANCE;

    public GameOX getGameOx() {
        return new GameOX();
    }
}
