package application.game.gameengine;

import application.game.exceptions.GameOXException;
import application.game.gameboard.GameBoard.Coordinate;

import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;

import static application.game.exceptions.GameOXException.Error.APP_WRONG_COORDINATE_ERROR;

public class GameState implements GameStateInterface {

    public enum State { WIN, PAT, RUN }

    private Map<Coordinate, Character> boardState;
    private char defaultSign;
    private int sizeX;
    private int sizeY;

    public GameState(int sizeX, int sizeY, char defaultSign) {
        this.defaultSign = defaultSign;
        this.sizeX = sizeX;
        this.sizeY = sizeY;
        boardState = new HashMap<>();
        clear();
    }

    public void clear() {
        for(int iy = 0; iy < sizeY; iy++) {
            for(int ix = 0; ix < sizeX; ix++) {
                boardState.put(new Coordinate(ix, iy), defaultSign);
            }
        }
    }

    public Iterator<Coordinate> getCoordinate() {
        return boardState.keySet().iterator();
    }

    public State set(String coord, char sign) throws GameOXException {
        return set(getKeyByString(coord), sign);
    }

    public State set(Coordinate coord, char sign) throws GameOXException {
        boardState.put(coord, sign);
        return checkGameState();
    }

    private boolean isNotEmpty() {
        for (Coordinate coordinate : boardState.keySet()) {
            char sign = boardState.get(coordinate);
            if (sign == defaultSign) {
                return false;
            }
        }
        return true;
    }

    private State checkGameState() throws GameOXException {
        if(
                (!boardState.get(getKey(0, 0)).equals(defaultSign) && boardState.get(getKey(0, 1)).equals(boardState.get(getKey(0, 0))) && boardState.get(getKey(0, 2)).equals(boardState.get(getKey(0, 0)))) ||
                        (!boardState.get(getKey(1, 0)).equals(defaultSign) && boardState.get(getKey(1, 1)).equals(boardState.get(getKey(1, 0))) && boardState.get(getKey(1, 2)).equals(boardState.get(getKey(1, 0)))) ||
                        (!boardState.get(getKey(2, 0)).equals(defaultSign) && boardState.get(getKey(2, 1)).equals(boardState.get(getKey(2, 0))) && boardState.get(getKey(2, 2)).equals(boardState.get(getKey(2, 0)))) ||
                        (!boardState.get(getKey(0, 0)).equals(defaultSign) && boardState.get(getKey(1, 0)).equals(boardState.get(getKey(0, 0))) && boardState.get(getKey(2, 0)).equals(boardState.get(getKey(0, 0)))) ||
                        (!boardState.get(getKey(0, 1)).equals(defaultSign) && boardState.get(getKey(1, 1)).equals(boardState.get(getKey(0, 1))) && boardState.get(getKey(2, 1)).equals(boardState.get(getKey(0, 1)))) ||
                        (!boardState.get(getKey(0, 2)).equals(defaultSign) && boardState.get(getKey(1, 2)).equals(boardState.get(getKey(0, 2))) && boardState.get(getKey(2, 2)).equals(boardState.get(getKey(0, 2)))) ||
                        (!boardState.get(getKey(0, 0)).equals(defaultSign) && boardState.get(getKey(1, 1)).equals(boardState.get(getKey(0, 0))) && boardState.get(getKey(2, 2)).equals(boardState.get(getKey(0, 0)))) ||
                        (!boardState.get(getKey(0, 2)).equals(defaultSign) && boardState.get(getKey(1, 1)).equals(boardState.get(getKey(1, 1))) && boardState.get(getKey(2, 0)).equals(boardState.get(getKey(0, 2))))
        ) {
            return State.WIN;
        }
        if(isNotEmpty()) {
            return State.PAT;
        }
        return State.RUN;
    }

    @Override
    public Coordinate getKey(int x, int y) throws GameOXException {
        Coordinate coordKey = new Coordinate(x, y);
        for (Coordinate key : boardState.keySet()) {
            if (coordKey.equals(key)) {
                return key;
            }
        }
        throw new GameOXException(APP_WRONG_COORDINATE_ERROR);
    }

    private Coordinate getKeyByString(String strCoord) throws GameOXException {
        String sx = strCoord.substring(0, 1);
        String sy = strCoord.substring(1);
        Coordinate coordKey = new Coordinate(sx, sy);
        for (Coordinate key : boardState.keySet()) {
            if (coordKey.equals(key)) {
                return key;
            }
        }
        throw new GameOXException(APP_WRONG_COORDINATE_ERROR);
    }

    @Override
    public char get(String coord) throws GameOXException {
        return boardState.get(getKeyByString(coord));
    }

    @Override
    public char get(Coordinate coord) {
        return boardState.get(coord);
    }
}
