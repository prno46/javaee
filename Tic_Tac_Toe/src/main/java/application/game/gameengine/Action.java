package application.game.gameengine;

import application.game.player.Player;
import application.game.gameboard.GameBoard.Coordinate;
import application.game.gameengine.GameState.State;

public interface Action {

     GameStateInterface getGameBoard();
     String getMessage();
     void setMessage(String msg);
     Player getCurrentPlayer();
     void setCoords(Coordinate coords);
     Coordinate getCoords();
     void setStatus(State status);
     State getStatus();
}
