package application.game.gameboard;

import application.game.exceptions.GameOXException;
import application.game.gameengine.GameOX;
import application.game.gameengine.GameState;
import application.game.gameengine.GameStateInterface;

import java.util.Iterator;

import static application.game.exceptions.GameOXException.Error.APP_NOT_EMPTY_FIELD_ERROR;

public class GameBoard {

    private static char defaultSign = ' ';
    private GameState gameState;

    public GameBoard() {
        gameState = new GameState(GameOX.literal.size(), GameOX.number.size(), ' ');
    }

    public GameStateInterface getGameState() {
        return gameState;
    }

    public Iterator<Coordinate> getBoardCoordinate() {
        return gameState.getCoordinate();
    }

    public void clearGameBoard() {
        gameState.clear();
    }

    public GameState.State setField(String strCoord, char playerSign) throws GameOXException {
        char sign = gameState.get(strCoord);
        if(sign == GameBoard.defaultSign) {
            return gameState.set(strCoord, playerSign);
        } else {
            throw new GameOXException(APP_NOT_EMPTY_FIELD_ERROR);
        }
    }

    public GameState.State setField(Coordinate coords, char playerSign) throws GameOXException {
        return gameState.set(coords, playerSign);
    }

    public static class Coordinate {

        private int x;
        private int y;

        public Coordinate(String x, String y) throws GameOXException {
            try {
                for(int ix = 0; ix < GameOX.literal.size(); ix++) {
                    if(GameOX.literal.get(ix).equalsIgnoreCase(x)) {
                        this.x = ix;
                        break;
                    }
                }
                for(int iy = 0; iy < GameOX.number.size(); iy++) {
                    if(GameOX.number.get(iy).equalsIgnoreCase(y)) {
                        this.y = iy;
                        break;
                    }
                }
            } catch(Exception e) {
                throw new GameOXException(GameOXException.Error.ERROR_EXCEPTION, e);
            }
        }

        public Coordinate(int x, int y) {
            this.x = x;
            this.y = y;
        }

        public boolean equals(Coordinate coord) {
            return (coord.getX() == x && coord.getY() == y);
        }

        public int getX() {
            return x;
        }

        public int getY() {
            return y;
        }

        public String toString() {
            return "[ x: " + x + " y: " + y + " ]" + this.getClass().getName() + "\n";
        }
    }
}
