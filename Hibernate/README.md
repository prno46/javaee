# Hibernate

The idea behind this project was to make it as Hibernate cheat sheet.

Project contains a basic examples of Hibernate configuration and entity mappings.

Each module of the project contains one Hibernate mapping example (Basic case).


## Prerequisites

Framework version used for this project:

```
Hibernate 5.+ 
```

## Built With

* [Maven](https://maven.apache.org/) - Dependency Management

## Authors

* **Przemyslaw Nowacki** 




