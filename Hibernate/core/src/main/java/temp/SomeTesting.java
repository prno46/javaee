package temp;

import static java.lang.Math.abs;

class SomeTesting {

    public static void main(String[] args) {

        int x = -1;
        int y = Integer.MIN_VALUE;

        // Integer.MAX_VALUE - Integer.MIN_VALUE -> -1
        // Integer.MAX_VALUE + Integer.MIN_VALUE -> -1

        /*boolean answer = isTrue(x, y);

        System.out.println(answer);
        System.out.println(-1 + Integer.MIN_VALUE);*/

        // ----------------------- regs test -----------------------------
        String st = "{gdgfd[fd(df ] d54f)fdASD]}";
        // matching all letters in a string and replacing it with empty string
        String ans = st.replaceAll("[a-zA-Z\\d\\s]", "");
        int len;
        do {
            len = ans.length();
            ans = ans.replace("()", "");
            ans = ans.replace("[]", "");
            ans = ans.replace("{}", "");
        }while (len != ans.length());
        System.out.println(ans);

    }
    // false -> ok; true -> not ok
    private static Boolean isTrue(int x, int y) {
        return abs(x) > Integer.MAX_VALUE - abs(y);
    }
}
