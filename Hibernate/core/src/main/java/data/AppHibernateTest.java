package data;

import data.entities.User;
import org.hibernate.Session;

import java.util.Date;

class AppHibernateTest {
    public static void main(String[] args) {
        Session session = HibernateUtil.getSessionFactory().openSession();
        session.getTransaction().begin();

        User user = new User();
        user.setFirstName("Maryjan");
        user.setLastName("Sniety");
        user.setBirthDate(new Date());
        user.setEmailAddress("someMail@gmail.com");
        user.setLastUpdate(new Date());
        user.setLastUpdatedBy("Maryjan");
        user.setCreatedDate(new Date());
        user.setCreatedBy("Ola");
        user.setUserAddressLineOne("first line of address");
        user.setUserAddressLineTwo("second line of address");
        user.setCity("New York");
        user.setState("NY");
        user.setZipCode("85478");

        session.save(user);

        session.getTransaction().commit();
        session.close();
    }
}
