package data.entities;

import java.math.BigDecimal;
import java.util.Date;

class Transaction {

    private Long transactionId;

    private Account account;

    private String transactionType;

    private BigDecimal amount;

    private BigDecimal initialBalance;

    private BigDecimal closingBalance;

    private String notes;

    private Date lastUpdatedDate;

    private String lastUpdatedBy;

    private Date createdDate;

    private String createdBy;

    Long getTransactionId() {
        return transactionId;
    }

    void setTransactionId(Long transactionId) {
        this.transactionId = transactionId;
    }

    Account getAccount() {
        return account;
    }

    void setAccount(Account account) {
        this.account = account;
    }

    String getTransactionType() {
        return transactionType;
    }

    void setTransactionType(String transactionType) {
        this.transactionType = transactionType;
    }

    BigDecimal getAmount() {
        return amount;
    }

    void setAmount(BigDecimal amount) {
        this.amount = amount;
    }

    BigDecimal getInitialBalance() {
        return initialBalance;
    }

    void setInitialBalance(BigDecimal initialBalance) {
        this.initialBalance = initialBalance;
    }

    BigDecimal getClosingBalance() {
        return closingBalance;
    }

    void setClosingBalance(BigDecimal closingBalance) {
        this.closingBalance = closingBalance;
    }

    String getNotes() {
        return notes;
    }

    void setNotes(String notes) {
        this.notes = notes;
    }

    Date getLastUpdatedDate() {
        return lastUpdatedDate;
    }

    void setLastUpdatedDate(Date lastUpdatedDate) {
        this.lastUpdatedDate = lastUpdatedDate;
    }

    String getLastUpdatedBy() {
        return lastUpdatedBy;
    }

    void setLastUpdatedBy(String lastUpdatedBy) {
        this.lastUpdatedBy = lastUpdatedBy;
    }

    Date getCreatedDate() {
        return createdDate;
    }

    void setCreatedDate(Date createdDate) {
        this.createdDate = createdDate;
    }

    String getCreatedBy() {
        return createdBy;
    }

    void setCreatedBy(String createdBy) {
        this.createdBy = createdBy;
    }
}
