package data.entities;

import java.util.Date;

class Bank {

    private Long bankId;

    private String name;

    private String addressLine1;

    private String addressLine2;

    private String city;

    private String state;

    private String zipCode;

    private Date lastUpdatedDate;

    private String lastUpdatedBy;

    private Date createdDate;

    private String createdBy;

    Long getBankId() {
        return bankId;
    }

    void setBankId(Long bankId) {
        this.bankId = bankId;
    }

    String getName() {
        return name;
    }

    void setName(String name) {
        this.name = name;
    }

    String getAddressLine1() {
        return addressLine1;
    }

    void setAddressLine1(String addressLine1) {
        this.addressLine1 = addressLine1;
    }

    String getAddressLine2() {
        return addressLine2;
    }

    void setAddressLine2(String addressLine2) {
        this.addressLine2 = addressLine2;
    }

    String getCity() {
        return city;
    }

    void setCity(String city) {
        this.city = city;
    }

    String getState() {
        return state;
    }

    void setState(String state) {
        this.state = state;
    }

    String getZipCode() {
        return zipCode;
    }

    void setZipCode(String zipCode) {
        this.zipCode = zipCode;
    }

    Date getLastUpdatedDate() {
        return lastUpdatedDate;
    }

    void setLastUpdatedDate(Date lastUpdatedDate) {
        this.lastUpdatedDate = lastUpdatedDate;
    }

    String getLastUpdatedBy() {
        return lastUpdatedBy;
    }

    void setLastUpdatedBy(String lastUpdatedBy) {
        this.lastUpdatedBy = lastUpdatedBy;
    }

    Date getCreatedDate() {
        return createdDate;
    }

    void setCreatedDate(Date createdDate) {
        this.createdDate = createdDate;
    }

    String getCreatedBy() {
        return createdBy;
    }

    void setCreatedBy(String createdBy) {
        this.createdBy = createdBy;
    }
}
