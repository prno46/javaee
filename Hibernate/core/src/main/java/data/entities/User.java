package data.entities;

import javax.persistence.*;
import java.util.Date;

@Entity
@Table(name="finances_user")
@Access(value = AccessType.FIELD)
public class User {

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE)
    @Column(name = "user_id")
    private Long userId;

    @Column(name = "first_name")
    private String firstName;

    @Column(name = "last_name")
    private String lastName;

    @Column(name = "birth_date", nullable = false) // does not allows nulls on this filed on java level - hibernate
    private Date birthDate;

    @Column(name = "email_address")
    private String emailAddress;

    @Column(name = "last_updated_date")
    private Date lastUpdate;

    @Column(name = "last_updated_by")
    private String lastUpdatedBy;

    @Column(name = "created_date", updatable = false) // this field will not be updated during update operation
    private Date createdDate;

    @Column(name = "created_by", updatable = false)
    private String createdBy;

    @Column(name = "user_address_line_1")
    private String userAddressLineOne;

    @Column(name = "user_address_line_2")
    private String userAddressLineTwo;

    @Column(name = "city")
    private String city;

    @Column(name = "state")
    private String state;

    @Column(name = "zip_code")
    private String zipCode;

    public Long getUserId() {
        return userId;
    }

    public void setUserId(Long userId) {
        this.userId = userId;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public Date getBirthDate() {
        return birthDate;
    }

    public void setBirthDate(Date birthDate) {
        this.birthDate = birthDate;
    }

    public String getEmailAddress() {
        return emailAddress;
    }

    public void setEmailAddress(String emailAddress) {
        this.emailAddress = emailAddress;
    }

    public Date getLastUpdate() {
        return lastUpdate;
    }

    public void setLastUpdate(Date lastUpdate) {
        this.lastUpdate = lastUpdate;
    }

    public String getLastUpdatedBy() {
        return lastUpdatedBy;
    }

    public void setLastUpdatedBy(String lastUpdatedBy) {
        this.lastUpdatedBy = lastUpdatedBy;
    }

    public Date getCreatedDate() {
        return createdDate;
    }

    public void setCreatedDate(Date createdDate) {
        this.createdDate = createdDate;
    }

    public String getCreatedBy() {
        return createdBy;
    }

    public void setCreatedBy(String createdBy) {
        this.createdBy = createdBy;
    }

    public String getUserAddressLineOne() {
        return userAddressLineOne;
    }

    public void setUserAddressLineOne(String userAddressLineOne) {
        this.userAddressLineOne = userAddressLineOne;
    }

    public String getUserAddressLineTwo() {
        return userAddressLineTwo;
    }

    public void setUserAddressLineTwo(String userAddressLineTwo) {
        this.userAddressLineTwo = userAddressLineTwo;
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public String getState() {
        return state;
    }

    public void setState(String state) {
        this.state = state;
    }

    public String getZipCode() {
        return zipCode;
    }

    public void setZipCode(String zipCode) {
        this.zipCode = zipCode;
    }
}
