package data.entities;

import java.math.BigDecimal;
import java.util.Date;

class Account {
    private Long accountId;

    private Bank bank;

    private AccountType accountType;

    private String name;

    private BigDecimal initialBalance;

    private BigDecimal currentBalance;

    private Date openDate;

    private Date closeDate;

    private Date lastUpdatedDate;

    private String lastUpdatedBy;

    private Date createdDate;

    private String createdBy;

    Long getAccountId() {
        return accountId;
    }

    void setAccountId(Long accountId) {
        this.accountId = accountId;
    }

    Bank getBank() {
        return bank;
    }

    void setBank(Bank bank) {
        this.bank = bank;
    }

    AccountType getAccountType() {
        return accountType;
    }

    void setAccountType(AccountType accountType) {
        this.accountType = accountType;
    }

    String getName() {
        return name;
    }

    void setName(String name) {
        this.name = name;
    }

    BigDecimal getInitialBalance() {
        return initialBalance;
    }

    void setInitialBalance(BigDecimal initialBalance) {
        this.initialBalance = initialBalance;
    }

    BigDecimal getCurrentBalance() {
        return currentBalance;
    }

    void setCurrentBalance(BigDecimal currentBalance) {
        this.currentBalance = currentBalance;
    }

    Date getOpenDate() {
        return openDate;
    }

    void setOpenDate(Date openDate) {
        this.openDate = openDate;
    }

    Date getCloseDate() {
        return closeDate;
    }

    void setCloseDate(Date closeDate) {
        this.closeDate = closeDate;
    }

    Date getLastUpdatedDate() {
        return lastUpdatedDate;
    }

    void setLastUpdatedDate(Date lastUpdatedDate) {
        this.lastUpdatedDate = lastUpdatedDate;
    }

    String getLastUpdatedBy() {
        return lastUpdatedBy;
    }

    void setLastUpdatedBy(String lastUpdatedBy) {
        this.lastUpdatedBy = lastUpdatedBy;
    }

    Date getCreatedDate() {
        return createdDate;
    }

    void setCreatedDate(Date createdDate) {
        this.createdDate = createdDate;
    }

    String getCreatedBy() {
        return createdBy;
    }

    void setCreatedBy(String createdBy) {
        this.createdBy = createdBy;
    }
}
