package data.entities;

import java.util.Date;

class AccountType {

    private Long accountTypeId;

    private String name;

    private Date lastUpdatedDate;

    private String lastUpdatedBy;

    private Date createdDate;

    private String createdBy;

    Long getAccountTypeId() {
        return accountTypeId;
    }

    void setAccountTypeId(Long accountTypeId) {
        this.accountTypeId = accountTypeId;
    }

    String getName() {
        return name;
    }

    void setName(String name) {
        this.name = name;
    }

    Date getLastUpdatedDate() {
        return lastUpdatedDate;
    }

    void setLastUpdatedDate(Date lastUpdatedDate) {
        this.lastUpdatedDate = lastUpdatedDate;
    }

    String getLastUpdatedBy() {
        return lastUpdatedBy;
    }

    void setLastUpdatedBy(String lastUpdatedBy) {
        this.lastUpdatedBy = lastUpdatedBy;
    }

    Date getCreatedDate() {
        return createdDate;
    }

    void setCreatedDate(Date createdDate) {
        this.createdDate = createdDate;
    }

    String getCreatedBy() {
        return createdBy;
    }

    void setCreatedBy(String createdBy) {
        this.createdBy = createdBy;
    }
}
