package data.entities.bidirectionalOneToMany;

import java.math.BigDecimal;

class AccountOTM {
    private Long accountId;

    private String name;

    private BigDecimal initialBalance;

    private BigDecimal currentBalance;

    Long getAccountId() {
        return accountId;
    }

    String getName() {
        return name;
    }

    void setName(String name) {
        this.name = name;
    }

    BigDecimal getInitialBalance() {
        return initialBalance;
    }

    void setInitialBalance(BigDecimal initialBalance) {
        this.initialBalance = initialBalance;
    }

    BigDecimal getCurrentBalance() {
        return currentBalance;
    }

    void setCurrentBalance(BigDecimal currentBalance) {
        this.currentBalance = currentBalance;
    }

}
