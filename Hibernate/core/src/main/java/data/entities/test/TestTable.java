package data.entities.test;


import javax.persistence.*;

@Entity(name = "Test_Table")
@Table(name = "test_table")
class TestTable {

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "test_id_seq")
    @SequenceGenerator(name = "test_id_seq", sequenceName = "test_seq", allocationSize = 1)
    @Column(name = "test_id")
    private Long id;

    @Column(name = "test_name")
    private String name;

    @Embedded
    private Address address;

    TestTable() { }

    TestTable(String name, Address address) {
        this.name = name;
        this.address = address;
    }

    Long getId() {
        return id;
    }

    String getName() {
        return name;
    }

    Address getAddress() {
        return address;
    }
}
