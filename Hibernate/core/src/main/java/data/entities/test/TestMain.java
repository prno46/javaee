package data.entities.test;

import data.HibernateUtil;
import org.hibernate.HibernateException;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;

import java.util.Optional;

class TestMain {

    public static void main(String[] args) {

        Optional<SessionFactory> oFactory = Optional.of(HibernateUtil.getSessionFactory());
        Optional<Session> oSession = oFactory.map(SessionFactory::openSession);
        Optional<Transaction> oTransaction = oSession.map(Session::beginTransaction);
        try {
            Address address = new Address("Kalinowa" ,"New Kalina", "874-852147");
            TestTable testTable = new TestTable("some test data", address);

            oSession.ifPresent(s -> s.save(testTable));

            oTransaction.ifPresent(Transaction::commit);

        } catch (HibernateException e) {
            oTransaction.ifPresent(Transaction::rollback);
            e.getMessage();
        } finally {
            oSession.ifPresent(Session::close);
            oFactory.ifPresent(SessionFactory::close);
        }
    }
}
