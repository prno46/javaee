package data.entities.test;

import javax.persistence.Column;
import javax.persistence.Embeddable;

@Embeddable
class Address {

    @Column(name = "test_street")
    private String street;

    @Column(name = "test_city")
    private String city;

    @Column(name = "test_postal_code")
    private String postalCode;

    Address() { }

    Address(String street, String city, String postalCode) {
        this.street = street;
        this.city = city;
        this.postalCode = postalCode;
    }

    String getStreet() {
        return street;
    }

    String getCity() {
        return city;
    }

    String getPostalCode() {
        return postalCode;
    }
}
