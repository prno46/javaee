package bidirectionalManyToMany.entities;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.List;

/***
 * we are going to have one to one relation between user entity and account entity
 * one user can have only one account (just an example)
 * SOMME COMMON TERMS:
 * Source Entity or owning entity is the one that holds the foreign key to another entity
 * Target Entity or non-owning entity is the one that originally does not have a foreign key field to another entity
 *
 * in our case User is the Target entity and Account is the Source entity
 */

@Entity
@Table(name="otm_user")
@Access(value = AccessType.FIELD)
public class UserMTM {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name = "user_id")
    private Long userId;

    @Column(name = "first_name")
    private String firstName;

    @Column(name = "last_name")
    private String lastName;

    @Column(name = "email_address")
    private String emailAddress;

    /*
         property cascade of @ToMany annotation has to be in the entity which is gong to be persisted
         to the database -> in this case it is user Entity
     */

    @ManyToMany(targetEntity = AccountMTM.class, mappedBy = "userMTMS", cascade = CascadeType.ALL)
    private List<AccountMTM> accountMTMS;

    public UserMTM() {
        accountMTMS = new ArrayList<>(5);
    }

    public Long getUserId() {
        return userId;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getEmailAddress() {
        return emailAddress;
    }

    public void setEmailAddress(String emailAddress) {
        this.emailAddress = emailAddress;
    }


    public List<AccountMTM> getAccountMTMS() {
        return accountMTMS;
    }

    public void setAccountMTMS(List<AccountMTM> accountMTMS) {
        this.accountMTMS = accountMTMS;
    }
}
