package bidirectionalManyToMany.entities;

import javax.persistence.*;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

@Entity
@Table(name = "otm_account_table")
@Access(value = AccessType.FIELD)
public class AccountMTM {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name = "account_id")
    private Long accountId;

    @Column(name = "name")
    private String name;

    @Column(name = "initial_balance")
    private BigDecimal initialBalance;

    @Column(name = "current_balance")
    private BigDecimal currentBalance;

    /*  the relation many to many is set through additional table the joins the primary keys of two entities
        in that kind of relation it does not matter which table is target / not owning or source / owning (a foreign key)
        in my case Account is a source table. In both tables we have a fields of type Collection
     */
    @ManyToMany(targetEntity = UserMTM.class)
    @JoinTable(name = "join_table",                                                               // the name of joining table
            joinColumns = @JoinColumn(name = "account_pk", referencedColumnName = "account_id"), // source entity
            inverseJoinColumns = @JoinColumn(name = "user_pk", referencedColumnName = "user_id")) // target entity
    private List<UserMTM> userMTMS;

    public AccountMTM() {
        userMTMS = new ArrayList<>(5);
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public BigDecimal getInitialBalance() {
        return initialBalance;
    }

    public void setInitialBalance(BigDecimal initialBalance) {
        this.initialBalance = initialBalance;
    }

    public BigDecimal getCurrentBalance() {
        return currentBalance;
    }

    public void setCurrentBalance(BigDecimal currentBalance) {
        this.currentBalance = currentBalance;
    }

    public Long getAccountId() {
        return accountId;
    }

    public List<UserMTM> getUserMTMS() {
        return userMTMS;
    }

    public void setUserMTMS(List<UserMTM> userMTMS) {
        this.userMTMS = userMTMS;
    }
}
