package bidirectionalManyToMany;

import bidirectionalManyToMany.entities.AccountMTM;
import bidirectionalManyToMany.entities.UserMTM;
import org.hibernate.HibernateException;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;

import java.math.BigDecimal;
import java.util.Optional;

class MTMMain {

    public static void main(String[] args) {

        Optional<SessionFactory> oFactory = Optional.of(HibernateUtil.getSessionFactory());
        Optional<Session> oSession = oFactory.map(SessionFactory::openSession);
        Optional<Transaction> oTransaction = oSession.map(Session::beginTransaction);
        try {

            AccountMTM accountUS = new AccountMTM();
            AccountMTM accountHA = new AccountMTM();
            AccountMTM accountWN = new AccountMTM();

            UserMTM userOTO1 = new UserMTM();
            UserMTM userOTO2 = new UserMTM();

            // setting user1
            userOTO1.setFirstName("Bruce");
            userOTO1.setLastName("Wayne");
            userOTO1.setEmailAddress("b.wayne@gmail.com");

            // setting user2
            userOTO2.setFirstName("John");
            userOTO2.setLastName("Wick");
            userOTO2.setEmailAddress("j.wick@gmail.com");

            // setting account -> we need to set user for account and account for user so this bidirectional relation could work

            accountUS.setName(userOTO1.getFirstName());
            accountUS.setInitialBalance(new BigDecimal(2500));
            accountUS.setCurrentBalance(new BigDecimal(8450));

            accountHA.setName(userOTO2.getFirstName());
            accountHA.setInitialBalance(new BigDecimal(2900));
            accountHA.setCurrentBalance(new BigDecimal(9450));

            accountWN.setName(userOTO1.getFirstName());
            accountWN.setInitialBalance(new BigDecimal(8500));
            accountWN.setCurrentBalance(new BigDecimal(128450));

            accountUS.getUserMTMS().add(userOTO1);
            accountWN.getUserMTMS().add(userOTO1);
            accountHA.getUserMTMS().add(userOTO2);

            userOTO1.getAccountMTMS().add(accountUS);
            userOTO1.getAccountMTMS().add(accountWN);
            userOTO2.getAccountMTMS().add(accountHA);

            // saving list of accounts and a user in database
            oSession.ifPresent(s-> s.save(userOTO1));
            oSession.ifPresent(s-> s.save(userOTO2));

            // retrieving account data through user
            if(oSession.isPresent()) {
                System.out.println();
            }

            oTransaction.ifPresent(Transaction::commit);

        } catch (HibernateException e) {
            oTransaction.ifPresent(Transaction::rollback);
            e.printStackTrace();
        } finally {
            oSession.ifPresent(Session::close);
            oFactory.ifPresent(SessionFactory::close);
        }

    }

}
