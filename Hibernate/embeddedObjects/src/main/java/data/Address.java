package data;

import javax.persistence.Column;
import javax.persistence.Embeddable;
/*
    to persist an object which is a part of entity we need to annotate it with
    an annotation @Embeddable - this object cannot contain a id and entity annotations
    we can annotate the fields with @Column annotation
    in the entity class we need to annotate the field representing type of this class with @Embedded
 */
@Embeddable
class Address {

    @Column(name = "street")
    private String addressLine;

    @Column(name = "city")
    private String city;

    @Column(name = "state")
    private String state;

    @Column(name = "zip_code")
    private String zipCode;

    public Address() { }

    public Address(String addressLine, String city, String state, String zipCode) {
        this.addressLine = addressLine;
        this.city = city;
        this.state = state;
        this.zipCode = zipCode;
    }

}
