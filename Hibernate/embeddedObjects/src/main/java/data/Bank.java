package data;

import javax.persistence.*;

@Entity
@Table(name = "bank_table")
@Access(value = AccessType.FIELD)
class Bank {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long bankId;

    @Column(name = "bank_name")
    private String name;

    @Embedded
    private Address address;

    public Bank() { }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Long getBankId() {
        return bankId;
    }

    public void setAddress(Address address) {
        this.address = address;
    }
}
