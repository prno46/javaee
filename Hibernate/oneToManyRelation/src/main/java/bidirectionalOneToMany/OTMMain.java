package bidirectionalOneToMany;

import bidirectionalOneToMany.entities.AccountOTM;
import bidirectionalOneToMany.entities.UserOTM;
import org.hibernate.HibernateException;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;

import java.math.BigDecimal;
import java.util.Optional;

class OTMMain {

    public static void main(String[] args) {

        Optional<SessionFactory> oFactory = Optional.of(HibernateUtil.getSessionFactory());
        Optional<Session> oSession = oFactory.map(SessionFactory::openSession);
        Optional<Transaction> oTransaction = oSession.map(Session::beginTransaction);
        try {

            AccountOTM accountUS = new AccountOTM();
            AccountOTM accountHA = new AccountOTM();
            AccountOTM accountWN = new AccountOTM();

            UserOTM userOTO = new UserOTM();

            // setting user
            userOTO.setFirstName("Bruce");
            userOTO.setLastName("Wayne");
            userOTO.setEmailAddress("b.wayne@gmail.com");

            // setting account -> we need to set user for account and account for user so this bidirectional relation could work
            accountUS.setUserOTM(userOTO);
            accountUS.setName(userOTO.getFirstName());
            accountUS.setInitialBalance(new BigDecimal(2500));
            accountUS.setCurrentBalance(new BigDecimal(8450));

            accountHA.setUserOTM(userOTO);
            accountHA.setName(userOTO.getFirstName());
            accountHA.setInitialBalance(new BigDecimal(2900));
            accountHA.setCurrentBalance(new BigDecimal(9450));

            accountWN.setUserOTM(userOTO);
            accountWN.setName(userOTO.getFirstName());
            accountWN.setInitialBalance(new BigDecimal(8500));
            accountWN.setCurrentBalance(new BigDecimal(128450));

            // adding accounts to user
            userOTO.getAccountOTMList().add(accountUS);
            userOTO.getAccountOTMList().add(accountHA);
            userOTO.getAccountOTMList().add(accountWN);

            // saving list of accounts and a user in database
            oSession.ifPresent(s-> s.save(userOTO));

            // retrieving account data through user
            if(oSession.isPresent()) {
              AccountOTM accountOTM = oSession.get().get(AccountOTM.class, userOTO.getAccountOTMList().get(0).getAccountId());
              System.out.println("name of the account owner: "+accountOTM.getName());
            }

            oTransaction.ifPresent(Transaction::commit);

        } catch (HibernateException e) {
            oTransaction.ifPresent(Transaction::rollback);
            e.printStackTrace();
        } finally {
            oSession.ifPresent(Session::close);
            oFactory.ifPresent(SessionFactory::close);
        }

    }

}
