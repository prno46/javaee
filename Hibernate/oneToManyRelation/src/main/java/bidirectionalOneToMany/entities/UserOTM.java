package bidirectionalOneToMany.entities;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.List;

/***
 * we are going to have one to one relation between user entity and account entity
 * one user can have only one account (just an example)
 * SOMME COMMON TERMS:
 * Source Entity or owning entity is the one that holds the foreign key to another entity
 * Target Entity or non-owning entity is the one that originally does not have a foreign key field to another entity
 *
 * in our case User is the Target entity and Account is the Source entity
 */

@Entity
@Table(name="otm_user")
@Access(value = AccessType.FIELD)
public class UserOTM {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name = "user_id")
    private Long userId;

    @Column(name = "first_name")
    private String firstName;

    @Column(name = "last_name")
    private String lastName;

    @Column(name = "email_address")
    private String emailAddress;

    /*  the one to many relation is usually a collection
        need to add mapped by to @OneToMany
        the cascade type - all has to be in a target entity for this to work
     */
    @OneToMany(targetEntity = AccountOTM.class, cascade = CascadeType.ALL, mappedBy = "userOTM")
    private List<AccountOTM> accountOTMList = new ArrayList<>();

    public UserOTM() { }

    public Long getUserId() {
        return userId;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getEmailAddress() {
        return emailAddress;
    }

    public void setEmailAddress(String emailAddress) {
        this.emailAddress = emailAddress;
    }

    public List<AccountOTM> getAccountOTMList() {
        return accountOTMList;
    }

    public void setAccountOTMList(List<AccountOTM> accountOTMList) {
        this.accountOTMList = accountOTMList;
    }
}
