package bidirectionalOneToMany.entities;

import javax.persistence.*;
import java.math.BigDecimal;

@Entity
@Table(name = "otm_account_table")
@Access(value = AccessType.FIELD)
public class AccountOTM {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name = "account_id")
    private Long accountId;

    @Column(name = "name")
    private String name;

    @Column(name = "initial_balance")
    private BigDecimal initialBalance;

    @Column(name = "current_balance")
    private BigDecimal currentBalance;

    /*  this is a owning entity and below we have a foreign key to user entity
        one user can have  multiple accounts
     */
    @ManyToOne(targetEntity = UserOTM.class)
    @JoinColumn(name = "user_otm_fk", referencedColumnName = "user_id", nullable = false)
    private UserOTM userOTM;

    public AccountOTM() { }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public BigDecimal getInitialBalance() {
        return initialBalance;
    }

    public void setInitialBalance(BigDecimal initialBalance) {
        this.initialBalance = initialBalance;
    }

    public BigDecimal getCurrentBalance() {
        return currentBalance;
    }

    public void setCurrentBalance(BigDecimal currentBalance) {
        this.currentBalance = currentBalance;
    }

    public Long getAccountId() {
        return accountId;
    }

    public UserOTM getUserOTM() {
        return userOTM;
    }

    public void setUserOTM(UserOTM userOTM) {
        this.userOTM = userOTM;
    }
}
