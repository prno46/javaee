package data;

import javax.persistence.*;
import java.util.HashMap;
import java.util.Map;

@Entity
@Table(name = "bank_table")
@Access(value = AccessType.FIELD)
class Bank {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name = "bank_id")
    private Long bankId;

    @Column(name = "bank_name")
    private String name;

    @Column(name = "bank_address")
    private String addressLine1;

    @Column(name = "city")
    private String city;

    @Column(name = "state")
    private String state;

    @Column(name = "zip_code")
    private String zipCode;

    // example of map  with basic types
    @ElementCollection(targetClass = String.class) // target class is the value part of the map
    @CollectionTable(name = "client_phones", joinColumns = @JoinColumn(name = "bank_fk", referencedColumnName = "bank_id"))
    @MapKeyColumn(name = "client_name")
    private Map<String, String> clientPhones;

    // example of the map with key as basic type and value as composite type
    @Embedded
    @ElementCollection(targetClass = PhoneNumber.class)
    @CollectionTable(name = "clients_with_phones", joinColumns = @JoinColumn(name = "bank_cfk", referencedColumnName = "bank_id"))
    @MapKeyClass(value = String.class)
    private Map<String, PhoneNumber> clientWithPhone;

    //example of the map with key and value as composite types
    @Embedded
    @CollectionTable(name = "phones_and_clients", joinColumns = @JoinColumn(name = "bank_cpfk", referencedColumnName = "bank_id"))
    @ElementCollection(targetClass = PhoneNumber.class)
    @MapKeyClass(value = Client.class)
    private Map<Client, PhoneNumber> clientsAndPhones;

    public Bank() {
        clientPhones = new HashMap<>(5);
        clientWithPhone = new HashMap<>(5);
        clientsAndPhones = new HashMap<>(5);
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getAddressLine1() {
        return addressLine1;
    }

    public void setAddressLine1(String addressLine1) {
        this.addressLine1 = addressLine1;
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public String getState() {
        return state;
    }

    public void setState(String state) {
        this.state = state;
    }

    public Long getBankId() {
        return bankId;
    }

    public String getZipCode() {
        return zipCode;
    }

    public void setZipCode(String zipCode) {
        this.zipCode = zipCode;
    }

    public Map<String, String> getClientPhones() {
        return clientPhones;
    }

    public void setClientPhones(Map<String, String> clientPhones) {
        this.clientPhones = clientPhones;
    }

    public void setClientWithPhone(Map<String, PhoneNumber> clientWithPhone) {
        this.clientWithPhone = clientWithPhone;
    }

    public void setClientsAndPhones(Map<Client, PhoneNumber> clientsAndPhones) {
        this.clientsAndPhones = clientsAndPhones;
    }
}
