package data;

import org.hibernate.HibernateException;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;

import java.util.HashMap;
import java.util.Map;
import java.util.Optional;

class Main {
    public static void main(String[] args) {
        Optional<SessionFactory> oFactory = Optional.of(HibernateUtil.getSessionFactory());
        Optional<Session> oSession = oFactory.map(SessionFactory::openSession);
        Optional<Transaction> oTransaction = oSession.map(Session::beginTransaction);
        try {

            Map<Client, PhoneNumber> clients = new HashMap<>(5);
            clients.put(new Client("Bruce", "Wayne", "b.wayne@gmail.com"), new PhoneNumber("587-147-369"));
            clients.put(new Client("Tony", "Stark", "t.stark@gmail.com"), new PhoneNumber("874-258-968"));
            clients.put(new Client("John", "Wick", "j.wick@gmail.com"), new PhoneNumber("236-458-856"));
            clients.put(new Client("Adam", "Walk", "a.walk@gmail.com"), new PhoneNumber("349-759-482"));

            Map<String, PhoneNumber> clients2 = new HashMap<>(5);
            clients2.put("Bruce Wayne", new PhoneNumber("587-147-369"));
            clients2.put("Tony Stark", new PhoneNumber("874-258-968"));
            clients2.put("Adam Walk", new PhoneNumber("349-759-482"));

            Bank bank5 = new Bank();
            bank5.setName("mBank SA");
            bank5.setAddressLine1("Grunwaldzka 487");
            bank5.setCity("Gdansk");
            bank5.setState("pomorskie");
            bank5.setZipCode("80-841");
            bank5.setClientWithPhone(clients2);

            oSession.ifPresent(s-> s.save(bank5));

            oTransaction.ifPresent(Transaction::commit);
        } catch (HibernateException e) {
            oTransaction.ifPresent(Transaction::rollback);
            e.getMessage();
        } finally {
            oSession.ifPresent(Session::close);
            oFactory.ifPresent(SessionFactory::close);
        }
    }
}
