package data;

import javax.persistence.Column;
import javax.persistence.Embeddable;

@Embeddable
class Client {

    @Column(name = "client_name")
    private String name;

    @Column(name = "client_last_nam")
    private String lastName;

    @Column(name = "client_email")
    private String mail;

    public Client() { }

    public Client(String name, String lastName, String mail) {
        this.name = name;
        this.lastName = lastName;
        this.mail = mail;
    }


}
