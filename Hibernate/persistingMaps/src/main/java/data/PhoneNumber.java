package data;

import javax.persistence.Column;
import javax.persistence.Embeddable;

@Embeddable
class PhoneNumber {

    @Column(name = "phone_number")
    private String number;

    public PhoneNumber() { }

    public PhoneNumber(String number) {
        this.number = number;
    }

}
