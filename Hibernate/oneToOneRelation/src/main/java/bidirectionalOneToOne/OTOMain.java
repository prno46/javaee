package bidirectionalOneToOne;

import bidirectionalOneToOne.entities.AccountOTO;
import bidirectionalOneToOne.entities.UserOTO;
import org.hibernate.HibernateException;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;

import java.math.BigDecimal;
import java.util.Optional;

class OTOMain {

    public static void main(String[] args) {

        Optional<SessionFactory> oFactory = Optional.of(HibernateUtil.getSessionFactory());
        Optional<Session> oSession = oFactory.map(SessionFactory::openSession);
        Optional<Transaction> oTransaction = oSession.map(Session::beginTransaction);
        try {

            AccountOTO accountOTO = new AccountOTO();
            UserOTO userOTO = new UserOTO();

            // setting user
            userOTO.setAccountOTO(accountOTO);
            userOTO.setFirstName("John");
            userOTO.setLastName("Wick");
            userOTO.setEmailAddress("j.wick@gmail.com");

            // setting account -> we need to set user for account and account for user so this bidirectional relation could work
            accountOTO.setUserOTO(userOTO);
            accountOTO.setName(userOTO.getFirstName());
            accountOTO.setInitialBalance(new BigDecimal(2500));
            accountOTO.setCurrentBalance(new BigDecimal(8450));

            oSession.ifPresent(s-> s.save(accountOTO));

            if(oSession.isPresent()) {
                UserOTO userOTO1 = oSession.get().get(UserOTO.class, accountOTO.getUserOTO().getUserId());
                System.out.println("the user name is: " + userOTO1.getFirstName());
            }

            oTransaction.ifPresent(Transaction::commit);

        } catch (HibernateException e) {
            oTransaction.ifPresent(Transaction::rollback);
            e.getMessage();
        } finally {
            oSession.ifPresent(Session::close);
            oFactory.ifPresent(SessionFactory::close);
        }

    }

}
