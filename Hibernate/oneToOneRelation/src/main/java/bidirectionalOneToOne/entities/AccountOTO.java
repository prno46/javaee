package bidirectionalOneToOne.entities;

import javax.persistence.*;
import java.math.BigDecimal;

@Entity
@Table(name = "oto_account_table")
@Access(value = AccessType.FIELD)
public class AccountOTO {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name = "account_id")
    private Long accountId;

    @Column(name = "name")
    private String name;

    @Column(name = "initial_balance")
    private BigDecimal initialBalance;

    @Column(name = "current_balance")
    private BigDecimal currentBalance;

    /* the reference column name is the name of the column from target entity which contains
       the primary key
     */
    @OneToOne(targetEntity = UserOTO.class, cascade = CascadeType.ALL)
    @JoinColumn(name = "oto_user_fk" ,referencedColumnName = "user_id")
    private UserOTO userOTO;

    public AccountOTO() { }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public BigDecimal getInitialBalance() {
        return initialBalance;
    }

    public void setInitialBalance(BigDecimal initialBalance) {
        this.initialBalance = initialBalance;
    }

    public BigDecimal getCurrentBalance() {
        return currentBalance;
    }

    public void setCurrentBalance(BigDecimal currentBalance) {
        this.currentBalance = currentBalance;
    }

    public UserOTO getUserOTO() {
        return userOTO;
    }

    public void setUserOTO(UserOTO userOTO) {
        this.userOTO = userOTO;
    }

    public Long getAccountId() {
        return accountId;
    }
}
