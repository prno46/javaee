package bidirectionalOneToOne.entities;

import javax.persistence.*;

/***
 * we are going to have one to one relation between user entity and account entity
 * one user can have only one account (just an example)
 * SOMME COMMON TERMS:
 * Source Entity or owning entity is the one that holds the foreign key to another entity
 * Target Entity or non-owning entity is the one that originally does not have a foreign key field to another entity
 *
 * in our case User is the Target entity and Account is the Source entity
 */

@Entity
@Table(name="oto_user")
@Access(value = AccessType.FIELD)
public class UserOTO {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name = "user_id")
    private Long userId;

    @Column(name = "first_name")
    private String firstName;

    @Column(name = "last_name")
    private String lastName;

    @Column(name = "email_address")
    private String emailAddress;

    /* bidirectional relation to AccountOTO entity
    *  the mappedBy refers to a field name (not a column) of type UserOTO entity
    *  in source entity
    */
    @OneToOne(mappedBy = "userOTO")
    private AccountOTO accountOTO;

    public UserOTO() { }

    public Long getUserId() {
        return userId;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getEmailAddress() {
        return emailAddress;
    }

    public void setEmailAddress(String emailAddress) {
        this.emailAddress = emailAddress;
    }

    public AccountOTO getAccountOTO() {
        return accountOTO;
    }

    public void setAccountOTO(AccountOTO accountOTO) {
        this.accountOTO = accountOTO;
    }
}
