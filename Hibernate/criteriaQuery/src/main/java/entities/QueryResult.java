package entities;

import javax.persistence.Entity;
import javax.persistence.Id;

@Entity
class QueryResult {

    //t.title, t.transactionType, a.accountType

    @Id
    private long id;

    private String title;
    private String transactionType;
    private AccountType accountType;

    public QueryResult() {
    }

    public String getTitle() {
        return title;
    }

    public String getTransactionType() {
        return transactionType;
    }

    public AccountType getAccountType() {
        return accountType;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public void setTransactionType(String transactionType) {
        this.transactionType = transactionType;
    }

    public void setAccountType(AccountType accountType) {
        this.accountType = accountType;
    }
}
