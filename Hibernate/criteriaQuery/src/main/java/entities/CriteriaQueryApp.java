package entities;

import org.hibernate.HibernateException;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.hibernate.criterion.Criterion;
import org.hibernate.criterion.Restrictions;
import org.hibernate.query.Query;
import org.hibernate.sql.JoinType;

import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import java.util.List;
import java.util.Optional;

class CriteriaQueryApp {


    public static void main(String[] args) {

        Optional<SessionFactory> oFactory = Optional.of(HibernateUtil.getSessionFactory());
        Optional<Session> oSession = oFactory.map(SessionFactory::openSession);
        Optional<Transaction> oTransaction = oSession.map(Session::beginTransaction);

        try {


            TransactionDao transactionDao = new TransactionDaoImpl();
            oSession.ifPresent(transactionDao::setSession);

            // Queries for tests
            String getAllTransactions = "select t from Transaction t";
            String getOnlySomeFields = "select t.transactionId from Transaction t";

            /*
            List<entities.Transaction> transactions = transactionDao.createHqlQuery(getAllTransactions);

            for (entities.Transaction t: transactions) {
                System.out.println(t.getTransactionId());
            }*/

            /*Query<Object[]> query = oSession.get().createQuery(
                    "select t.title, t.transactionType, a.accountType from Transaction t "
                            + "inner join t.account a"
            , Object[].class);

            List<Object[]> objects =  query.getResultList();
            Object[] objs = objects.get(0);
            System.out.println(objs[0]);
            System.out.println(objs[1]);
            System.out.println(objs[2]);*/

            // criteria query example -> it loads all from transaction table
            // the Hibernate criteria is deprecated  so should use JPA instead
            @SuppressWarnings({"unchecked", "deprecation"})
            List<entities.Transaction> transactions = oSession.get().createCriteria(entities.Transaction.class).list();
            for (entities.Transaction t: transactions) {
                System.out.println("value of t: " + t.getTitle());
            }

            // crating a criterion for our queries + adding it to query it is like "where" clause in sql
            Criterion criterion = Restrictions.ilike("transactionType", "Deposit");
            @SuppressWarnings({"unchecked", "deprecation"})
            List<entities.Transaction> transactions1 = oSession.get().createCriteria(entities.Transaction.class)
                    .add(criterion)
                    .list();
            for (entities.Transaction t: transactions1) {
                System.out.println("value of t1: " + t.getTitle());
            }

            // creating join with criteria query same as with hql
            @SuppressWarnings({"unchecked", "deprecation"})
            List<entities.Transaction> transactions2 = oSession.get().createCriteria(entities.Transaction.class)
                    .createCriteria("account", JoinType.INNER_JOIN)
                    .list();
            for (entities.Transaction t: transactions2) {
                System.out.println("value of t2: " + t.getTitle() + " " + t.getTransactionType() +" " + t.getAccount().getAccountType());
            }

            // same as above but different way
            @SuppressWarnings({"unchecked", "deprecation"})
            /*List<entities.Transaction> transactions3  = oSession.get().createCriteria(entities.Transaction.class, "t")
                    .createAlias("t.account", "a")
                    .add(Restrictions.eq("a.accountType", "checking"))
                    .setResultTransformer(CriteriaSpecification.ROOT_ENTITY)
                    .list();
            for (entities.Transaction t: transactions3) {
                System.out.println("value of t3: " + t.getTitle() + " " + t.getTransactionType() +" " + t.getAccount().getAccountType());
            }*/ // todo problem with enum -> look at this later

            // Using criteria builder from hibernate to build JPA CriteriaQuery
            CriteriaBuilder builder = oSession.get().getCriteriaBuilder();
            CriteriaQuery<entities.Transaction> query = builder.createQuery(entities.Transaction.class);
           // Root<Account> root = query.from(Account.class);


            query.select(query.from(Account.class)
                    .join("transactions", javax.persistence.criteria.JoinType.INNER)
            );
            Query<entities.Transaction> qr = oSession.get().createQuery(query);
            List<entities.Transaction > tr = qr.list();
            for (entities.Transaction t: tr) {
                System.out.println("value of tr: " + t.getTitle() + " " + t.getTransactionType() +" " + t.getAccount().getAccountType());
            }


        } catch (HibernateException hi) {
            oTransaction.ifPresent(Transaction::rollback);
            hi.printStackTrace();
        } finally {
            oSession.ifPresent(Session::close);
            oFactory.ifPresent(SessionFactory::close);
        }
    }
}
