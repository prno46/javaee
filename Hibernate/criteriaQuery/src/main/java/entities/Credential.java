package entities;

import globalVariables.SerializationNumber;

import javax.persistence.*;
import java.io.Serializable;

@Entity
@Table(name = "credentials")
@Access(value = AccessType.FIELD)
class Credential implements Serializable {

    @Transient
    private static final long serialVersionUID = SerializationNumber.SERIAL_NUMBER;

    @Id
    @GeneratedValue(strategy= GenerationType.AUTO)
    @Column(name="CREDENTIAL_ID")
    public Long credentialId;

    @Column(name="USERNAME")
    private String username;

    @Column(name="PASSWORD")
    private String password;

    public Credential() { }

}
