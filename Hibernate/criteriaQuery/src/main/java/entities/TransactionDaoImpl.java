package entities;

import persistence.AbstractDao;

import java.util.List;

class TransactionDaoImpl extends AbstractDao<Transaction, Long> implements TransactionDao {

    @Override
    public List<Transaction> createHqlQuery(String query) {
        return getSession().createQuery(query, Transaction.class).list();
    }


}
