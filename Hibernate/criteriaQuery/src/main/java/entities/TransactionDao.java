package entities;

import persistence.Dao;

import java.util.List;


public interface TransactionDao extends Dao<Transaction, Long> {

    // put specific methods for this transaction dao if needed

    List<Transaction> createHqlQuery(String query);

}
