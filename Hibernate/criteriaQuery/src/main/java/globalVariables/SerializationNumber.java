package globalVariables;

import java.util.UUID;

public class SerializationNumber {


    private  static UUID uuid = UUID.fromString("a0a70e9e-bd72-11e8-a355-529269fb1459");

    public static final long SERIAL_NUMBER = uuid.timestamp();

}
