package temp;

class SomeHash {

    private String name;

    private int number;

    public SomeHash() {
    }

    public SomeHash(String name, int number) {
        this.name = name;
        this.number = number;
    }

    @Override
    public int hashCode() {
        int result = Integer.hashCode(number);
        result = 31 * result + name.hashCode();
        return result;
    }
}
