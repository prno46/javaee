package entities;

import org.hibernate.annotations.Cascade;

import javax.persistence.*;
import java.io.Serializable;


@Entity
@Table(name = "my_order")
@Access(AccessType.FIELD)
public class Order implements Serializable {

    private static final long serialVersionUID = 46L;

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "order_id_sq")
    @SequenceGenerator(name = "order_id_sq", sequenceName = "order_id_sq", initialValue = 1, allocationSize = 1)
    @Column(name = "order_id")
    private long orderId;

    @Column(name = "order_code", nullable = false, unique = true)
    private String orderCode;

    @OneToOne(targetEntity = PtoData.class, fetch = FetchType.LAZY)
    @Cascade({org.hibernate.annotations.CascadeType.SAVE_UPDATE})
    @JoinColumn(name = "pto_id",referencedColumnName = "pto_id", nullable = false /*foreignKey = @ForeignKey(name = "fk_pto_id")*/)
    private PtoData ptoData;

    public Order() { }

    public String getOrderCode() {
        return orderCode;
    }

    public PtoData getPtoData() {
        return ptoData;
    }

    public long getOrderId() {
        return orderId;
    }

    public void setOrderCode(String orderCode) {
        this.orderCode = orderCode;
    }

    public void setPtoData(PtoData ptoData) {
        this.ptoData = ptoData;
    }
}
