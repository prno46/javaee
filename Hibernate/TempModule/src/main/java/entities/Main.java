package entities;

import org.hibernate.HibernateException;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.hibernate.query.Query;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.Optional;

class Main {
    static Logger logger = LoggerFactory.getLogger(Main.class);

    public static void main(String[] args) {

        Optional<SessionFactory> oFactory = Optional.of(HibernateUtil.getSessionFactory());
        Optional<Session> oSession = oFactory.map(SessionFactory::openSession);
        Optional<Transaction> oTransaction = oSession.map(Session::beginTransaction);

        try {
            // setting pto
            PtoData pto = new PtoData();
            pto.setPtoRatio(1.22);
            pto.setPtoType("ENGINE");

            // setting a order
           /* Order order1 = new Order();
            order1.setOrderCode("FF9954");
            order1.setPtoData(pto);*/

            // searching required value in database
            Query query = oSession.get().createQuery("select ptoId from PtoData p where p.ptoRatio=1.22 and p.ptoType='ENGINE' ");
            long id =  (long) query.getResultList().get(0);
            logger.info("The id of loaded pto id: {}", id);

            // loading search value with given id
            PtoData ptoData = oSession.get().load(PtoData.class, id);

            // saving new order in database
            Order order2 = new Order();
            order2.setOrderCode("FT2358");
            order2.setPtoData(ptoData);


            /*Order order5 = new Order();
            order5.setOrderCode("FXX2185");
            order5.setPtoData(pto);*/

            oSession.ifPresent(s-> s.saveOrUpdate(order2));


            oTransaction.ifPresent(Transaction::commit);
        } catch (HibernateException e) {
            oTransaction.ifPresent(Transaction::rollback);
            e.getMessage();
        } finally {
            oSession.ifPresent(Session::close);
            oFactory.ifPresent(SessionFactory::close);
        }
    }

    // todo -> add constraints UNIQUE to pto table and experiment
}
