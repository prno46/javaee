package entities;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Objects;

@Entity
@Table(name = "pto", uniqueConstraints = {
        @UniqueConstraint(columnNames = {"pto_ratio", "pto_type"}, name = "ukn_pto_rt")
})
@Access(AccessType.FIELD)
public class PtoData implements Serializable {

    private static final long serialVersionUID = 46L;

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "pto_id_sq")
    @SequenceGenerator(name = "pto_id_sq" ,sequenceName = "pto_id_sq", initialValue = 1, allocationSize = 1)
    @Column(name = "pto_id")
    private long ptoId;

    @Column(name = "pto_ratio", nullable = false, unique = true)
    private double ptoRatio;

    @Column(name = "pto_type", nullable = false, unique = true)
    private String ptoType;

    public PtoData() { }

    public long getPtoId() {
        return ptoId;
    }

    public double getPtoRatio() {
        return ptoRatio;
    }

    public String getPtoType() {
        return ptoType;
    }

    public void setPtoRatio(double ptoRatio) {
        this.ptoRatio = ptoRatio;
    }

    public void setPtoType(String ptoType) {
        this.ptoType = ptoType;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        PtoData ptoData = (PtoData) o;
        return Double.compare(ptoData.ptoRatio, ptoRatio) == 0 &&
                Objects.equals(ptoType, ptoData.ptoType);
    }

    @Override
    public int hashCode() {
        return Objects.hash(ptoRatio, ptoType);
    }
}
