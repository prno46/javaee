package entities;

import org.hibernate.HibernateException;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;

import java.math.BigDecimal;
import java.util.Optional;

class MainApp {

    public static void main(String[] args) {
        Optional<SessionFactory> oFactory = Optional.of(HibernateUtil.getSessionFactory());
        Optional<Session> oSession = oFactory.map(SessionFactory::openSession);
        Optional<Transaction> oTransaction = oSession.map(Session::beginTransaction);
        try {

            Portfolio portfolio = new Portfolio();
            portfolio.setName("Good investments");

            Bound bound = new Bound();
            bound.setName("Profitable bound");
            bound.setIssuer("Market guru");
            bound.setValue(new BigDecimal(5487));
            bound.setInterestRate(new BigDecimal(4));
            bound.setPortfolio(portfolio);

            portfolio.getInvestments().add(bound);


            Portfolio portfolio2 = new Portfolio();
            portfolio2.setName("Good Stocks");

            Stock stock = new Stock();
            stock.setName("Profitable stock");
            stock.setIssuer("Market guru");
            stock.setQuantity(new BigDecimal(58));
            stock.setSharePrice(new BigDecimal(698));
            stock.setPortfolio(portfolio2);

            portfolio2.getInvestments().add(stock);


            oSession.ifPresent(s-> s.persist(bound));
            oSession.ifPresent(s-> s.persist(stock));

            oTransaction.ifPresent(Transaction::commit);
        } catch (HibernateException e) {
            oTransaction.ifPresent(Transaction::rollback);
            e.getMessage();
        } finally {
            oSession.ifPresent(Session::close);
            oFactory.ifPresent(SessionFactory::close);
        }
    }
}
