package entities;

import javax.persistence.*;

/**
 *  to use a inheritance with strategy .SINGLE_TABLE we need add additional annotation
 *  @ DiscriminatorColumn and we need also put @Table annotation on our superclass.
 *  we also specify the Id generation strategy - we can use auto in this case.
 *
 *  we set all relations between subclasses and other entities in our super class.
 *
 *  we need to remove the @Table annotation from subclasses
 *
 *  SINGLE TABLE strategy : in database we put all the fields from subclasses to one table.
 *  we need to accept that the fields that are not common will be null wen we persist only one subclass.
 *
 */

@Entity
@Inheritance(strategy = InheritanceType.SINGLE_TABLE)
@DiscriminatorColumn(name = "investment_type", discriminatorType = DiscriminatorType.STRING) // this will distinguish which of the subclass we have persisted
@Table(name = "INVESTMENTS")
class Investments {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name="INVESTMENT_ID")
    private Long investmentId;

    @JoinColumn(name="PORTFOLIO_ID", referencedColumnName = "PORTFOLIO_ID")
    @ManyToOne(targetEntity = Portfolio.class, cascade=CascadeType.ALL)
    private Portfolio portfolio;

    @Column(name = "NAME")
    protected String name;

    @Column(name = "ISSUER")
    protected String issuer;

    public Portfolio getPortfolio() {
        return portfolio;
    }

    public void setPortfolio(Portfolio portfolio) {
        this.portfolio = portfolio;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getIssuer() {
        return issuer;
    }

    public void setIssuer(String issuer) {
        this.issuer = issuer;
    }

    public Long getInvestmentId() {
        return investmentId;
    }
}
