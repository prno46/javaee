package entities;

import javax.persistence.*;

@MappedSuperclass
abstract class Investments {

    /**
     * we simply move the common fields of both classes to a new created abstract class
     * we mark it with @MappedSuperclass mark fields with @Column.
     * Than other entities need to extend this class.
     *
     * with this way we are not able to use inheritance as in Java way.
     * this is only a container for a common values. We cannot have a collection of this class etc.
     */

    @Column(name = "NAME")
    private String name;

    @Column(name = "ISSUER")
    private String issuer;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getIssuer() {
        return issuer;
    }

    public void setIssuer(String issuer) {
        this.issuer = issuer;
    }
}
