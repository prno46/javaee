package entities;

import javax.persistence.*;
import java.math.BigDecimal;

@Entity
@Table(name = "BOUND")
@Access(value = AccessType.FIELD)
class Bound extends Investments {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name = "BOUND_ID")
    private long boundId;

    @Column(name = "VALUE")
    private BigDecimal value;

    @Column(name = "INTEREST_RATE")
    private BigDecimal interestRate;

    public Bound() { }

    public BigDecimal getValue() {
        return value;
    }

    public void setValue(BigDecimal value) {
        this.value = value;
    }

    public BigDecimal getInterestRate() {
        return interestRate;
    }

    public void setInterestRate(BigDecimal interestRate) {
        this.interestRate = interestRate;
    }

    public long getBoundId() {
        return boundId;
    }
}
