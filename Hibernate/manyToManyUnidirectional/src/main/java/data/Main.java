package data;

import org.hibernate.HibernateException;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;

import java.math.BigDecimal;
import java.util.Arrays;
import java.util.List;
import java.util.Optional;

class Main {
    public static void main(String[] args) {
        Optional<SessionFactory> oFactory = Optional.of(HibernateUtil.getSessionFactory());
        Optional<Session> oSession = oFactory.map(SessionFactory::openSession);
        Optional<Transaction> oTransaction = oSession.map(Session::beginTransaction);
        try {
            List<Bank> banks = Arrays.asList(
                    new Bank("Santander", "Blotna", "Gdynia", "87-158"),
                    new Bank("PEKAO", "Samotna", "Gdansk", "43-918"),
                    new Bank("Milenium", "Wesola", "Wroclaw", "45-178"),
                    new Bank("mBank", "Kolorowa", "Zakopane", "12-157")
            );

            TransactionMTM tr1 = new TransactionMTM();
            tr1.setAmount(new BigDecimal(458));
            tr1.setInitialBalance(new BigDecimal(4785));
            tr1.setClosingBalance(new BigDecimal(78965));
            tr1.getBanks().addAll(banks);

            TransactionMTM tr2 = new TransactionMTM();
            tr2.setAmount(new BigDecimal(854));
            tr2.setInitialBalance(new BigDecimal(78425));
            tr2.setClosingBalance(new BigDecimal(7452));
            tr2.getBanks().addAll(banks);

            oSession.ifPresent(s-> s.save(tr1));
            oSession.ifPresent(s-> s.save(tr2));

            oTransaction.ifPresent(Transaction::commit);
        } catch (HibernateException e) {
            oTransaction.ifPresent(Transaction::rollback);
            e.getMessage();
        } finally {
            oSession.ifPresent(Session::close);
            oFactory.ifPresent(SessionFactory::close);
        }
    }
}
