package data;

import javax.persistence.*;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

@Entity
@Table(name = "transaction_table")
@Access(value = AccessType.FIELD)
class TransactionMTM {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name = "transaction_id")
    private Long transactionId;

    @Column(name = "amount")
    private BigDecimal amount;

    @Column(name = "initial_balance")
    private BigDecimal initialBalance;

    @Column(name = "closing_balance")
    private BigDecimal closingBalance;

    // setting many to many relation with Bank entity
    @ManyToMany(targetEntity = Bank.class, cascade = CascadeType.ALL)
    @JoinTable(name = "bank_transactions",
            joinColumns = @JoinColumn(name = "source_entity",referencedColumnName = "transaction_id", nullable = false),
            inverseJoinColumns = @JoinColumn(name = "target_entity", referencedColumnName = "bank_id", nullable = false))
    private List<Bank> banks;

    public TransactionMTM() {
        banks = new ArrayList<>(5);
    }

    public void setAmount(BigDecimal amount) {
        this.amount = amount;
    }

    public void setInitialBalance(BigDecimal initialBalance) {
        this.initialBalance = initialBalance;
    }

    public void setClosingBalance(BigDecimal closingBalance) {
        this.closingBalance = closingBalance;
    }

    public List<Bank> getBanks() {
        return banks;
    }
}
