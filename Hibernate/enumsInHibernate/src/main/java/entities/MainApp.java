package entities;

import org.hibernate.HibernateException;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;

import java.math.BigDecimal;
import java.util.Optional;

class MainApp {

    public static void main(String[] args) {
        Optional<SessionFactory> oFactory = Optional.of(HibernateUtil.getSessionFactory());
        Optional<Session> oSession = oFactory.map(SessionFactory::openSession);
        Optional<Transaction> oTransaction = oSession.map(Session::beginTransaction);
        try {

                Account account = new Account();
                account.setName("Batman's account");
                account.setType(AccountType.SAVINGS);
                account.setInitialBalance(new BigDecimal(58456));
                account.setCurrentBalance(new BigDecimal(1247851));


                oSession.ifPresent(s-> s.persist(account));

            oTransaction.ifPresent(Transaction::commit);
        } catch (HibernateException e) {
            oTransaction.ifPresent(Transaction::rollback);
            e.getMessage();
        } finally {
            oSession.ifPresent(Session::close);
            oFactory.ifPresent(SessionFactory::close);
        }
    }
}
