package data;

import javax.persistence.Column;
import javax.persistence.Embeddable;

@Embeddable
class Client {

    @Column(name = "client_first_name")
    private String name;

    @Column(name = "client_last_name")
    private String lastName;

    @Column(name = "client_account_type")
    private String accountType;

    public Client() { }

    public Client(String name, String lastName, String accountType) {
        this.name = name;
        this.lastName = lastName;
        this.accountType = accountType;
    }

    public String getName() {
        return name;
    }

    public String getLastName() {
        return lastName;
    }

    public String getAccountType() {
        return accountType;
    }
}
