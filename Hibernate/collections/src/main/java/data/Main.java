package data;

import org.hibernate.HibernateException;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;

import java.util.Arrays;
import java.util.List;
import java.util.Optional;

class Main {
    public static void main(String[] args) {
        Optional<SessionFactory> oFactory = Optional.of(HibernateUtil.getSessionFactory());
        Optional<Session> oSession = oFactory.map(SessionFactory::openSession);
        Optional<Transaction> oTransaction = oSession.map(Session::beginTransaction);
        try {
            List<String> phones = Arrays.asList(
                    "587-6524-987",
                    "748-6227-587",
                    "278-9478-937",
                    "147-1298-907"
            );

            List<Client> clients = Arrays.asList(
                    new Client("Bruce", "Wayne", "VIP"),
                    new Client("Bruce", "Banner", "Premium"),
                    new Client("Tony", "Stark", "VIP"),
                    new Client("Some", "Man", "Basic")
            );

            Bank bank2 = new Bank();
            bank2.setName("PEKAO SA");
            bank2.setAddressLine("Garncarska 48");
            bank2.setCity("Gdansk");
            bank2.setState("pomorskie");
            bank2.setZipCode("80-845");
            //bank.setClientPhones(phones);
            bank2.setClients(clients);

            oSession.ifPresent(s-> s.save(bank2));

            oTransaction.ifPresent(Transaction::commit);
        } catch (HibernateException e) {
            oTransaction.ifPresent(Transaction::rollback);
            e.getMessage();
        } finally {
            oSession.ifPresent(Session::close);
            oFactory.ifPresent(SessionFactory::close);
        }
    }
}
