package data;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.List;

@Entity
@Table(name = "bank_collections")
@Access(value = AccessType.FIELD)
class Bank {

    @Id
    @GeneratedValue(strategy =  GenerationType.AUTO)
    @Column(name = "bank_id")
    private Long bankId;

    @Column(name = "bank_name")
    private String name;

    @Column(name = "bank_address")
    private String addressLine;

    @Column(name = "bank_address_city")
    private String city;

    @Column(name = "bank_address_state")
    private String state;

    @Column(name = "bank_address_zipCode")
    private String zipCode;

    // collection of primitive types
    @ElementCollection(targetClass = String.class)
    @CollectionTable(name = "client_phones", joinColumns = @JoinColumn(name = "bank_fk", referencedColumnName = "bank_id"))
    @Column(name = "phones")
    private List<String> clientPhones;

    // collection of object / composite types
    @Embedded
    @ElementCollection(targetClass = Client.class)
    @CollectionTable(name = "clients_of_bank", joinColumns = @JoinColumn(name = "bank_cfk", referencedColumnName = "bank_id"))
    @Column(name = "clients")
    private List<Client> clients;

    public Bank() {
        clientPhones = new ArrayList<>(5);
        clients = new ArrayList<>(5);
    }

    public Long getBankId() {
        return bankId;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getAddressLine() {
        return addressLine;
    }

    public void setAddressLine(String addressLine) {
        this.addressLine = addressLine;
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public String getState() {
        return state;
    }

    public void setState(String state) {
        this.state = state;
    }

    public String getZipCode() {
        return zipCode;
    }

    public void setZipCode(String zipCode) {
        this.zipCode = zipCode;
    }

    public List<String> getClientPhones() {
        return clientPhones;
    }

    public void setClientPhones(List<String> clientPhones) {
        this.clientPhones = clientPhones;
    }

    public List<Client> getClients() {
        return clients;
    }

    public void setClients(List<Client> clients) {
        this.clients = clients;
    }
}
