package data;

import org.hibernate.HibernateException;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.EntityTransaction;
import javax.persistence.Persistence;
import java.util.Optional;

class Main {
    public static void main(String[] args) {
        Optional<EntityManagerFactory> factory = Optional.of(Persistence.createEntityManagerFactory("persistence"));
        Optional<EntityManager> manager = factory.map(EntityManagerFactory::createEntityManager);
        Optional<EntityTransaction> transaction = manager.map(EntityManager::getTransaction);

        try {
            transaction.ifPresent(EntityTransaction::begin);
            JPABank bank = createNewBank();

            // saving entity in database
            //manager.ifPresent(p -> p.persist(bank));

            // getting the entity from the database
            if(manager.isPresent()) {
                JPABank bank1 = manager.get().find(JPABank.class,  1L);
                if(manager.get().contains(bank1)){
                    System.out.println(bank1.getName());

                    // updating the entity -> after committing this transaction the update in database will be made automatically
                    // we do not need to do anything as along as we do this within one persistence context (session)
                    bank1.setName("Alioir Bank");
                }
            }

            // removing entity from database
            if(manager.isPresent()) {
                JPABank bank2 = manager.get().find(JPABank.class, 2L);
                if(manager.get().contains(bank2)) {
                    manager.ifPresent(m-> m.remove(bank2));
                }
            }


            transaction.ifPresent(EntityTransaction::commit);
        } catch (HibernateException e) {
            /*oTransaction.ifPresent(Transaction::rollback);*/
            transaction.ifPresent(EntityTransaction::rollback);
            e.getMessage();
        } finally {
            manager.ifPresent(EntityManager::close);
            factory.ifPresent(EntityManagerFactory::close);
        }
    }

    private static JPABank createNewBank() {
        JPABank bank = new JPABank();
        bank.setName("Millenium Bank");
        bank.setAddressLine("Targowa 254");
        bank.setCity("Gdynia");
        bank.setZipCode("87-215");
        return bank;
    }
}
