package data;

import javax.persistence.*;
import java.math.BigDecimal;

@Entity
@Table(name = "transaction_table")
@Access(value = AccessType.FIELD)
class JPATransaction {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name = "transaction_id")
    private Long transactionId;

    @Column(name = "amount")
    private BigDecimal amount;

    @Column(name = "initial_balance")
    private BigDecimal initialBalance;

    @Column(name = "closing_balance")
    private BigDecimal closingBalance;

    // foreign key in Transaction table

}
