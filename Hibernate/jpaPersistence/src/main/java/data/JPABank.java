package data;

import javax.persistence.*;

@Entity
@Table(name = "bank_table")
@Access(value = AccessType.FIELD)
class JPABank {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name = "bank_id")
    private Long bankId;

    @Column(name = "name")
    private String name;

    @Column(name = "street")
    private String addressLine;

    @Column(name = "city")
    private String city;

    @Column(name = "zip_code")
    private String zipCode;

    public void setName(String name) {
        this.name = name;
    }

    public void setAddressLine(String addressLine) {
        this.addressLine = addressLine;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public void setZipCode(String zipCode) {
        this.zipCode = zipCode;
    }

    public String getName() {
        return name;
    }

    public String getAddressLine() {
        return addressLine;
    }

    public String getCity() {
        return city;
    }

    public String getZipCode() {
        return zipCode;
    }
}
