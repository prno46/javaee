package data;

import org.hibernate.HibernateException;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;

import java.math.BigDecimal;
import java.util.Arrays;
import java.util.List;
import java.util.Optional;

class Main {
    public static void main(String[] args) {
        Optional<SessionFactory> oFactory = Optional.of(HibernateUtil.getSessionFactory());
        Optional<Session> oSession = oFactory.map(SessionFactory::openSession);
        Optional<Transaction> oTransaction = oSession.map(Session::beginTransaction);
        try {

            List<TransactionOTM> transactions = Arrays.asList(
                    new TransactionOTM(new BigDecimal(458), new BigDecimal(47854), new BigDecimal(3698)),
                    new TransactionOTM(new BigDecimal(987), new BigDecimal(54736), new BigDecimal(74185)),
                    new TransactionOTM(new BigDecimal(254), new BigDecimal(87435), new BigDecimal(3698745))
            );

            Bank bank = new Bank();
            bank.setName("PEKAO SA");
            bank.setAddressLine("Blotna 5");
            bank.setCity("Gdynia");
            bank.setZipCode("78-147");

            bank.getTrasactions().addAll(transactions);

            /*
                we have to persist bank entity instead of transaction in this case
            */
            oSession.ifPresent(s-> s.save(bank));

            oTransaction.ifPresent(Transaction::commit);
        } catch (HibernateException e) {
            oTransaction.ifPresent(Transaction::rollback);
            e.getMessage();
        } finally {
            oSession.ifPresent(Session::close);
            oFactory.ifPresent(SessionFactory::close);
        }
    }
}
