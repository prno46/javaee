package data;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.List;

@Entity
@Table(name = "bank_table")
@Access(value = AccessType.FIELD)
class Bank {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name = "bank_id")
    private Long bankId;

    @Column(name = "name")
    private String name;

    @Column(name = "street")
    private String addressLine;

    @Column(name = "city")
    private String city;

    @Column(name = "zip_code")
    private String zipCode;

    /* when we have one to many unidirectional the reference column name refers to target entity not source
     */
    @OneToMany(targetEntity = TransactionOTM.class, cascade = CascadeType.ALL)
    @JoinColumn(name = "bank_fk", referencedColumnName = "bank_id", nullable = false)
    private List<TransactionOTM> trasactions;

    public Bank() {
        trasactions = new ArrayList<>(5);
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setAddressLine(String addressLine) {
        this.addressLine = addressLine;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public void setZipCode(String zipCode) {
        this.zipCode = zipCode;
    }

    public Long getBankId() {
        return bankId;
    }

    public void setTrasactions(List<TransactionOTM> trasactions) {
        this.trasactions = trasactions;
    }

    public List<TransactionOTM> getTrasactions() {
        return trasactions;
    }
}
