package data;

import org.hibernate.SessionFactory;
import org.hibernate.boot.Metadata;
import org.hibernate.boot.MetadataSources;
import org.hibernate.boot.registry.StandardServiceRegistryBuilder;
import org.hibernate.service.ServiceRegistry;

class HibernateUtil {
    private static final SessionFactory sessionFactory = buildSessionFactory();

    private static SessionFactory buildSessionFactory() {
        try{
            ServiceRegistry serviceRegistry = new StandardServiceRegistryBuilder()
                    .configure("hibernateOTM.cfg.xml").build();
            Metadata metadata = new MetadataSources(serviceRegistry).getMetadataBuilder().build();
            return metadata.getSessionFactoryBuilder().build();
        }catch (Exception ex) {
            ex.printStackTrace();
            throw new RuntimeException("Error during building a session factory for Hibernate");
        }
    }

    static SessionFactory getSessionFactory() {
        return sessionFactory;
    }
}
