package data;

import javax.persistence.*;
import java.math.BigDecimal;

/*
    when we have one to many relation there is no foreign key in source entity
    all mapping  is done in target entity -> Bank in this case
 */

@Entity
@Table(name = "transaction_table")
@Access(value = AccessType.FIELD)
class TransactionOTM {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name = "transaction_id")
    private Long transactionId;

    @Column(name = "amount")
    private BigDecimal amount;

    @Column(name = "initial_balance")
    private BigDecimal initialBalance;

    @Column(name = "closing_balance")
    private BigDecimal closingBalance;

    public TransactionOTM() { }

    public TransactionOTM(BigDecimal amount, BigDecimal initialBalance, BigDecimal closingBalance) {
        this.amount = amount;
        this.initialBalance = initialBalance;
        this.closingBalance = closingBalance;
    }

    public void setAmount(BigDecimal amount) {
        this.amount = amount;
    }

    public void setInitialBalance(BigDecimal initialBalance) {
        this.initialBalance = initialBalance;
    }

    public void setClosingBalance(BigDecimal closingBalance) {
        this.closingBalance = closingBalance;
    }
}
