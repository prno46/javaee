package entities;

import javax.persistence.*;
import java.math.BigDecimal;

@Entity
@Table(name = "BOUND")
@Access(value = AccessType.FIELD)
class Bound extends Investments {

    @Column(name = "INVESTMENT_ID")
    private Long investmentId;


    @Column(name = "VALUE")
    private BigDecimal value;

    @Column(name = "INTEREST_RATE")
    private BigDecimal interestRate;

    public Bound() { }

    public BigDecimal getValue() {
        return value;
    }

    public void setValue(BigDecimal value) {
        this.value = value;
    }

    public BigDecimal getInterestRate() {
        return interestRate;
    }

    public void setInterestRate(BigDecimal interestRate) {
        this.interestRate = interestRate;
    }

    public Long getInvestmentId() {
        return investmentId;
    }

}
