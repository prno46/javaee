package entities;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.List;

@Entity
@Table(name = "PORTFOLIO")
@Access(value = AccessType.FIELD)
class Portfolio {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name="PORTFOLIO_ID")
    private Long portfolioId;

    @Column(name="NAME")
    private String name;

    @OneToMany(targetEntity = Investments.class, mappedBy = "portfolio") // mappedBy = name of the field from Investment.class
    private List<Investments> investments;

    public Portfolio() {
        investments = new ArrayList<>(10);
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public List<Investments> getInvestments() {
        return investments;
    }

    public void setInvestments(List<Investments> investments) {
        this.investments = investments;
    }

    public Long getPortfolioId() {
        return portfolioId;
    }
}
