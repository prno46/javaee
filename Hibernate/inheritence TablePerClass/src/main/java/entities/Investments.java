package entities;

import javax.persistence.*;

/**
 * to implement inheritance between entities we need to set our super class first.
 * we mark our super class with annotation @Entity, @Inheritance
 * we move the Id generation to super class , cause we choose strategy = InheritanceType.TABLE_PER_CLASS
 * we need to set a id generator if we have chosen the strategy = GenerationType.TABLE
 *
 * then we need to set the relations between entities, all mapping (stock -> portfolio) goes through super class
 * we need to remove the @Id annotation from subclasses.
 *
 * this is the working example of inheritance in Hibernate
 */
@Entity
@Inheritance(strategy = InheritanceType.TABLE_PER_CLASS)
abstract class Investments {

    @Id
    /*@GeneratedValue(strategy = GenerationType.TABLE, generator = "key_generator") // the same name have to be used in TableGenerator.name property
    @TableGenerator(name = "key_generator", table = "Invest_keys", pkColumnName = "pk_name", valueColumnName = "pk_value")*/
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "key_generator")
    @SequenceGenerator(name = "key_generator", sequenceName = "invest_seq", initialValue = 10, allocationSize = 1)
    @Column(name="INVESTMENT_ID")
    private Long investmentId;

    @JoinColumn(name="FK_PORTFOLIO_ID", referencedColumnName = "PORTFOLIO_ID")
    @ManyToOne(cascade=CascadeType.ALL)
    private Portfolio portfolio;

    @Column(name = "NAME")
    private String name;

    @Column(name = "ISSUER")
    private String issuer;

    public Portfolio getPortfolio() {
        return portfolio;
    }

    public void setPortfolio(Portfolio portfolio) {
        this.portfolio = portfolio;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getIssuer() {
        return issuer;
    }

    public void setIssuer(String issuer) {
        this.issuer = issuer;
    }

    public Long getInvestmentId() {
        return investmentId;
    }
}
