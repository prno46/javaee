package entities;

import org.hibernate.HibernateException;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;

import java.math.BigDecimal;
import java.util.Optional;

class MainApp {

    public static void main(String[] args) {
        Optional<SessionFactory> oFactory = Optional.of(HibernateUtil.getSessionFactory());
        Optional<Session> oSession = oFactory.map(SessionFactory::openSession);
        Optional<Transaction> oTransaction = oSession.map(Session::beginTransaction);
        try {

            Portfolio portfolio = new Portfolio();
            portfolio.setName("First investments");

            Bound bound = new Bound();
            bound.setName("Super Bound");
            bound.setIssuer("Batman");
            bound.setValue(new BigDecimal(5487));
            bound.setInterestRate(new BigDecimal(5));
            bound.setPortfolio(portfolio);

            portfolio.getInvestments().add(bound);

            oSession.ifPresent(s-> s.persist(bound));

            oTransaction.ifPresent(Transaction::commit);
        } catch (HibernateException e) {
            oTransaction.ifPresent(Transaction::rollback);
            e.getMessage();
        } finally {
            oSession.ifPresent(Session::close);
            oFactory.ifPresent(SessionFactory::close);
        }
    }
}
