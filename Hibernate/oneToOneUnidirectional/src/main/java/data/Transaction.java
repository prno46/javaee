package data;

import javax.persistence.*;
import java.math.BigDecimal;

@Entity
@Table(name = "transaction_table")
@Access(value = AccessType.FIELD)
class Transaction {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name = "transaction_id")
    private Long transactionId;

    @Column(name = "amount")
    private BigDecimal amount;

    @Column(name = "initial_balance")
    private BigDecimal initialBalance;

    @Column(name = "closing_balance")
    private BigDecimal closingBalance;

    // foreign key in Transaction table

    @OneToOne(targetEntity = Bank.class, cascade = CascadeType.ALL)
    @JoinColumn(name = "bank_fk", referencedColumnName = "bank_id")
    private Bank bank;

    public Transaction() { }

    public BigDecimal getAmount() {
        return amount;
    }

    public void setAmount(BigDecimal amount) {
        this.amount = amount;
    }

    public BigDecimal getInitialBalance() {
        return initialBalance;
    }

    public void setInitialBalance(BigDecimal initialBalance) {
        this.initialBalance = initialBalance;
    }

    public BigDecimal getClosingBalance() {
        return closingBalance;
    }

    public void setClosingBalance(BigDecimal closingBalance) {
        this.closingBalance = closingBalance;
    }

    public Bank getBank() {
        return bank;
    }

    public void setBank(Bank bank) {
        this.bank = bank;
    }

    public Long getTransactionId() {
        return transactionId;
    }
}
