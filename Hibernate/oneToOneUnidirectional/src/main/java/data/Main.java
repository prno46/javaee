package data;

import org.hibernate.HibernateException;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;

import java.math.BigDecimal;
import java.util.Optional;

class Main {
    public static void main(String[] args) {
        Optional<SessionFactory> oFactory = Optional.of(HibernateUtil.getSessionFactory());
        Optional<Session> oSession = oFactory.map(SessionFactory::openSession);
        Optional<Transaction> oTransaction = oSession.map(Session::beginTransaction);
        try {
            // setting bank
            Bank bank = new Bank();
            bank.setName("Santander");
            bank.setAddressLine("Grunwaldzka 87");
            bank.setCity("Gdansk");
            bank.setZipCode("87-874");

            // setting a transaction
            data.Transaction transaction = new data.Transaction();
            transaction.setAmount(new BigDecimal(5874));
            transaction.setInitialBalance(new BigDecimal(69874));
            transaction.setClosingBalance(new BigDecimal(44784));
            transaction.setBank(bank);


            oSession.ifPresent(s-> s.save(transaction));


            oTransaction.ifPresent(Transaction::commit);
        } catch (HibernateException e) {
            oTransaction.ifPresent(Transaction::rollback);
            e.getMessage();
        } finally {
            oSession.ifPresent(Session::close);
            oFactory.ifPresent(SessionFactory::close);
        }
    }
}
