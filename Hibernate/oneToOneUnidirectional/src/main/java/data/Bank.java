package data;

import javax.persistence.*;

@Entity
@Table(name = "bank_table")
@Access(value = AccessType.FIELD)
class Bank {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name = "bank_id")
    private Long bankId;

    @Column(name = "name")
    private String name;

    @Column(name = "street")
    private String addressLine;

    @Column(name = "city")
    private String city;

    @Column(name = "zip_code")
    private String zipCode;

    public Bank() { }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getAddressLine() {
        return addressLine;
    }

    public void setAddressLine(String addressLine1) {
        this.addressLine = addressLine1;
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public String getZipCode() {
        return zipCode;
    }

    public void setZipCode(String zipCode) {
        this.zipCode = zipCode;
    }

}
