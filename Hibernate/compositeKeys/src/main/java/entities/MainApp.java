package entities;

import org.hibernate.HibernateException;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;

import java.util.Optional;

class MainApp {

    public static void main(String[] args) {
        Optional<SessionFactory> oFactory = Optional.of(HibernateUtil.getSessionFactory());
        Optional<Session> oSession = oFactory.map(SessionFactory::openSession);
        Optional<Transaction> oTransaction = oSession.map(Session::beginTransaction);
        try {

            Currency currency = new Currency("Dollar", "USA");
            currency.setSymbol("$");

            // below persistence of single entity with composite primary key.
            //oSession.ifPresent(s-> s.persist(currency));

            // persistence of entity with composite primary key + relation mapping
            Market market = new Market();
            market.setMarketName("Black Market");
            market.setCountryName("Indonesia");
            market.setCurrency(currency);

            oSession.ifPresent(s-> s.persist(market));

            oTransaction.ifPresent(Transaction::commit);
        } catch (HibernateException e) {
            oTransaction.ifPresent(Transaction::rollback);
            e.getMessage();
        } finally {
            oSession.ifPresent(Session::close);
            oFactory.ifPresent(SessionFactory::close);
        }
    }
}
