package entities;

import javax.persistence.*;

/**
 * to handle an entity with composite primary key we need to mark both fields with @Id annotation
 * then we need to create new class that implements Serializable and has both those fields
 * the fields need to have same name as they have in entity.
 */

@Entity
@Table(name = "CURRENCY_TABLE")
@IdClass(value = CompositeKeys.class)
@Access(value = AccessType.FIELD)
class Currency {

    @Id
    @Column(name="CURRENCY_NAME")
    private String name;

    @Id
    @Column(name="CURRENCY_COUNTRY_NAME")
    private String countryName;

    @Column(name="CURRENCY_SYMBOL")
    private String symbol;

    public Currency() { }

    public Currency(String name, String countryName) {
        this.name = name;
        this.countryName = countryName;
    }

    public void setSymbol(String symbol) {
        this.symbol = symbol;
    }

    public String getName() {
        return name;
    }

    public String getCountryName() {
        return countryName;
    }

    public String getSymbol() {
        return symbol;
    }

}
