package entities;

import javax.persistence.*;
import java.io.Serializable;

@Entity
@Table(name = "MARKET_TABLE")
@Access(value = AccessType.FIELD)
class Market implements Serializable {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name="MARKET_ID")
    private Long marketId;

    /**
     * to make relation with an entity that has a composite primary key
     * we need to use @JoinColumns annotation to list all columns that
     * a composite key is made of.
     *
     * below is the situation when in Market table in Database we have composite foreign key
     * this is why we use @ManyToOne -> one currency on many markets
     */
    @ManyToOne(targetEntity = Currency.class, cascade = CascadeType.ALL)
    @JoinColumns({
            @JoinColumn(name = "FK_CURRENCY_NAME", referencedColumnName = "CURRENCY_NAME", nullable = false),
            @JoinColumn(name = "FK_COUNTRY_NAME", referencedColumnName = "CURRENCY_COUNTRY_NAME", nullable = false)
    })
    private Currency currency;

    @Column(name="COUNTRY_NAME")
    private String countryName;

    @Column(name="MARKET_NAME")
    private String marketName;


    public Market() {
    }

    public String getCountryName() {
        return countryName;
    }

    public void setCountryName(String countryName) {
        this.countryName = countryName;
    }

    public String getMarketName() {
        return marketName;
    }

    public void setMarketName(String marketName) {
        this.marketName = marketName;
    }

    public Long getMarketId() {
        return marketId;
    }

    public Currency getCurrency() {
        return currency;
    }

    public void setCurrency(Currency currency) {
        this.currency = currency;
    }
}
