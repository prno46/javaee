package entities;

import java.io.Serializable;

class CompositeKeys implements Serializable {

    private String name;

    private String countryName;

    public CompositeKeys() { }

    public CompositeKeys(String name, String countryName) {
        this.name = name;
        this.countryName = countryName;
    }

    public String getName() {
        return name;
    }

    public String getCountryName() {
        return countryName;
    }
}
