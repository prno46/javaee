package core.gameConfig;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.context.ConfigurableApplicationContext;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;

class Main {
    private static final Logger log = LoggerFactory.getLogger(Main.class);

    public static void main(String[] args) {
        log.info("test message from the logger");

        ConfigurableApplicationContext context = new AnnotationConfigApplicationContext(Config.class);

        context.close();
    }

}

