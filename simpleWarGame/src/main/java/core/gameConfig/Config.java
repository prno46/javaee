package core.gameConfig;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.PropertySource;

@PropertySource("classpath:game.properties")
@ComponentScan(basePackages = "core")
@Configuration
class Config {

    @Value("${game.max:35}")
    private int maxNumber;

    @Value("${game.min:1}")
    private int minNumber;

    @Value("${game.win:2}")
    private int gameWin;

    @Value("${game.tie:1}")
    private int gameTie;

    @Bean
    @MaxNumber
    int getMaxNumber() {
        return maxNumber;
    }

    @Bean
    @MinNumber
    int getMinNumber() {
        return minNumber;
    }

    @Bean
    @GameWin
    int getGameWin(){
        return gameWin;
    }

    @Bean
    @GameTie
    int getGameTie() {
        return gameTie;
    }
}
