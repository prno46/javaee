package core.model;

public class Player {
    private final String name;
    private int score;

    public Player(String name) {
        this.name = name;
        score=0;
    }

    public int getScore() {
        return score;
    }

    public void setScore(int score) {
        this.score += score;
    }

    public String getName() {
        return name;
    }

    public void scoreReset() {
        score = 0;
    }
}
