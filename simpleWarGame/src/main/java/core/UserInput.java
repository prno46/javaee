package core;

import java.util.Scanner;

public class UserInput {

    private static Scanner scanner = new Scanner(System.in);

    public static String getUserInput(){
        return scanner.nextLine();
    }

    public static void closeScanner() {
        scanner.close();
    }
}
