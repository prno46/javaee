package core.gameEngine;

public interface NumberGenerator {
    int getRandomNumber();
}
