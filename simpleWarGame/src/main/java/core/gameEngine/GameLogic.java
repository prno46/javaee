package core.gameEngine;

import core.gameConfig.GameTie;
import core.gameConfig.GameWin;
import core.model.Player;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
class GameLogic {

    private final NumberGenerator generator;
    private int p1RandomNumber;
    private int p2RandomNumber;

    private final int win;
    private final int tie;

    @Autowired
    GameLogic(NumberGenerator generator, @GameWin int win, @GameTie int tie) {
        this.generator = generator;
        this.win = win;
        this.tie = tie;
    }

    void setP1RandomNumber() {
        this.p1RandomNumber = generator.getRandomNumber();
    }

    void setP2RandomNumber() {
        this.p2RandomNumber = generator.getRandomNumber();
    }

    void dispatchPoints(Player playerOne, Player playerTwo) {
        if(p1RandomNumber == p2RandomNumber) {
            playerOne.setScore(tie);
            playerTwo.setScore(tie);
        }else if (p1RandomNumber > p2RandomNumber) {
            playerOne.setScore(win);
        } else {
            playerTwo.setScore(win);
        }
    }

    String pointWinner(Player playerOne, Player playerTwo) {
        return (playerOne.getScore() > playerTwo.getScore()) ? playerOne.getName() : playerTwo.getName();
    }
}
