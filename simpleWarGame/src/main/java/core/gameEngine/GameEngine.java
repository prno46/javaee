package core.gameEngine;

import core.model.Player;
import core.UserInput;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.event.ContextRefreshedEvent;
import org.springframework.context.event.EventListener;
import org.springframework.stereotype.Component;

@Component
class GameEngine {

    private Player playerOne;
    private Player playerTwo;
    private int moveCouner = 1;
    private String input;

    private final GameLogic logic;

    @Autowired
    GameEngine(GameLogic logic) {
        this.logic = logic;
    }

    @EventListener(ContextRefreshedEvent.class)
    public void start() {
        launchGame();
    }

    private void launchGame() {
        System.out.print("Enter the name of Player One: ");
        playerOne = new Player(UserInput.getUserInput());
        System.out.print("Enter the name of Player Two: ");
        playerTwo = new Player(UserInput.getUserInput());
        while (true) {
            if(moveCouner%2 != 0){
                System.out.println(playerOne.getName() + " press Enter");
                logic.setP1RandomNumber();
            } else {
                System.out.println(playerTwo.getName() + " press Enter");
                logic.setP2RandomNumber();
            }
            UserInput.getUserInput();
            if(moveCouner%2 == 0) {
                logic.dispatchPoints(playerOne, playerTwo);
                System.out.println(playerOne.getName() + " has " + playerOne.getScore() + " points and "+ playerTwo.getName() + " has "+ playerTwo.getScore() + " points");
            }
            moveCouner++;
            if(moveCouner == 11) {
                System.out.print(logic.pointWinner(playerOne, playerTwo) +" has won the game");
                System.out.println();
                System.out.print("Press Y to play again");
                input = UserInput.getUserInput();
                if(!input.equalsIgnoreCase("y")){
                    break;
                }
                gameReset();
            }
        }
        UserInput.closeScanner();
        System.out.println("End of the game");
    }

    private void gameReset() {
        moveCouner = 1;
        playerOne.scoreReset();
        playerTwo.scoreReset();
    }
}
