package core.gameEngine;

import core.gameConfig.MaxNumber;
import core.gameConfig.MinNumber;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.Random;

@Component
class NumberGeneratorImpl implements NumberGenerator {

    private Random random;
    private int max;
    private int min;

    @Autowired
    NumberGeneratorImpl(@MaxNumber int max, @MinNumber int min) {
        this.max = max;
        this.min = min;
        random = new Random();
    }

    @Override
    public int getRandomNumber() {
        return random.nextInt(max - min) + min;
    }
}
