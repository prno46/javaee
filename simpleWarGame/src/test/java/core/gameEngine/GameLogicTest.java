package core.gameEngine;

import core.model.Player;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.Mockito.*;

@ExtendWith(MockitoExtension.class)
class GameLogicTest {

    @Mock
    private NumberGenerator numberGenerator;
    private final int WIN = 3;
    private final int TIE =1;
    private final int WIN_NUMBER = 25;
    private final int LOSE_NUMBER = 15;
    private  Player playerOne;
    private  Player playerTwo;
    private final String PLAYER_ONE_NAME = "Adam";
    private final String PLAYER_ONE_TWO = "Joanna";

    private GameLogic SUT;

    @BeforeEach
    void setUp() {
        SUT = new GameLogic(numberGenerator, WIN, TIE);
        playerOne = new Player(PLAYER_ONE_NAME);
        playerTwo = new Player(PLAYER_ONE_TWO);
    }

    @Test
    void dispatchPoints_randomNumberGenerated_shouldReturnTwoInteractionsWithMock() {
        winCaseP1();
        loseCaseP2();
        verify(numberGenerator, times(2)).getRandomNumber();
    }

    @Test
    void dispatchPoints_playerOneWins_shouldReturnWIN() {
        winCaseP1();
        loseCaseP2();
        SUT.dispatchPoints(playerOne, playerTwo);
        assertEquals(WIN, playerOne.getScore());
        assertEquals(0, playerTwo.getScore());
    }

    // check win case for P2
    @Test
    void dispatchPoints_playerTwoWins_shouldReturnWIN() {
        winCaseP2();
        loseCaseP1();
        SUT.dispatchPoints(playerOne, playerTwo);
        assertEquals(WIN, playerTwo.getScore());
        assertEquals(0, playerOne.getScore());
    }

    @Test
    void dispatchPoints_playersHaveTie_shouldReturnTrue() {
        tieCase();
        SUT.dispatchPoints(playerOne, playerTwo);
        assertEquals(playerOne.getScore(), playerTwo.getScore());
    }

    @Test
    void pointWinner_playerOneWins_shouldReturnTrue() {
        winCaseP1();
        loseCaseP2();
        SUT.dispatchPoints(playerOne, playerTwo);
        String winnerName =  SUT.pointWinner(playerOne, playerTwo);
        assertEquals(PLAYER_ONE_NAME, winnerName);
    }

    private void winCaseP1() {
        when(numberGenerator.getRandomNumber()).thenReturn(WIN_NUMBER);
        SUT.setP1RandomNumber();
    }
    private void loseCaseP2() {
        when(numberGenerator.getRandomNumber()).thenReturn(LOSE_NUMBER);
        SUT.setP2RandomNumber();
    }
    private void winCaseP2() {
        when(numberGenerator.getRandomNumber()).thenReturn(WIN_NUMBER);
        SUT.setP2RandomNumber();
    }
    private void loseCaseP1() {
        when(numberGenerator.getRandomNumber()).thenReturn(LOSE_NUMBER);
        SUT.setP1RandomNumber();
    }

    private void tieCase() {
        when(numberGenerator.getRandomNumber()).thenReturn(LOSE_NUMBER);
        SUT.setP1RandomNumber();
        SUT.setP2RandomNumber();
    }
}