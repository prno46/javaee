package core.gameEngine;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertTrue;

class NumberGeneratorImplTest {

    private NumberGenerator SUT;
    private final int MIN = 5;
    private final int MAX = 30;

    @BeforeEach
    void setUp() {
        SUT = new NumberGeneratorImpl(MAX, MIN);
    }

    @Test
    void getRandomNumber_randomNumberIsGreaterThanMin_trueReturn() {
        int number = SUT.getRandomNumber();
        int number1 = SUT.getRandomNumber();
        int number2 = SUT.getRandomNumber();
        int number3 = SUT.getRandomNumber();
        int number4 = SUT.getRandomNumber();
        assertTrue(number >= MIN);
        assertTrue(number1 >= MIN);
        assertTrue(number2 >= MIN);
        assertTrue(number3 >= MIN);
        assertTrue(number4 >= MIN);
    }

    @Test
    void getRandomNumber_randomNumberIsNotGreaterThanMax_trueReturn() {
        int number = SUT.getRandomNumber();
        int number1 = SUT.getRandomNumber();
        int number2 = SUT.getRandomNumber();
        int number3 = SUT.getRandomNumber();
        assertTrue(number <= MAX);
        assertTrue(number1 <= MAX);
        assertTrue(number2 <= MAX);
        assertTrue(number3 <= MAX);
    }
}