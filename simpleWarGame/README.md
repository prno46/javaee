# War Game

Very simple console game build with Spring Framework.

You play with another person by pressing the enter button interchangeably.

After each button press you get a random points. Player who will have higher score after 10 rounds wins.


## Installing

Import it as a Maven project.

Start this project from main class.


## Built With

* [Maven](https://maven.apache.org/) - Dependency Management

## Authors

* **Przemyslaw Nowacki** 

